# Changelog
Ce fichier contient les modifications techniques du module.


## [2.16.1.0] - 2020-09-11

###  Ajout

- Modifier le logo de la bannière

__Properties__ :

### Modification

| fichier   |      nom      | nouvelle valeur |
|-----------|:-------------:|----------------:|
| gpart-exchange-webapp.properties | ui.theme.default | default |

## [2.12.5.0] - 2019-11-13

Utiliser le nouveau layout Guichet-Partenaires

###  Ajout

  __Properties__ :

Concerne uniquement les applications : exchange-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| gpart-exchange-webapp.properties | environment | Nom de l'environnement cible  | Non valorisé pour la production |
| gpart-exchange-webapp.properties | ui.theme.default | Thème par défaut  | base-v3 |
| gpart-exchange-webapp.properties | ui.theme.defaultSubTheme | Domaine utilisé pour le thème | guichet-partenaires |
| gpart-exchange-webapp.properties | frontUrl | URL publique du WWW Guichet-Partenaires  | ${WWW_GP_UI_EXT} |
| gpart-exchange-webapp.properties | ui.dashboard.url | URL publique du Dashboard Guichet-Partenaires  | ${DASHBOARD_GP_UI_EXT} |
| gpart-exchange-webapp.properties | welcome.public.url | URL publique de Welcome Guichet-Partenaires  | ${WELCOME_GP_UI_EXT} |

###  Suppression

  __Properties__ :

Concerne uniquement les applications : exchange-ui

| fichier   |      nom      |
|-----------|:-------------:|
| gpart-exchange-webapp.properties | ge.feedback.host |
| gpart-exchange-webapp.properties | ge.feedback.url |
