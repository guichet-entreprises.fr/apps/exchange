Logical Architecture {#logical_architecture}
====
# Logicals nodes {#nodes}
## nodes list {#nodes_lists}

| __type__ | __name__ | __details__ |
| ---- | ---- | ---- |
|virtual host| VH_EXCHANGE_IHM | vhost for exchange.guichet-entreprises.fr | 
|application server | EXCHANGE_SERVER | |
|application server| EXCHANGE_IHM | |
|DBMS | BDD_EXCHANGE | Exchange database |

## open ports {#ports}

| __source__ | __destination__ | __protocol__ | __classic port__ |
| ---- | ---- | ---- | ---- |
| VH_EXCHANGE_IHM | EXCHANGE_IHM | AJP13 | 8019 |
| EXCHANGE_IHM | EXCHANGE_SERVER | HTTP | 80 |
| EXCHANGE_IHM | ACCOUNT | HTTPS | 81 |
| EXCHANGE_SERVER | TRACKER | HTTP | 80 |
| EXCHANGE_SERVER | BDD_EXCHANGE | JDBC | 5432 |


