# Exchange Bubble {#exchange}
## Introduction
This applications set's purpose is to provide a single exit point (bubble level) to every in the Information System.

## Exit channels
### Email channel
Sending an email to a recipient without being concerned about SMTP server authentication or email formatting.

### Mail channel
Sending an actual postmail by using an external mail service provider. The client application don't have to know the provider, they only need to know the format and the channel to use.

## Logical Architecture 

see : [LA classic syntax direct](doc/logical_architecture.md)
