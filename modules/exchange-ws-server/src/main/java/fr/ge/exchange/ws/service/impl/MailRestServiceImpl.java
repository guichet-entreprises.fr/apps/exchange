/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.exchange.ws.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.MailMessage;
import fr.ge.exchange.service.IPostMailService;
import fr.ge.exchange.ws.model.MailMessageStatus;
import fr.ge.exchange.ws.rest.IMailRestService;
import fr.ge.ws.exchange.bean.conf.PostMailConfigurationBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Postmail service implementation.
 */
@Api
@Path("/v1")
public class MailRestServiceImpl implements IMailRestService {

  /** Le logger fonctionnel. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();
  /** Le logger technique. */
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  /** The postmail service. */
  @Autowired
  private IPostMailService postMailService;

  /**
   * {@inheritDoc}
   * 
   * @throws TechniqueException
   *           a technical error prevents from sending the mail
   */
  @Override
  public String send(final String referenceId, final String recipientName, final String recipientNameCompl,
    final String recipientAddressName, final String recipientAddressNameCompl, final String recipientPostalCode,
    final String recipientCity, final InputStream attachmentFile) throws TechniqueException {

    String mailId = null;
    LOGGER_FONC.info("WebService sendPostMail avec referenceId  {}.", referenceId);
    MailMessage postmailBean = new MailMessage();
    // reformat recipient name
    boolean partOne = StringUtils.isNotBlank(recipientName);
    boolean partTwo = StringUtils.isNotBlank(recipientNameCompl);
    postmailBean.setRecipientName(
      String.format("%s%s%s", partOne ? recipientName : "", partOne & partTwo ? "\n" : "", partTwo ? recipientNameCompl : ""));
    // reformat address
    partOne = StringUtils.isNotBlank(recipientAddressName);
    partTwo = StringUtils.isNotBlank(recipientAddressNameCompl);
    postmailBean.setRecipientAddress(String.format("%s%s%s", partOne ? recipientAddressName : "", partOne & partTwo ? "\n" : "",
      partTwo ? recipientAddressNameCompl : ""));
    postmailBean.setRecipientPostalCode(recipientPostalCode);
    postmailBean.setRecipientCity(recipientCity);
    try {
      mailId = postMailService.postMail(attachmentFile, postmailBean, referenceId,
        PostMailConfigurationBean.getInstance().getAttachmentLocalDirectoryStorage(),
        PostMailConfigurationBean.getInstance().getAttachmentSftpDirectoryStorage());
    } catch (TechniqueException | RuntimeException e) {
      String errorMsg = String.format("Unable to send a mail to '%s' with reference '%s'.", recipientName, referenceId);
      LOGGER_FONC.error(errorMsg);
      LOGGER_TECH.error(errorMsg);
      throw new InternalServerErrorException(errorMsg);
    }

    return mailId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Response download(final String postmailId) {
    LOGGER_FONC.info("Demande de telechargement de la demande d'envoi de courrier : " + postmailId);
    try {
      MailMessage mailMessage = postMailService.getMailMessage(postmailId);
      final String attachmentPath = mailMessage.getAttachmentPath();
      LOGGER_FONC.debug("Chemin du courrier : " + attachmentPath);

      int index = attachmentPath.lastIndexOf(File.separator);
      String fileName = attachmentPath.substring(index + 1);
      LOGGER_FONC.info("Nom du fichier pour le telechargement : " + fileName);

      return Response.ok(FileUtils.openInputStream(new File(attachmentPath))).header(HttpHeaders.CONTENT_TYPE, "application/zip")
        .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=\"%s\"", fileName)).build();
    } catch (IOException e) {
      LOGGER_TECH.error("Une erreur technique est remontée lors de la récupération du fichier", e);
      return Response.noContent().status(Status.NOT_FOUND).build();
    } catch (TechniqueException | RuntimeException e) {
      LOGGER_TECH.error("Une erreur technique est remontée lors de la recherche du contenu du message", e);
      return Response.noContent().status(Status.INTERNAL_SERVER_ERROR).build();
    }
  }

  @Override
  @ApiOperation(value = "Searching a postmail")
  public MailMessageStatus getStatus(@ApiParam("Id postmail") final String trackerId) {
    LOGGER_FONC.info("Search for trackId {}.", trackerId);
    MailMessageStatus mailMessageStatus = null;
    try {
      MailMessage mailMessage = postMailService.getMailMessage(trackerId);
      if (mailMessage == null) {
        throw new NotFoundException(String.format("Unable to find the mail message by its id : %s.", trackerId));
      }
      mailMessageStatus = new MailMessageStatus();
      mailMessageStatus.setTrackerId(mailMessage.getTrackerId());
      mailMessageStatus.setRecipientName(mailMessage.getRecipientName());
      mailMessageStatus.setUrgency(mailMessage.getUrgency());
      mailMessageStatus.setChannel(mailMessage.getChannel());
      mailMessageStatus.setDeliveryId(mailMessage.getDeliveryId());
      mailMessageStatus.setStatus(mailMessage.getStatus());
      mailMessageStatus.setAddress(String.format("%s %s %s", mailMessage.getRecipientAddress(),
        mailMessage.getRecipientPostalCode(), mailMessage.getRecipientCity()));
      String datePostmail = DateFormatUtils.format(mailMessage.getDatePostmail(), "dd/MM/yyyy");
      mailMessageStatus.setDatePostmail(datePostmail);
    } catch (TechniqueException | RuntimeException e) {
      String errorMsg = String.format("Une erreur technique est remontée lors de la récupération du courrier %s.", trackerId);
      LOGGER_FONC.error(errorMsg, e);
      throw new InternalServerErrorException(errorMsg);
    }
    LOGGER_FONC.info("Search result : {}.", mailMessageStatus);
    return mailMessageStatus;
  }

}
