/**
 * 
 */
package fr.ge.ws.exchange;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.PostmailBean;
import fr.ge.exchange.postmail.ws.v1.bean.PostMailAddress;

/**
 * Classe utilitaire de transition.
 * 
 * @deprecated
 * @author $Author: LABEMONT $
 * @version $Revision: 0 $
 */
public class BeanModelUtils {

  /**
   * Convert a WS bean to a Service bean. TODO LAB : remove
   * 
   * @param postMailInfo
   *          the postmail information
   * @return null if null
   * @throws TechniqueException
   *           the technical exception
   */
  public static PostmailBean toPostmail(final PostMailAddress postMailInfo) throws TechniqueException {
    if (postMailInfo == null) {
      return null;
    }
    PostmailBean postmailBean = new PostmailBean();
    postmailBean.setReferenceId(postMailInfo.getTrackId());
    postmailBean.setRecipientName(postMailInfo.getRecipientNameComplete());
    postmailBean.setRecipientAddress(postMailInfo.getAddressNameComplete());
    postmailBean.setRecipientPostalCode(postMailInfo.getPostalCode());
    postmailBean.setRecipientCity(postMailInfo.getCity());
    postmailBean.setAttachmentPath(postMailInfo.getAttachmentPath());
    return postmailBean;
  }
}
