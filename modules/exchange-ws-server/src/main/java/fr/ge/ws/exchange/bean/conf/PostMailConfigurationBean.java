/**
 * 
 */
package fr.ge.ws.exchange.bean.conf;

import java.io.Serializable;

/**
 * Class PostMailConfigurationBean.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class PostMailConfigurationBean implements Serializable {

  /** UID. */
  private static final long serialVersionUID = -1323454463640492145L;

  /** local storage for zip file. */
  private String attachmentLocalDirectoryStorage;

  /** sftp storage for zip file. */
  private String attachmentSftpDirectoryStorage;

  /** sftp storage for update status file */
  private String updateStatusCsvDirectoryStorage;

  /** sftp name for update status file */
  private String updateStatusCsvFile;

  /** temp directory for the downloaded sftp csv file */
  private String tmpLocalDirectoryStorage;

  /** Instance statique du bean de configuration. */
  private static PostMailConfigurationBean instance = new PostMailConfigurationBean();

  /**
   * Récupère l'instance unique de PostMailConfigurationBean.
   *
   * @return unique instance de PostMailConfigurationBean
   */
  public static PostMailConfigurationBean getInstance() {
    return instance;
  }

  /**
   * Accesseur sur l'attribut {@link #attachmentLocalDirectoryStorage}.
   *
   * @return String attachmentLocalDirectoryStorage
   */
  public String getAttachmentLocalDirectoryStorage() {
    return this.attachmentLocalDirectoryStorage;
  }

  /**
   * Mutateur sur l'attribut {@link #attachmentLocalDirectoryStorage}.
   *
   * @param attachmentLocalDirectoryStorage
   *          la nouvelle valeur de l'attribut attachmentLocalDirectoryStorage
   */
  public void setAttachmentLocalDirectoryStorage(final String attachmentLocalDirectoryStorage) {
    this.attachmentLocalDirectoryStorage = attachmentLocalDirectoryStorage;
  }

  /**
   * Accesseur sur l'attribut {@link #attachmentSftpDirectoryStorage}.
   *
   * @return String attachmentSftpDirectoryStorage
   */
  public String getAttachmentSftpDirectoryStorage() {
    return this.attachmentSftpDirectoryStorage;
  }

  /**
   * Mutateur sur l'attribut {@link #attachmentSftpDirectoryStorage}.
   *
   * @param attachmentSftpDirectoryStorage
   *          la nouvelle valeur de l'attribut attachmentSftpDirectoryStorage
   */
  public void setAttachmentSftpDirectoryStorage(final String attachmentSftpDirectoryStorage) {
    this.attachmentSftpDirectoryStorage = attachmentSftpDirectoryStorage;
  }

  /**
   * Accesseur sur l'attribut {@link #updateStatusCsvDirectoryStorage}.
   *
   * @return String updateSatusCsvDirectoryStorage
   */
  public String getUpdateSatusCsvDirectoryStorage() {
    return updateStatusCsvDirectoryStorage;
  }

  /**
   * Mutateur sur l'attribut {@link #updateStatusCsvDirectoryStorage}.
   *
   * @param updateSatusCsvDirectoryStorage
   *          la nouvelle valeur de l'attribut updateSatusCsvDirectoryStorage
   */
  public void setUpdateSatusCsvDirectoryStorage(String updateSatusCsvDirectoryStorage) {
    this.updateStatusCsvDirectoryStorage = updateSatusCsvDirectoryStorage;
  }

  /**
   * Accesseur sur l'attribut {@link #updateStatusCsvFile}.
   *
   * @return String updateStatusCsvFile
   */
  public String getUpdateStatusCsvFile() {
    return updateStatusCsvFile;
  }

  /**
   * Mutateur sur l'attribut {@link #updateStatusCsvFile}.
   *
   * @param updateStatusCsvFile
   *          la nouvelle valeur de l'attribut updateStatusCsvFile
   */
  public void setUpdateStatusCsvFile(String updateStatusCsvFile) {
    this.updateStatusCsvFile = updateStatusCsvFile;
  }

  /**
   * Accesseur sur l'attribut {@link #tmpLocalDirectoryStorage}.
   *
   * @return String tmpLocalDirectoryStorage
   */
  public String getTmpLocalDirectoryStorage() {
    return tmpLocalDirectoryStorage;
  }

  /**
   * Mutateur sur l'attribut {@link #tmpLocalDirectoryStorage}.
   *
   * @param tmpLocalDirectoryStorage
   *          la nouvelle valeur de l'attribut tmpLocalDirectoryStorage
   */
  public void setTmpLocalDirectoryStorage(String tmpLocalDirectoryStorage) {
    this.tmpLocalDirectoryStorage = tmpLocalDirectoryStorage;
  }

}
