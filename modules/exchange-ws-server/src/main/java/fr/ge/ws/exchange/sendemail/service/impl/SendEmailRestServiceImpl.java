/**
 * 
 */
package fr.ge.ws.exchange.sendemail.service.impl;

import java.io.InputStream;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;
import fr.ge.exchange.email.ws.v1.bean.EmailErrorBean;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;
import fr.ge.exchange.email.ws.v1.service.ISendEmailRestService;
import fr.ge.exchange.service.ISendEmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Postmail service implementation.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@Api
@Path("/v1")
public class SendEmailRestServiceImpl implements ISendEmailRestService {

  /** Le logger fonctionnel. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /** The sendemail service. */
  @Autowired
  private ISendEmailService sendEmailService;

  /**
   * {@inheritDoc}
   */
  @Override
  @ApiOperation(value = "Send an email with a PDF attachment")
  public Response sendEmail(@ApiParam(value = "Email information") final EmailSendBean emailSend,
    final InputStream attachmentPdf) {
    LOGGER_FONC.info("Appel au service d'envoi de courriel avec une pièce jointe au format PDF");
    try {
      sendEmailService.sendEmail(emailSend, attachmentPdf);
    } catch (TechniqueException e) {
      EmailErrorBean error = new EmailErrorBean();
      error.setErrorCode(e.getCode());
      error.setErrorMessage(e.getMessage());
      if (ICodeExceptionConstante.ERR_EXCH_INVALID_PARAMETER.equals(e.getCode())) {
        return Response.status(Status.BAD_REQUEST).entity(error).build();
      }
      return Response.serverError().entity(error).build();
    }
    return Response.ok(emailSend).build();
  }

  @Override
  @ApiOperation(value = "Send an email without any attachment")
  public Response sendEmail(final EmailSendBean emailSend) {
    LOGGER_FONC.info("Appel au service d'envoi de courriel sans pièce jointes");
    try {
      sendEmailService.sendEmail(emailSend);
    } catch (TechniqueException e) {
      LOGGER_FONC.info("Une erreur est survenue lors de l'envoi de courriel sans pièce jointes", e);
      EmailErrorBean error = new EmailErrorBean();
      error.setErrorCode(e.getCode());
      error.setErrorMessage(e.getMessage());
      if (ICodeExceptionConstante.ERR_EXCH_INVALID_PARAMETER.equals(e.getCode())) {
        return Response.status(Status.BAD_REQUEST).entity(error).build();
      }
      return Response.serverError().entity(error).build();
    }
    return Response.ok(emailSend).build();
  }
}
