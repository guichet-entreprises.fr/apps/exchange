/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.ws.exchange.support.appstatus;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.core.log.GestionnaireTrace;
import fr.ge.ct.ses.clients.SFTPClient;
import net.sf.appstatus.core.check.AbstractCheck;
import net.sf.appstatus.core.check.CheckResultBuilder;
import net.sf.appstatus.core.check.ICheckResult;

/**
 * Checking if SFTP serveur accessible.
 * 
 * @author $Author: labemont $
 */
public class ServiceImprimerieNationaleCheck extends AbstractCheck {

  /** The technical logger. */
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  /** The schema version service. */
  @Autowired
  private SFTPClient sftpINClient;

  @Override
  public String getGroup() {
    return "Services";
  }

  @Override
  public String getName() {
    return "EXCHANGE_ImprimerieNationale";
  }

  @Override
  public ICheckResult checkStatus() {
    CheckResultBuilder result = new CheckResultBuilder();
    // Récupération nom et groupe du checker
    result.from(this);
    List < String > files = null;
    try {
      // Teste les méthodes paramétrées.
      files = this.sftpINClient.listerNonRecursive("/", 1, null);
      result.code(OK);
    } catch (Exception e) {
      LOGGER_TECH.error("Impossible d'accéder au serveur SFTP de l'Imprimerie Nationale", e);
      result.code(FATAL);
      result.resolutionSteps(e.toString());
    }

    result.description("Le Serveur SFTP de l'Imprimerie Nationale possède au moins 1 fichier.");
    return result.build();
  }

}
