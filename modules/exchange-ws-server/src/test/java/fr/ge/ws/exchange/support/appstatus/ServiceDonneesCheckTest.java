package fr.ge.ws.exchange.support.appstatus;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.exchange.service.ISchemaVersionService;
import fr.ge.ws.exchange.support.appstatus.ServiceDonneesCheck;
import net.sf.appstatus.core.check.ICheckResult;

/**
 * Tests {@link ServiceDonneesCheck}.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ServiceDonneesCheckTest {

  /** database version check. */
  @InjectMocks
  private ServiceDonneesCheck serviceDonneesCheck;

  /** The schema version service. */
  @Mock
  private ISchemaVersionService schemaVersionService;

  /**
   * Mock injection.
   */
  @Before
  public void initMock() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Testing database version.
   */
  @Test
  public void testCheckStatus() {
    Mockito.when(schemaVersionService.getVersion()).thenReturn("1.0.0.1");
    ICheckResult resultTrue = serviceDonneesCheck.checkStatus();
    assertThat(resultTrue.getCode()).isEqualTo(0);
  }

  /**
   * Testing fail database version.
   */
  @Test
  public void testFailCheckStatus() {
    Mockito.doThrow(new NullPointerException()).when(schemaVersionService).getVersion();
    ICheckResult resultTrue = serviceDonneesCheck.checkStatus();
    assertThat(resultTrue.getCode()).isEqualTo(2);
  }

}
