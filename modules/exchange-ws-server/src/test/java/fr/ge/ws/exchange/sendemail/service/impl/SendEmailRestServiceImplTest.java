package fr.ge.ws.exchange.sendemail.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;
import fr.ge.exchange.service.ISendEmailService;

/**
 * Test WebService {@link SendEmailRestServiceImpl}.
 */
public class SendEmailRestServiceImplTest {

  /** sendEmail rest service. */
  @InjectMocks
  private SendEmailRestServiceImpl sendEmailRestService;

  /** send mail service. */
  @Mock
  private ISendEmailService mockSendEmailService;

  /**
   * Mock injection.
   */
  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test send email.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSendEmail() throws Exception {
    EmailSendBean emailInfo = new EmailSendBean();
    emailInfo.setContent("Email content");
    emailInfo.setRecipient("recipient1@test.com;recipient2@test.com;recipient3@test.com");
    emailInfo.setObject("Object");
    File file = new File(this.getClass().getResource("/").getFile() + "pdf/templateTest.pdf");
    InputStream attachmentPdf = new FileInputStream(file);

    // cas nominal : HTTP 200
    Response response = sendEmailRestService.sendEmail(emailInfo, attachmentPdf);
    assertNotNull(response);
    assertEquals(200, response.getStatus());

    // cas invalid parameter : HTTP 400 (request invalid)
    Mockito.doThrow(new TechniqueException(ICodeExceptionConstante.ERR_EXCH_INVALID_PARAMETER, "Erreur Invalid parameter"))
      .when(mockSendEmailService).sendEmail(Mockito.anyObject(), Mockito.anyObject());
    response = sendEmailRestService.sendEmail(emailInfo, attachmentPdf);
    assertNotNull(response);
    assertEquals(400, response.getStatus());

    // cas erreur envoi mail : HTTP 500
    Mockito.doThrow(new TechniqueException(ICodeExceptionConstante.ERR_EXCH_EMAIL, "Erreur envoi mail"))
      .when(mockSendEmailService).sendEmail(Mockito.anyObject(), Mockito.anyObject());
    response = sendEmailRestService.sendEmail(emailInfo, attachmentPdf);
    assertNotNull(response);
    assertEquals(500, response.getStatus());
  }

  /**
   * Test send email without attachement.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSendEmailWithoutAttachement() throws Exception {
    EmailSendBean emailInfo = new EmailSendBean();
    emailInfo.setContent("Email content");
    emailInfo.setRecipient("recipient1@test.com;recipient2@test.com;recipient3@test.com");
    emailInfo.setObject("Object");

    // cas nominal : HTTP 200
    Response response = sendEmailRestService.sendEmail(emailInfo);
    assertNotNull(response);
    assertEquals(200, response.getStatus());

    // cas invalid parameter : HTTP 400 (request invalid)
    Mockito.doThrow(new TechniqueException(ICodeExceptionConstante.ERR_EXCH_INVALID_PARAMETER, "Erreur Invalid parameter"))
      .when(mockSendEmailService).sendEmail(Mockito.anyObject());
    response = sendEmailRestService.sendEmail(emailInfo);
    assertNotNull(response);
    assertEquals(400, response.getStatus());

    // cas erreur envoi mail : HTTP 500
    Mockito.doThrow(new TechniqueException(ICodeExceptionConstante.ERR_EXCH_EMAIL, "Erreur envoi mail"))
      .when(mockSendEmailService).sendEmail(Mockito.anyObject());
    response = sendEmailRestService.sendEmail(emailInfo);
    assertNotNull(response);
    assertEquals(500, response.getStatus());
  }
}
