/**
 * 
 */
package fr.ge.exchange.ws.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import fr.ge.common.utils.test.AbstractRestTest;



/**
 * @author mmouhous
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:spring/test-context-server.xml"})

public class ManagedWebServiceImplTest extends AbstractRestTest {
	  
	  
	@Test
    public void healthcheckTest() {
		
        Response response = this.client().accept(MediaType.TEXT_PLAIN).path("/exchange/healthcheck").get();
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

    }
}
