package fr.ge.exchange.context;

import fr.ge.exchange.bean.ExchangeUserBean;

/**
 * ThreadContext Exchange.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class ExchangeThreadContext {

    /**
     * La constante thread local.
     */
    public static final ThreadLocal<ExchangeUserBean> LOCAL_USER = new ThreadLocal<ExchangeUserBean>();

    /**
     * Constructeur de la classe.
     *
     */
    private ExchangeThreadContext() {
        super();
    }

    /**
     * Sets.
     *
     * @param userFormsBean
     *            l'utilisateur GPart
     */
    public static void setUser(final ExchangeUserBean userFormsBean) {
        LOCAL_USER.set(userFormsBean);
    }

    /**
     * Unset User.
     */
    public static void unsetUser() {
        LOCAL_USER.remove();
    }

    /**
     * Gets GPartUserBean.
     *
     * @return l'utilisateur GPartUserBean
     */
    public static ExchangeUserBean getUser() {
        return LOCAL_USER.get();
    }

}
