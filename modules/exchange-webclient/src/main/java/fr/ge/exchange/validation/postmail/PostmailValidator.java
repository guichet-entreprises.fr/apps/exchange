package fr.ge.exchange.validation.postmail;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import fr.ge.common.support.i18n.MessageReader;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.constant.IExchangeConstant;
import fr.ge.exchange.postmail.ws.v1.bean.PostMailAddress;

/**
 * Validator for the PostmailValidator.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component
public class PostmailValidator implements Validator {

  /** La constante LOGGER_FONC. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /**
   * Token Validation.
   *
   * @param token
   *          token
   * @param regEx
   *          regular expression to check
   * @return true if success
   */
  @SuppressWarnings("static-method")
  protected boolean validatePattern(final String token, final String regEx) {
    Pattern pattern = Pattern.compile(regEx);
    // Vérifie la conformité du format
    Matcher m = pattern.matcher(token);
    if (!m.matches()) {
      return false;
    }
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void validate(final Object target, final Errors errors) {
    LOGGER_FONC.debug("Postmail Validation");
    PostMailAddress postmail = (PostMailAddress) target;
    this.validateRecipient(errors, postmail);

    this.validateAddress(errors, postmail);

    this.validateCity(errors, postmail);
  }

  /**
   * Validate city and postal code fields.
   * 
   * @param errors
   *          All errors occured
   * @param postmail
   *          postmail information
   */
  private void validateCity(final Errors errors, final PostMailAddress postmail) {
    // -->Checking recipient postal code
    if (StringUtils.isBlank(postmail.getPostalCode())) {
      LOGGER_FONC.debug("Empty recipient postal code");
      ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode",
        MessageReader.getReader().getFormatter().format("The postal code is required"));
    } else {
      if (postmail.getPostalCode().trim().length() > IExchangeConstant.POSTALCODE_MAX_LENGTH) {
        errors.rejectValue("postalCode",
          MessageReader.getReader().getFormatter().format("The maximum length for the postal code is 10"));
      }
    }

    // -->Checking recipient city
    if (StringUtils.isBlank(postmail.getCity())) {
      LOGGER_FONC.debug("Empty recipient city");
      ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city",
        MessageReader.getReader().getFormatter().format("The city is required"));
    } else {
      if (postmail.getCity().trim().length() > IExchangeConstant.CITY_MAX_LENGTH) {
        errors.rejectValue("city", MessageReader.getReader().getFormatter().format("The maximum length for the city is 55"));
      }

      if (!this.validatePattern(postmail.getCity(), IExchangeConstant.REGEX_ISO8859_CITY)) {
        errors.rejectValue("city",
          MessageReader.getReader().getFormatter().format("The city should not contain any special characters"));
      }
    }
  }

  /**
   * Validate addresses fields.
   * 
   * @param errors
   *          All errors occured
   * @param postmail
   *          postmail information
   */
  private void validateAddress(final Errors errors, final PostMailAddress postmail) {
    // -->Checking recipient address
    if (StringUtils.isBlank(postmail.getAddressName())) {
      LOGGER_FONC.debug("Empty recipient address");
      ValidationUtils.rejectIfEmptyOrWhitespace(errors, "addressName",
        MessageReader.getReader().getFormatter().format("The address is required"));
    } else {
      if (postmail.getAddressName().trim().length() > IExchangeConstant.ADDRESS_MAX_LENGTH) {
        errors.rejectValue("addressName",
          MessageReader.getReader().getFormatter().format("The maximum length for the address name is 18 "));
      } else if (!this.validatePattern(postmail.getAddressName(), IExchangeConstant.REGEX_ISO8859_ADDRESS)) {
        errors.rejectValue("addressName",
          MessageReader.getReader().getFormatter().format("The city should not contain any special characters"));
      }
    }

    // -->Checking recipient address complement
    if (!StringUtils.isBlank(postmail.getAddressNameCompl())) {
      if (postmail.getAddressNameCompl().trim().length() > IExchangeConstant.ADDRESS_MAX_LENGTH) {
        errors.rejectValue("addressNameCompl",
          MessageReader.getReader().getFormatter().format("The maximum length for the address complement name is 75 "));
      } else if (!this.validatePattern(postmail.getAddressNameCompl(), IExchangeConstant.REGEX_ISO8859_ADDRESS)) {
        errors.rejectValue("addressNameCompl",
          MessageReader.getReader().getFormatter().format("The city should not contain any special characters"));
      }
    }
  }

  /**
   * Validate recipient fields.
   * 
   * @param errors
   *          All errors occured
   * @param postmail
   *          postmail information
   */
  private void validateRecipient(final Errors errors, final PostMailAddress postmail) {
    // -->Checking recipient name
    if (StringUtils.isBlank(postmail.getRecipientName())) {
      LOGGER_FONC.debug("Empty recipient address");
      ValidationUtils.rejectIfEmptyOrWhitespace(errors, "recipientName",
        MessageReader.getReader().getFormatter().format("The recipient name is required"));
    } else {
      if (postmail.getRecipientName().trim().length() > IExchangeConstant.RECIPIENT_MAX_LENGTH) {
        errors.rejectValue("recipientName",
          MessageReader.getReader().getFormatter().format("The maximum length for the recipient name is 125"));
      }
    }

    // -->Checking recipient name complement
    if (!StringUtils.isBlank(postmail.getRecipientNameCompl())) {
      if (postmail.getRecipientNameCompl().trim().length() > IExchangeConstant.RECIPIENT_MAX_LENGTH) {
        errors.rejectValue("recipientNameCompl",
          MessageReader.getReader().getFormatter().format("The maximum length for the recipient complement name is 125"));
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean supports(@SuppressWarnings("rawtypes") final Class c) {
    return PostMailAddress.class.equals(c);
  }
}
