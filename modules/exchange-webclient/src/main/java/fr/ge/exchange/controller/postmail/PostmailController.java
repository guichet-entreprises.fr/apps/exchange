/**
 * 
 */
package fr.ge.exchange.controller.postmail;

import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.WebContentGenerator;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.postmail.ws.v1.bean.PostMailAddress;
import fr.ge.exchange.postmail.ws.v1.bean.PostMailErrorBean;
import fr.ge.exchange.validation.postmail.PostmailValidator;
import fr.ge.exchange.ws.rest.IMailRestService;

/**
 * The postmail controller.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Controller
public class PostmailController extends WebContentGenerator {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /** The Mail REST Service. */
    @Autowired
    private IMailRestService mailRestService;

    /** Postmail validator. **/
    @Autowired
    private PostmailValidator postmailValidator;

    /**
     * Display forms to send a new postmail.
     *
     * @param model
     *            le model
     * @return le string
     * @throws Exception
     *             le exception
     */
    @RequestMapping(value = "/postmail", method = RequestMethod.GET)
    public String displayPostmailPage(final Model model) throws Exception {
        model.addAttribute("postmailSend", new PostMailAddress());
        return "postmail/main";
    }

    /**
     * Send a new postmail for the Imprimerie Nationale.
     * 
     * @param postMailSend
     *            the postmail information
     * @param model
     *            the model
     * @param errors
     *            the errors
     * @param file
     *            the attachment
     * @return the main page to display
     * @throws Exception
     *             Exception raised
     */
    @RequestMapping(value = "/postmail/send", method = RequestMethod.POST)
    public String sendPostmail(@ModelAttribute("postmailSend") final PostMailAddress postMailSend, final Model model, final BindingResult errors,
            @RequestParam(value = "file", required = true) final MultipartFile file

    ) throws Exception {
        // -->Algorithme attendu
        // 1--Le champ Id Suivi devient Id Tracker
        // 2--Ce champ est non-obligatoire
        // 3.1--Si le champ n'est pas saisi par l'utilisateur alors appel au
        // service TRACKER de
        // génération d'un nouvel UID
        // 3.2--Récupérer l'identifiant fourni par l'utilisateur
        // <--
        LOGGER_FONC.info("Contrôleur sendPostmail : envoi courrier à l'Imprimerie Nationale");

        this.postmailValidator.validate(postMailSend, errors);
        if (errors.hasErrors()) {
            LOGGER_FONC.warn("Le formulaire postMail contiennent des erreurs");
            model.addAttribute("postmailSend", postMailSend);
            return "postmail/main";
        }

        String response = null;
        try {
            response = this.mailRestService.send(postMailSend.getTrackId(), postMailSend.getRecipientName(), postMailSend.getRecipientNameCompl(), postMailSend.getAddressName(),
                    postMailSend.getAddressNameCompl(), postMailSend.getPostalCode(), postMailSend.getCity(), file.getInputStream());
        } catch (TechniqueException te) {
            return "postmail/error";
        }

        if (StringUtils.isEmpty(response)) {
            LOGGER_FONC.error("Unable to send mail to {} {} at {} {} {} {}.", postMailSend.getRecipientName(), postMailSend.getRecipientNameCompl(), postMailSend.getAddressName(),
                    postMailSend.getAddressNameCompl(), postMailSend.getPostalCode(), postMailSend.getCity());
            return "postmail/error";
        }

        postMailSend.setTrackId(response);
        model.addAttribute("postmailSend", postMailSend);
        LOGGER_FONC.info("Message {} send to {} {} at {} {} {} {}.", response, postMailSend.getRecipientName(), postMailSend.getRecipientNameCompl(), postMailSend.getAddressName(),
                postMailSend.getAddressNameCompl(), postMailSend.getPostalCode(), postMailSend.getCity());
        return "postmail/success";
    }

    /**
     * Gets an postmail from local file system.
     * 
     * @param trackId
     *            the track id
     * @param httpResponse
     *            the http response
     * @throws Exception
     *             The Exception
     */
    @RequestMapping(value = "/postmail/download", method = RequestMethod.GET)
    public void download(@RequestParam("trackId") final String trackId, final HttpServletResponse httpResponse) throws Exception {
        LOGGER_FONC.info("Contrôleur download : téléchargement du ZIP postMail [{}]", trackId);

        Response fileDownload = this.mailRestService.download(trackId);
        InputStream fileInputStream = fileDownload.readEntity(InputStream.class);

        httpResponse.addHeader(HttpHeaders.CONTENT_TYPE, fileDownload.getHeaderString(HttpHeaders.CONTENT_TYPE));
        httpResponse.addHeader(HttpHeaders.CONTENT_DISPOSITION, fileDownload.getHeaderString(HttpHeaders.CONTENT_DISPOSITION));

        IOUtils.copy(fileInputStream, httpResponse.getOutputStream());
        httpResponse.flushBuffer();
    }

    /**
     * Display a postmail sent successfully.
     *
     * @param postMailSend
     *            post mail send
     * @param model
     *            le model
     * @return le string
     * @throws Exception
     *             le exception
     */
    @RequestMapping(value = "/postmailSuccess", method = RequestMethod.GET)
    public String displaySuccessPostmail(@ModelAttribute("postmailSend") final PostMailAddress postMailSend, final Model model) throws Exception {
        model.addAttribute("postmailSend", postMailSend);
        return "postmail/success";
    }

    /**
     * Display an error for a postmail.
     *
     * @param errorBean
     *            error bean
     * @param model
     *            le model
     * @return le string
     * @throws Exception
     *             le exception
     */
    @RequestMapping(value = "/postmailError", method = RequestMethod.GET)
    public String displayErrorPostmail(@ModelAttribute("postmailError") final PostMailErrorBean errorBean, final Model model) throws Exception {
        model.addAttribute("postmailError", errorBean);
        return "postmail/error";
    }

}
