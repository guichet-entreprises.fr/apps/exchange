/**
 * 
 */
package fr.ge.exchange.constant;

/**
 * Interface of all constant for Exchange.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface IExchangeConstant {

  /**
   * The regular expression to verify the conformity with ISO 8859-1 for address name (digits OK).
   */
  String REGEX_ISO8859_ADDRESS = "^[-a-zA-Z0-9'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïñòóôõöùúûüýÿ. ]+";

  /** The regular expression to verify the conformity with ISO 8859-1 for cities (digits KO). */
  String REGEX_ISO8859_CITY = "^[-a-zA-Z0-9'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïñòóôõöùúûüýÿ ]+";

  /** Max length for input trackId. */
  int ID_TRACK_MAX_LENGTH = 18;

  /** Max length for input recipient name. */
  int RECIPIENT_MAX_LENGTH = 125;

  /** Max length for input recipient address. */
  int ADDRESS_MAX_LENGTH = 75;

  /** Max length for input postal code. */
  int POSTALCODE_MAX_LENGTH = 10;

  /** Max length for input city. */
  int CITY_MAX_LENGTH = 55;

  /** Maximum upload size in bytes by default. */
  int MAX_UPLOAD_FILE_SIZE = 10000000;

  /** Maximum number of recipients email addresses allowed by default. */
  int NUMBER_MAX_RECIPIENT_EMAIL = 5;
}
