/**
 * 
 */
package fr.ge.exchange.bean;

import org.apache.commons.lang3.StringUtils;

import fr.ge.core.bean.UserBean;

/**
 * Bean representing an Exchange user.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ExchangeUserBean extends UserBean {

    /** Civility. **/
    private String civilite;

    /** last name. **/
    private String nom;

    /** Fist name. **/
    private String prenom;

    /** Email address. **/
    private String email;

    /**
     * Anonymous user (not registred in account </br>
     * Used for nash instances that allows anonymous users
     */
    private boolean anonymous;

    /**
     * Constructeur de la classe.
     *
     * @param identifiant
     *            : identifiant de l'utilisateur
     */
    public ExchangeUserBean(final String identifiant) {
        super(identifiant);
    }

    /**
     * Accesseur sur l'attribut {@link #civilite}.
     *
     * @return String civilite
     */
    public String getCivilite() {
        return civilite;
    }

    /**
     * Mutateur sur l'attribut {@link #civilite}.
     *
     * @param civilite
     *            la nouvelle valeur de l'attribut civilite
     */
    public ExchangeUserBean setCivilite(final String civilite) {
        this.civilite = civilite;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #nom}.
     *
     * @return String nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Mutateur sur l'attribut {@link #nom}.
     *
     * @param nom
     *            la nouvelle valeur de l'attribut nom
     */
    public ExchangeUserBean setNom(final String nom) {
        this.nom = nom;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #prenom}.
     *
     * @return String prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Mutateur sur l'attribut {@link #prenom}.
     *
     * @param prenom
     *            la nouvelle valeur de l'attribut prenom
     */
    public ExchangeUserBean setPrenom(final String prenom) {
        this.prenom = prenom;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #email}.
     *
     * @return String email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Mutateur sur l'attribut {@link #email}.
     *
     * @param email
     *            la nouvelle valeur de l'attribut email
     */
    public ExchangeUserBean setEmail(final String email) {
        this.email = email;
        return this;
    }

    /**
     * @return the anonymous
     */
    public boolean isAnonymous() {
        return anonymous;
    }

    /**
     * @param anonymous
     *            the anonymous to set
     */
    public ExchangeUserBean setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
        return this;
    }

    @Override
    public String toString() {
        final String displayName = StringUtils.isNotEmpty(this.nom) ? this.nom : StringUtils.EMPTY;
        final String displayFirstName = StringUtils.isNotEmpty(this.prenom) ? this.prenom : StringUtils.EMPTY;
        return String.format("%s %s", displayName, displayFirstName);
    }

}
