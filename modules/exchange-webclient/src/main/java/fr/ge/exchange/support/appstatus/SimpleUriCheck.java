package fr.ge.exchange.support.appstatus;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;

import fr.ge.core.log.GestionnaireTrace;
import net.sf.appstatus.core.check.AbstractCheck;
import net.sf.appstatus.core.check.CheckResultBuilder;
import net.sf.appstatus.core.check.ICheckResult;

/**
 * Check that a simple GET request on a URI is possible.
 */
public class SimpleUriCheck extends AbstractCheck {

  /** La constante LOGGER_FONC. */
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  /** uri. */
  // URI à vérifier
  private String URI;

  /** service name. */
  // Nom du service
  private String serviceName;

  /**
   * Check uri status.
   * 
   * @return i check result
   */
  public ICheckResult checkStatus() {
    CheckResultBuilder result = new CheckResultBuilder();
    // Récupération nom et groupe du checker
    result.from(this);
    try {
      URL url = new URL(this.URI);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("GET");
      int responseCode = con.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        result.code(OK);
        result.resolutionSteps("Code HTTP : " + responseCode);
      } else {
        result.code(FATAL);
        result.resolutionSteps("Code HTTP : " + responseCode);
      }
    } catch (IOException e) {
      result.code(FATAL);
      result.resolutionSteps(e.toString());
      LOGGER_TECH.error("Impossible d'appeler le service " + serviceName + " sur l'URI " + URI, e);
    } catch (Exception e) {
      result.code(FATAL);
      result.resolutionSteps(e.toString());
      LOGGER_TECH.warn("Exception non IO retournée l'appel au service " + serviceName + " sur l'URI " + URI, e);
    }

    result.description("Accès HTTP au service " + this.serviceName);
    return result.build();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getGroup() {
    return "Services";
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getName() {
    return this.serviceName;
  }

  /**
   * Setter du nom du service.
   *
   * @param name
   *          la nouvelle valeur de l'attribut name
   */
  public void setName(final String name) {
    this.serviceName = name;
  }

  /**
   * Sette de l'URI.
   *
   * @param uRI
   *          la nouvelle valeur de l'attribut uri
   */
  public void setURI(final String uRI) {
    URI = uRI;
  }

}
