package fr.ge.exchange.validation.email;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import fr.ge.common.support.i18n.MessageReader;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.conf.ExchangeConfigurationWebBean;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;

/**
 * Validator for the EmailController.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component
public class EmailValidator implements Validator {

  /** La constante LOGGER_FONC. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  @Autowired
  private ExchangeConfigurationWebBean exchangeConfigurationWebBean;

  /**
   * Mutateur sur l'attribut {@link #exchangeConfigurationWebBean}.
   *
   * @param exchangeConfigurationWebBean
   *          la nouvelle valeur de l'attribut exchangeConfigurationWebBean
   */
  public void setExchangeConfigurationWebBean(ExchangeConfigurationWebBean exchangeConfigurationWebBean) {
    this.exchangeConfigurationWebBean = exchangeConfigurationWebBean;
  }

  /**
   * Validate mail.
   *
   * @param mail
   *          the email address
   * @return true if succeed
   */
  protected boolean isValid(final String mail) {
    return org.apache.commons.validator.EmailValidator.getInstance().isValid(mail);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean supports(@SuppressWarnings("rawtypes") final Class c) {
    return EmailSendBean.class.equals(c);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void validate(final Object target, final Errors errors) {
    LOGGER_FONC.debug("Email Validation");
    EmailSendBean emailSendBean = (EmailSendBean) target;
    this.validateSender(errors, emailSendBean);
    this.validateRecipients(errors, emailSendBean);
    this.validateObject(errors, emailSendBean);
    this.validateContent(errors, emailSendBean);
  }

  /**
   * Validate sender address.
   * 
   * @param errors
   *          The errors
   * @param emailSendBean
   *          The email information
   */
  private void validateSender(final Errors errors, final EmailSendBean emailSendBean) {
    if (StringUtils.isBlank(emailSendBean.getSender())) {
      LOGGER_FONC.warn("L'email est vide");
      ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sender",
        MessageReader.getReader().getFormatter().format("The sender email is required"));

    } else if (!isValid(emailSendBean.getSender())) {
      LOGGER_FONC.warn("L'email est invalide");
      errors.rejectValue("sender", "invalid.sender.email.address");
    }
  }

  /**
   * Validate recipients address.
   * 
   * @param errors
   *          The errors
   * @param emailSendBean
   *          The email information
   */
  private void validateRecipients(final Errors errors, final EmailSendBean emailSendBean) {
    if (StringUtils.isBlank(emailSendBean.getRecipient())) {
      LOGGER_FONC.warn("L'email est vide");
      ValidationUtils.rejectIfEmptyOrWhitespace(errors, "recipient",
        MessageReader.getReader().getFormatter().format("The recipient email address is required"));

    } else {
      // -->Verification du nombre de destinataire autorisés
      String[] recipientTab = emailSendBean.getRecipient().split(",");
      int maxNumber = Integer.valueOf(exchangeConfigurationWebBean.getMaxRecipientEmail());
      if (recipientTab.length > maxNumber) {
        errors.rejectValue("recipient",
          MessageReader.getReader().getFormatter().format("The number of recipient addresses has been reached"));
      } else {
        for (String recipient : recipientTab) {
          if (!isValid(recipient)) {
            LOGGER_FONC.warn("L'email est invalide");
            errors.rejectValue("recipient",
              MessageReader.getReader().getFormatter().format("The recipient email address is invalid"));
            break;
          }
        }
      }
    }
  }

  /**
   * Validate object email.
   * 
   * @param errors
   *          The errors
   * @param emailSendBean
   *          The email information
   */
  private void validateObject(final Errors errors, final EmailSendBean emailSendBean) {
    if (StringUtils.isBlank(emailSendBean.getObject())) {
      LOGGER_FONC.warn("L'objet est vide");
      ValidationUtils.rejectIfEmptyOrWhitespace(errors, "object",
        MessageReader.getReader().getFormatter().format("The email object is required"));
    }
  }

  /**
   * Validate body content email.
   * 
   * @param errors
   *          The errors
   * @param emailSendBean
   *          The email information
   */
  private void validateContent(final Errors errors, final EmailSendBean emailSendBean) {
    if (StringUtils.isBlank(emailSendBean.getContent())) {
      LOGGER_FONC.warn("Le corps est vide");
      ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "required.body");
    }
  }

}
