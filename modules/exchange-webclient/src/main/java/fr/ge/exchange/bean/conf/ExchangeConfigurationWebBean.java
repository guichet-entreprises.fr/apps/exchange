/**
 *
 */
package fr.ge.exchange.bean.conf;

import java.io.Serializable;
import java.util.Map;

/**
 * ExchangeConfigurationWebBean which contains different URLs.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ExchangeConfigurationWebBean implements Serializable {

    /** The serialVersionUID constant. */
    private static final long serialVersionUID = -1866859830898012512L;

    /** The URL that check if the exchange server is alive. */
    private String urlExchangeServerAlive;

    /** Maximum number of recipients email addresses allowed. */
    private String maxRecipientEmail;

    /** Static instance of the ExchangeConfigurationWeb bean. */
    private static ExchangeConfigurationWebBean instance = new ExchangeConfigurationWebBean();

    /** Template email for Exchange. **/
    private Map<String, String> templateEmail;

    /**
     * Retrieves the unique instance of the ExchangeConfigurationWebBean.
     *
     * @return the unique instance of the ExchangeConfigurationWebBean
     */
    public static ExchangeConfigurationWebBean getInstance() {
        return instance;
    }

    /**
     * Accesseur sur l'attribut {@link #urlExchangeServerAlive}.
     *
     * @return String urlAccountAlive
     */
    public String getUrlExchangeServerAlive() {
        return this.urlExchangeServerAlive;
    }

    /**
     * Mutateur sur l'attribut {@link #urlExchangeServerAlive}.
     *
     * @param urlExchangeServerAlive
     *            la nouvelle valeur de l'attribut urlExchangeServerAlive
     */
    public void setUrlExchangeServerAlive(final String urlExchangeServerAlive) {
        this.urlExchangeServerAlive = urlExchangeServerAlive;
    }

    /**
     * Accesseur sur l'attribut {@link #maxRecipientEmail}.
     *
     * @return String maxRecipientEmail
     */
    public String getMaxRecipientEmail() {
        return this.maxRecipientEmail;
    }

    /**
     * Mutateur sur l'attribut {@link #maxRecipientEmail}.
     *
     * @param maxRecipientEmail
     *            la nouvelle valeur de l'attribut maxRecipientEmail
     */
    public void setMaxRecipientEmail(final String maxRecipientEmail) {
        this.maxRecipientEmail = maxRecipientEmail;
    }

    /**
     * Accesseur sur l'attribut {@link #templateEmail}.
     *
     * @return TODO templateEmail c'est quoi?
     */
    public Map<String, String> getTemplateEmail() {
        return this.templateEmail;
    }

    /**
     * Mutateur sur l'attribut {@link #templateEmail}.
     *
     * @param templateEmail
     *            la nouvelle valeur de l'attribut templateEmail
     */
    public void setTemplateEmail(final Map<String, String> templateEmail) {
        this.templateEmail = templateEmail;
    }
}
