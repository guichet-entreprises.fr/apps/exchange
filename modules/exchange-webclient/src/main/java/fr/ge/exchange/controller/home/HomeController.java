/**
 * 
 */
package fr.ge.exchange.controller.home;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.WebContentGenerator;

/**
 * The home controller.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Controller
public class HomeController extends WebContentGenerator {

  /**
   * Display the home page for Exchange.
   *
   * @param model
   *          le model
   * @return le string
   * @throws Exception
   *           le exception
   */
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String displayHomePage(final Model model) throws Exception {
    return "redirect:/postmail";
  }
}
