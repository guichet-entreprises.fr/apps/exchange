/**
 * 
 */
package fr.ge.exchange.controller.email;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.support.WebContentGenerator;

import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.ExchangeUserBean;
import fr.ge.exchange.bean.conf.ExchangeConfigurationWebBean;
import fr.ge.exchange.context.ExchangeThreadContext;
import fr.ge.exchange.email.ws.v1.bean.EmailErrorBean;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;
import fr.ge.exchange.email.ws.v1.service.ISendEmailRestService;
import fr.ge.exchange.validation.email.EmailValidator;

/**
 * The email controller.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Controller
public class EmailController extends WebContentGenerator {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /** The Email REST Service. */
    @Autowired
    private ISendEmailRestService sendEmailRestService;

    /** Postmail validator. **/
    @Autowired
    private EmailValidator emailValidator;

    /** exchange configuration web bean. */
    @Autowired
    private ExchangeConfigurationWebBean exchangeConfigurationWebBean = ExchangeConfigurationWebBean.getInstance();

    /**
     * Display forms to send a new email.
     *
     * @param model
     *            le model
     * @return le string
     * @throws Exception
     *             le exception
     */
    @RequestMapping(value = "/email", method = RequestMethod.GET)
    public String displayEmailPage(final Model model) {
        ExchangeUserBean utilisateur = ExchangeThreadContext.getUser();
        EmailSendBean emailSend = new EmailSendBean();
        emailSend.setSender(utilisateur.getEmail());
        model.addAttribute("emailSend", emailSend);
        model.addAttribute("templateEmailList", exchangeConfigurationWebBean.getTemplateEmail());
        return "email/main";
    }

    /**
     * Send a new postmail for the Imprimerie Nationale.
     * 
     * @param emailSend
     *            the email information
     * @param model
     *            the model
     * @param errors
     *            the errors
     * @param file
     *            the attachment
     * @return the main page to display
     * @throws Exception
     *             Exception raised
     */
    @RequestMapping(value = "/email/send", method = RequestMethod.POST)
    public String send(@ModelAttribute("emailSend") final EmailSendBean emailSend, final Model model, final BindingResult errors,
            @RequestParam(value = "file", required = true) final CommonsMultipartFile file) {

        LOGGER_FONC.info("Contrôleur sendEmail : envoi email");
        try {
            ExchangeUserBean utilisateur = ExchangeThreadContext.getUser();
            emailSend.setSender(utilisateur.getEmail());
            emailSend.setAttachmentPDFName(file.getOriginalFilename());
            this.emailValidator.validate(emailSend, errors);
            this.checkPdfAttachment(errors, file.getInputStream());
            if (errors.hasErrors()) {
                LOGGER_FONC.warn("Le formulaire contiennent des erreurs");
                model.addAttribute("emailSend", emailSend);
                model.addAttribute("templateEmailList", exchangeConfigurationWebBean.getTemplateEmail());
                return "email/main";
            }
            Response response = sendEmailRestService.sendEmail(emailSend, file.getInputStream());
            if (response != null) {
                if (Status.OK.getStatusCode() == response.getStatus()) {
                    model.addAttribute("emailSend", emailSend);
                    return "email/success";
                }
                EmailErrorBean errorBean = (EmailErrorBean) response.readEntity(EmailErrorBean.class);
                model.addAttribute("emailError", errorBean);
            }
        } catch (Exception e) {
            // the last line of defense catch everything
            LOGGER_FONC.error(String.format("Unable to send the email to %s.", emailSend.getRecipient()), e);
            EmailErrorBean errorBean = new EmailErrorBean();
            errorBean.setErrorCode("XXX");
            errorBean.setErrorMessage(e.getMessage());
            model.addAttribute("emailError", errorBean);
        }
        return "email/error";
    }

    /**
     * Check if the attachment is a PDF and if the file is not bigger than the
     * size limit.
     *
     * @param errors
     *            errors
     * @param pdfInputStream
     *            pdf input stream
     */
    private void checkPdfAttachment(final BindingResult errors, final InputStream pdfInputStream) {
        try {
            PDDocument pdfDoc = PDDocument.load(pdfInputStream);
            File pdfFile = File.createTempFile("tmp_" + UUID.randomUUID().toString(), ".pdf");
            pdfDoc.save(pdfFile);
            pdfDoc.close();
            pdfFile.delete();
        } catch (IOException e) {
            LOGGER_FONC.warn("La pièce jointe n'est pas un fichier PDF valide");
            errors.addError(new FieldError("file", "file", "Le fichier doit être au format PDF"));
        }
    }

}
