/**
 * 
 */
package fr.ge.exchange.manager;

import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.manager.IAccountManager;
import fr.ge.exchange.bean.ExchangeUserBean;
import fr.ge.exchange.context.ExchangeThreadContext;

/**
 * AccountManager est la classe d'intégration du ct-authentification.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class AccountManager implements IAccountManager<ExchangeUserBean> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ExchangeUserBean getUserBean(final String idUser, final AccountUserBean accountUserBean) {
        ExchangeUserBean userBean = new ExchangeUserBean(idUser);
        if (null != accountUserBean) {
            userBean //
                    .setCivilite(accountUserBean.getCivility()) //
                    .setEmail(accountUserBean.getEmail()) //
                    .setNom(accountUserBean.getLastName()) //
                    .setPrenom(accountUserBean.getFirstName()) //
                    .setAnonymous(false);
        }

        return userBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void persistUserContext(final ExchangeUserBean userBean) {
        // Persistance dans le thread local
        ExchangeThreadContext.setUser(userBean);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanUserContext() {
        // on vide le context
        ExchangeThreadContext.unsetUser();
    }

}
