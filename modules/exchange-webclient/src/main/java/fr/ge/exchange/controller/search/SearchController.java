/**
 * 
 */
package fr.ge.exchange.controller.search;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.WebContentGenerator;

import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.postmail.ws.v1.bean.PostMailAddress;
import fr.ge.exchange.ws.model.MailMessageStatus;
import fr.ge.exchange.ws.rest.IMailRestService;

/**
 * The status controller.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Controller
public class SearchController extends WebContentGenerator {

  /** Le logger fonctionnel. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /** The PostMail REST Service. */
  @Autowired
  private IMailRestService mailRestService;

  /**
   * Display forms to search records.
   * 
   * @param model
   *          The model
   * @return The page
   * @throws Exception
   *           The exception raised
   */
  @RequestMapping(value = "/postmail/search", method = RequestMethod.GET)
  public String displaySearchPage(final Model model) throws Exception {
    model.addAttribute("listePostmail", new ArrayList < PostMailAddress >());
    model.addAttribute("notFound", false);
    return "postmail/search";
  }

  /**
   * Searching records.
   * 
   * @param model
   *          The model
   * @param trackerId
   *          Id track for searching
   * @return The page
   * @throws Exception
   *           The exception raised
   */
  @RequestMapping(value = "/postmail/search", method = RequestMethod.POST)
  public String search(final Model model, @RequestParam(value = "trackIdSearch") final String trackerId) {

    MailMessageStatus response = null;
    try {
      response = mailRestService.getStatus(trackerId);
    } catch (RuntimeException e) {
      // whatever happened, we could'nt get the ressource
      LOGGER_FONC.error(String.format("Unable to retrieve this ressource because of an error : %s.", trackerId), e);
      response = null;
    }

    if (response != null) {
      // -->Un résultat est retourné
      model.addAttribute("notFound", false);
      List < MailMessageStatus > listePostmailSatus = new ArrayList < MailMessageStatus >();
      listePostmailSatus.add(response);
      model.addAttribute("listePostmailStatus", listePostmailSatus);
      return "postmail/search";
    }
    // -->Pas de résultat
    model.addAttribute("notFound", true);
    model.addAttribute("listePostmailStatus", new ArrayList < MailMessageStatus >());
    return "postmail/search";
  }

}
