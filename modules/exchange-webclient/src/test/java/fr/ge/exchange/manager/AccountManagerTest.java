package fr.ge.exchange.manager;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.exchange.bean.ExchangeUserBean;
import fr.ge.exchange.context.ExchangeThreadContext;

public class AccountManagerTest {

    /** account manager. */
    @InjectMocks
    private AccountManager accountManager;

    /**
     * Injection of mocks.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Methode de test de getUserBean.
     */
    @Test
    public void getUserBean() {
        final AccountUserBean accountUserBean = new AccountUserBean();
        accountUserBean.setCivility("Monsieur");
        accountUserBean.setFirstName("John");
        accountUserBean.setLastName("DOE");
        accountUserBean.setEmail("john.doe@yopmail.com");
        accountUserBean.setLanguage("fr");
        accountUserBean.setPhone("+33612345678");
        accountUserBean.setAnonymous(false);

        ExchangeUserBean userFormsBean = accountManager.getUserBean("2016-11-VHB-LKD-55", accountUserBean);
        assertThat(userFormsBean.getCivilite()).isEqualTo("Monsieur");
        assertThat(userFormsBean.getEmail()).isEqualTo("john.doe@yopmail.com");
        assertThat(userFormsBean.getId()).isEqualTo("2016-11-VHB-LKD-55");
        assertThat(userFormsBean.getNom()).isEqualTo("DOE");
        assertThat(userFormsBean.getPrenom()).isEqualTo("John");
    }

    /**
     * Methode de test de persistUserContext.
     */
    @Test
    public void persistUserContext() {
        ExchangeUserBean userFormsBean = new ExchangeUserBean("2016-11-VHB-LKD-55");
        accountManager.persistUserContext(userFormsBean);
        assertThat(ExchangeThreadContext.getUser().getId()).isEqualTo("2016-11-VHB-LKD-55");
    }

    /**
     * Methode de test de cleanUserContext.
     */
    @Test
    public void cleanUserContext() {
        ExchangeUserBean userFormsBean = new ExchangeUserBean("2016-11-VHB-LKD-55");
        accountManager.persistUserContext(userFormsBean);
        accountManager.cleanUserContext();
        assertThat(ExchangeThreadContext.getUser()).isNull();
    }

}
