package fr.ge.exchange.validation.postmail;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import fr.ge.common.support.i18n.MessageReader;
import fr.ge.exchange.postmail.ws.v1.bean.PostMailAddress;

/**
 * Class PostmailValidatorTest.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class PostmailValidatorTest {

  /** Le validateur. */
  private PostmailValidator postmailValidator;

  /** Errors. **/
  private Errors errors;

  /**
   * Set up method.
   */
  @Before
  public void setUp() {
    postmailValidator = new PostmailValidator();
  }

  /**
   * Test support method.
   */
  @Test
  public void testSupports() {
    assertTrue(postmailValidator.supports(PostMailAddress.class));
  }

  /**
   * Validate.
   */
  @Test
  public void testValidateAll() {

    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "Mairie de Vincennes";
    String recipientAddress = "120 rue Diderot";
    String recipientPostalCode = "94300";
    String recipientCity = "Vincennes";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);

    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNull(errors.getFieldError("trackId"));
    assertNull(errors.getFieldError("recipientName"));
    assertNull(errors.getFieldError("addressName"));
    assertNull(errors.getFieldError("city"));
    assertNull(errors.getFieldError("postalCode"));
  }

  /**
   * Validate recipientName.
   */
  @Test
  public void testValidateRecipientName() {

    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "AZERTYUIOP¨_AZERTYUIOP_AZERTYUIOP_AZERTYUIOP1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
    String recipientAddress = "120 rue Diderot";
    String recipientPostalCode = "94300";
    String recipientCity = "Vincennes";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);

    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("recipientName"));
  }

  /**
   * Validate recipientNameCompl.
   */
  @Test
  public void testValidateRecipientNameCompl() {

    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "Mairie de Vincennes";
    String recipientNameCompl = "?AZERTYUIOP¨_AZERTYUIOP_AZERTYUIOP_AZERTYUIOP123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
    String recipientAddress = "120 rue Diderot";
    String recipientPostalCode = "94300";
    String recipientCity = "Vincennes";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setRecipientNameCompl(recipientNameCompl);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);

    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("recipientNameCompl"));
  }

  /**
   * Validate addressName.
   */
  @Test
  public void testValidateAddressName() {

    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "Mairie de Vincennes";
    String recipientNameCompl = "Mairie de Vincennes";
    String recipientAddress = "Adresse bon format '-Äéé 22";
    String recipientPostalCode = "94300";
    String recipientCity = "Vincennes";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setRecipientNameCompl(recipientNameCompl);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);

    // Cas nominal
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNull(errors.getFieldError("addressName"));
    // cas erreur format adresse
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postMailInfo.setAddressName("@");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("addressName"));
    assertTrue(errors.getFieldError("addressName").getCode()
      .equals(MessageReader.getReader().getFormatter().format("The city should not contain any special characters")));
    assertNull(errors.getFieldError("addressNameCompl"));

    // -->Cas erreur adresse complément
    postMailInfo = new PostMailAddress();
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setAddressNameCompl("Paris,");
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNull(errors.getFieldError("addressName"));
    assertNotNull(errors.getFieldError("addressNameCompl"));
    assertTrue(errors.getFieldError("addressNameCompl").getCode()
      .equals(MessageReader.getReader().getFormatter().format("The city should not contain any special characters")));

    // -->Cas erreur adresse complément
    postMailInfo = new PostMailAddress();
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("addressName"));

    // -->Cas erreur adresse complément
    postMailInfo = new PostMailAddress();
    postMailInfo.setAddressName("12 rue de Paris 12 rue de Paris 12 rue de Paris 12 rue de Paris~BAD");
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("addressName"));
  }

  /**
   * Validate addressNameCompl.
   */
  @Test
  public void testValidateAddressNameCompl() {

    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "Mairie de Vincennes";
    String recipientNameCompl = "Mairie de Vincennes";
    String recipientAddress = "120 rue Diderot";
    String recipientAddressCompl = "AZERTYUIOP¨_AZERTYUIOP_AZERTYUIOP_AZERTYUIOP";
    String recipientPostalCode = "94300";
    String recipientCity = "Vincennes";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setRecipientNameCompl(recipientNameCompl);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setAddressNameCompl(recipientAddressCompl);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);

    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("addressNameCompl"));
  }

  /**
   * Validate postal code.
   */
  @Test
  public void testValidatePostalCode() {

    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "Mairie de Vincennes";
    String recipientAddress = "120 rue Diderot";
    String recipientPostalCode = "12345678901";
    String recipientCity = "Vincennes";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);

    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("postalCode"));

    // -->Null postal code
    postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("postalCode"));
  }

  /**
   * Validate city.
   */
  @Test
  public void testValidateCity() {

    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "Mairie de Vincennes";
    String recipientAddress = "120 rue Diderot";
    String recipientPostalCode = "94300";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setPostalCode(recipientPostalCode);

    // test ville non renseignée
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("city"));
    String test = MessageReader.getReader().getFormatter().format("The city is required");

    assertTrue(errors.getFieldError("city").getCode().equals(test));

    // test ville pas bon format
    postMailInfo.setCity("Vincennes0~BAD");
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("city"));
    assertTrue(errors.getFieldError("city").getCode()
      .equals(MessageReader.getReader().getFormatter().format("The city should not contain any special characters")));

    // test nominal
    postMailInfo.setCity("Vincennes");
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNull(errors.getFieldError("city"));

    // test nominal
    postMailInfo.setCity("VincennesVincennesVincennesVincennesVincennesVincennesVincennesVincennes");
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("city"));
    assertTrue(errors.getFieldError("city").getCode()
      .equals(MessageReader.getReader().getFormatter().format("The maximum length for the city is 55")));
  }

  /**
   * Validate wrong addresses.
   */
  @Test
  public void testValidateWrongAddressName() {

    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "Mairie de Vincennes";
    String addressName = "52 rue diderot";
    String addressNameCompl = "????";
    String recipientPostalCode = "94300";
    String recipientCity = "Vincennes";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);
    postMailInfo.setAddressName(addressName);
    postMailInfo.setAddressNameCompl(addressNameCompl);

    // cas erreur format adresse compl
    errors = new BeanPropertyBindingResult(postMailInfo, "postmailSend");
    postmailValidator.validate(postMailInfo, errors);
    assertNotNull(errors.getFieldError("addressNameCompl"));
    assertTrue(errors.getFieldError("addressNameCompl").getCode()
      .equals(MessageReader.getReader().getFormatter().format("The city should not contain any special characters")));
  }
}
