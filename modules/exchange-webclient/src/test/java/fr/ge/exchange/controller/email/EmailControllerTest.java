package fr.ge.exchange.controller.email;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import fr.ge.exchange.bean.conf.ExchangeConfigurationWebBean;
import fr.ge.exchange.context.ExchangeUserBeanMock;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;
import fr.ge.exchange.email.ws.v1.service.ISendEmailRestService;
import fr.ge.exchange.validation.email.EmailValidator;

/**
 * Testing EmailController.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class EmailControllerTest {

    /** Le controller EmailController. **/
    @InjectMocks
    private EmailController controller;

    /** The sendEmail rest service. */
    @Mock
    private ISendEmailRestService sendEmailRestService;

    /** Le validateur Email. **/
    @Mock
    private final EmailValidator emailValidator = Mockito.mock(EmailValidator.class);

    /** attachment pdf. */
    @Mock
    private final CommonsMultipartFile attachmentPdf = Mockito.mock(CommonsMultipartFile.class);

    /** exchange configuration web bean. */
    @Mock
    private final ExchangeConfigurationWebBean exchangeConfigurationWebBean = Mockito.mock(ExchangeConfigurationWebBean.class);

    /**
     * Injection of mocks in controller.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        final InputStream templateStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("pdf/attachmentTest.pdf");
        // File file = new File(this.getClass().getResource("/").getFile() +
        // "pdf/attachmentTest.pdf");
        // InputStream templateStream = new FileInputStream(file);
        Mockito.when(this.attachmentPdf.getInputStream()).thenReturn(templateStream);
        ExchangeUserBeanMock.init("1");
    }

    /**
     * Test display email page.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDisplayEmailPage() throws Exception {
        final Model model = Mockito.mock(Model.class);
        final String actual = this.controller.displayEmailPage(model);
        assertEquals(actual, "email/main");
    }

    /**
     * Test send email with HTTP status code 200.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSendEmail() throws Exception {
        final String sender = "exchange@yopmail";
        final String recipient = "exchange1@yopmail.com";
        final String object = "Test JUNIT validator";
        final String content = "Test avec succès";

        final EmailSendBean emailSendBean = new EmailSendBean();
        emailSendBean.setSender(sender);
        emailSendBean.setRecipient(recipient);
        emailSendBean.setObject(object);
        emailSendBean.setContent(content);

        final BindingResult result = Mockito.mock(BindingResult.class);
        final Model model = Mockito.mock(Model.class);
        final Response response = Mockito.mock(Response.class);
        Mockito.when(response.getStatus()).thenReturn(200);
        Mockito.when(this.sendEmailRestService.sendEmail(Matchers.any(EmailSendBean.class), Matchers.any(InputStream.class))).thenReturn(response);

        Mockito.doNothing().when(this.emailValidator).validate(Matchers.any(), Matchers.any(Errors.class));

        final String actual = this.controller.send(emailSendBean, model, result, this.attachmentPdf);
        assertEquals("email/success", actual);
    }

    /**
     * Test send email with HTTP status code 400.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSendEmailErrorValidation() throws Exception {
        final Model model = Mockito.mock(Model.class);
        final String recipient = "exchange1@yopmail.com";
        final String object = "Test JUNIT validator";
        final String content = "Test avec succès";

        final EmailSendBean emailSendBean = new EmailSendBean();
        emailSendBean.setRecipient(recipient);
        emailSendBean.setObject(object);
        emailSendBean.setContent(content);

        final Response response = Mockito.mock(Response.class);
        Mockito.when(response.getStatus()).thenReturn(400);

        Mockito.when(this.sendEmailRestService.sendEmail(Matchers.any(EmailSendBean.class), Matchers.any(InputStream.class))).thenReturn(response);

        final BindingResult errors = Mockito.mock(BindingResult.class);
        Mockito.when(errors.hasErrors()).thenReturn(true);

        final String actual = this.controller.send(emailSendBean, model, errors, this.attachmentPdf);
        // oddly the errors are displayed on the main page (LAB : can't
        // undestand that point)
        assertEquals("email/main", actual);
    }

    /**
     * Test send email with HTTP status code 400.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSendEmail400() throws Exception {
        final Model model = Mockito.mock(Model.class);
        final String sender = "exchange@yopmail";
        final String recipient = "exchange1@yopmail.com";
        final String object = "Test JUNIT validator";
        final String content = "Test avec succès";

        final EmailSendBean emailSendBean = new EmailSendBean();
        emailSendBean.setRecipient(recipient);
        emailSendBean.setObject(object);
        emailSendBean.setContent(content);
        emailSendBean.setSender(sender);

        final Response response = Mockito.mock(Response.class);
        Mockito.when(response.getStatus()).thenReturn(400);

        Mockito.when(this.sendEmailRestService.sendEmail(Matchers.any(EmailSendBean.class), Matchers.any(InputStream.class))).thenReturn(response);

        final BindingResult errors = Mockito.mock(BindingResult.class);
        Mockito.when(errors.hasErrors()).thenReturn(false);

        final String actual = this.controller.send(emailSendBean, model, errors, this.attachmentPdf);
        assertEquals(actual, "email/error");
    }

    /**
     * Test send email with null response.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSendEmailWithNullResponse() throws Exception {
        final String sender = "exchange@yopmail";
        final String recipient = "exchange1@yopmail.com";
        final String object = "Test JUNIT validator";
        final String content = "Test avec succès";

        final EmailSendBean emailSendBean = new EmailSendBean();
        emailSendBean.setSender(sender);
        emailSendBean.setRecipient(recipient);
        emailSendBean.setObject(object);
        emailSendBean.setContent(content);

        final BindingResult result = Mockito.mock(BindingResult.class);
        final Model model = Mockito.mock(Model.class);
        Mockito.when(this.sendEmailRestService.sendEmail(Matchers.any(EmailSendBean.class), Matchers.any(InputStream.class))).thenReturn(null);
        Mockito.doNothing().when(this.emailValidator).validate(Matchers.any(), Matchers.any(Errors.class));

        final String actual = this.controller.send(emailSendBean, model, result, this.attachmentPdf);
        assertEquals(actual, "email/error");
    }

}
