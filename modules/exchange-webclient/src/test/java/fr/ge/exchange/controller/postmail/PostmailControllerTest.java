package fr.ge.exchange.controller.postmail;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import fr.ge.exchange.postmail.ws.v1.bean.PostMailAddress;
import fr.ge.exchange.postmail.ws.v1.bean.PostMailErrorBean;
import fr.ge.exchange.validation.postmail.PostmailValidator;
import fr.ge.exchange.ws.rest.IMailRestService;
import fr.ge.tracker.facade.ITrackerFacade;
import fr.ge.tracker.facade.impl.SimpleTrackerFacadeImpl;

/**
 * Testing PostmailController.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class PostmailControllerTest {

  /** Le controller PostmailController. **/
  @InjectMocks
  private PostmailController controller;

  /** The Interface ITrackerFacade. **/
  @Mock
  private ITrackerFacade trackerFacade = Mockito.mock(SimpleTrackerFacadeImpl.class);

  /** The mail rest service. */
  @Mock
  private IMailRestService mailRestService;

  /** The PostmailValidator. **/
  @Mock
  private PostmailValidator postmailValidator = Mockito.mock(PostmailValidator.class);

  /**
   * Injection of mocks in controller.
   *
   * @throws Exception
   *           exception
   */
  @Before
  public void setup() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test home page.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testHomePage() throws Exception {
    Model model = Mockito.mock(Model.class);
    String actual = controller.displayPostmailPage(model);
    assertEquals(actual, "postmail/main");
  }

  /**
   * Test send postmail with HTTP status code 200.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSendPostmail200() throws Exception {
    Model model = Mockito.mock(Model.class);
    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "Mairie de Vincennes";
    String recipientAddress = "120 rue Diderot";
    String recipientPostalCode = "94300";
    String recipientCity = "Vincennes";

    CommonsMultipartFile attachmentPdf = Mockito.mock(CommonsMultipartFile.class);

    Mockito.when(attachmentPdf.getInputStream()).thenReturn(null);

    Mockito.when(mailRestService.send(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
      Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.any(InputStream.class))).thenReturn(trackId);

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(null);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);

    BindingResult result = Mockito.mock(BindingResult.class);

    String actual = controller.sendPostmail(postMailInfo, model, result, attachmentPdf);
    assertEquals(actual, "postmail/success");
  }

  /**
   * Test send postmail with HTTP status code 400.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSendPostmail400() throws Exception {
    Model model = Mockito.mock(Model.class);
    String trackId = "2017-02-ABC-DEF-00";
    String recipientAddress = "120 rue Diderot";
    String recipientName = "Mairie de Vincennes";
    String recipientPostalCode = "94300";
    String recipientCity = "Vincennes";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);

    CommonsMultipartFile attachmentPdf = Mockito.mock(CommonsMultipartFile.class);

    Mockito.when(attachmentPdf.getInputStream()).thenReturn(null);

    Mockito.when(mailRestService.send(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
      Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.any(InputStream.class))).thenReturn(null);

    BindingResult result = Mockito.mock(BindingResult.class);

    String actual = controller.sendPostmail(postMailInfo, model, result, attachmentPdf);
    assertEquals(actual, "postmail/error");
  }

  /**
   * Test display success postmail page.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testDisplaySuccessPostmail() throws Exception {
    Model model = Mockito.mock(Model.class);
    PostMailAddress postMailSend = Mockito.mock(PostMailAddress.class);
    String actual = controller.displaySuccessPostmail(postMailSend, model);
    assertEquals(actual, "postmail/success");
  }

  /**
   * Test display error postmail page.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testDisplayErrorPostmail() throws Exception {
    Model model = Mockito.mock(Model.class);
    PostMailErrorBean errorBean = Mockito.mock(PostMailErrorBean.class);
    String actual = controller.displayErrorPostmail(errorBean, model);
    assertEquals(actual, "postmail/error");
  }

  /**
   * Test send postmail with validation errors.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSendPostmailWithErrors() throws Exception {
    Model model = Mockito.mock(Model.class);
    String trackId = "2017-02-ABC-DEF-00";
    String recipientAddress = "120 rue Diderot";
    String recipientName = "Mairie de Vincennes";
    String recipientPostalCode = "1234567890";
    String recipientCity = "Vincennes";

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);

    CommonsMultipartFile attachmentPdf = Mockito.mock(CommonsMultipartFile.class);
    Mockito.when(attachmentPdf.getInputStream()).thenReturn(null);

    BindingResult result = Mockito.mock(BindingResult.class);
    Mockito.when(result.hasErrors()).thenReturn(true);
    String actual = controller.sendPostmail(postMailInfo, model, result, attachmentPdf);
    assertEquals(actual, "postmail/main");
  }

  /**
   * Test send postmail with null response.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSendPostmailNullResponse() throws Exception {
    Model model = Mockito.mock(Model.class);
    String trackId = "2017-02-ABC-DEF-00";
    String recipientName = "Mairie de Vincennes";
    String recipientAddress = "120 rue Diderot";
    String recipientPostalCode = "94300";
    String recipientCity = "Vincennes";

    CommonsMultipartFile attachmentPdf = Mockito.mock(CommonsMultipartFile.class);

    Mockito.when(attachmentPdf.getInputStream()).thenReturn(null);

    Response response = Mockito.mock(Response.class);
    Mockito.when(response.getStatus()).thenReturn(200);

    // Mockito.when(postmailRestService.sendPostMail(Matchers.anyString(),
    // Matchers.any(PostMailAddress.class),
    // Matchers.any(InputStream.class))).thenReturn(null);
    Mockito.when(mailRestService.send(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
      Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.any(InputStream.class))).thenReturn(null);

    PostMailAddress postMailInfo = new PostMailAddress();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAddressName(recipientAddress);
    postMailInfo.setCity(recipientCity);
    postMailInfo.setPostalCode(recipientPostalCode);
    BindingResult result = Mockito.mock(BindingResult.class);

    String actual = controller.sendPostmail(postMailInfo, model, result, attachmentPdf);
    assertEquals(actual, "postmail/error");
  }

}
