package fr.ge.exchange.context;

import static org.fest.assertions.Assertions.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;

import fr.ge.exchange.bean.ExchangeUserBean;

public class ExchangeThreadContextTest {

  /**
   * La constante thread local.
   */
  public static final ThreadLocal < ExchangeUserBean > LOCAL_USER = new ThreadLocal < ExchangeUserBean >();

  /**
   * Methode de test de l'attribut de la class.
   * 
   * @throws Exception
   *           : Exception
   */
  @Test
  public void classTest() throws Exception {
    final Constructor < ? > constructor = ExchangeThreadContext.class.getDeclaredConstructor();
    assertThat(constructor.isAccessible()).isFalse();
    assertThat(Modifier.isPrivate(constructor.getModifiers())).isTrue();
    constructor.setAccessible(true);
    constructor.newInstance();
    assertThat(constructor.isAccessible()).isTrue();
    constructor.setAccessible(false);
  }

  /**
   * Methode de test global pour GentThreadContext.
   *
   */
  @Test
  public void initTest() {

    // on verifie que le context
    ExchangeUserBean userFormsBean = new ExchangeUserBean("1");
    ExchangeThreadContext.setUser(userFormsBean);

    assertThat(ExchangeThreadContext.getUser().getId()).isEqualTo("1");

    // on verifie qu'il est bien supprime du contexte
    ExchangeThreadContext.unsetUser();
    assertThat(ExchangeThreadContext.getUser()).isNull();
  }

}
