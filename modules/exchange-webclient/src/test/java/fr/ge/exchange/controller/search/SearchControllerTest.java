package fr.ge.exchange.controller.search;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import fr.ge.exchange.ws.model.MailMessageStatus;
import fr.ge.exchange.ws.rest.IMailRestService;

/**
 * Testing StatusController.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class SearchControllerTest {

  /** Le controller SearchController. **/
  @InjectMocks
  private SearchController controller;

  /** The postmail rest service. */
  @Mock
  private IMailRestService mailRestService;

  /**
   * Injection of mocks in controller.
   *
   * @throws Exception
   *           exception
   */
  @Before
  public void setup() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test display search page.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testDisplaySearchPage() throws Exception {
    Model model = Mockito.mock(Model.class);
    String actual = controller.displaySearchPage(model);
    assertEquals(actual, "postmail/search");
  }

  /**
   * Test search.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSearch() throws Exception {
    Model model = Mockito.mock(Model.class);
    String trackId = "2017-02-ABC-DEF-00";
    MailMessageStatus postMailStatusBean = Mockito.mock(MailMessageStatus.class);
    Mockito.when(mailRestService.getStatus(trackId)).thenReturn(postMailStatusBean);
    String actual = controller.search(model, trackId);
    assertEquals(actual, "postmail/search");
  }

  /**
   * Test fail search.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testFailSearch() throws Exception {
    Model model = Mockito.mock(Model.class);
    String trackId = "2017-02-ABC-DEF-00";
    MailMessageStatus postMailStatusBean = Mockito.mock(MailMessageStatus.class);
    Mockito.when(mailRestService.getStatus(trackId)).thenReturn(postMailStatusBean);
    String actual = controller.search(model, trackId);
    assertEquals(actual, "postmail/search");
  }

  /**
   * Test fail search with null response.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testFailSearchNullResponse() throws Exception {
    Model model = Mockito.mock(Model.class);
    String trackId = "2017-02-ABC-DEF-00";
    Mockito.when(mailRestService.getStatus(trackId)).thenReturn(null);
    String actual = controller.search(model, trackId);
    assertEquals(actual, "postmail/search");
  }

}
