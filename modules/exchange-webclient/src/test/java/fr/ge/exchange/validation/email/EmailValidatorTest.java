package fr.ge.exchange.validation.email;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import fr.ge.exchange.bean.conf.ExchangeConfigurationWebBean;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;

/**
 * Class EmailValidatorTest.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class EmailValidatorTest {

  /** Le validateur. */
  private EmailValidator validator;

  /** Errors. **/
  private Errors errors;

  @Mock
  private ExchangeConfigurationWebBean exchangeConfigurationWebBean = Mockito.mock(ExchangeConfigurationWebBean.class);

  /**
   * Set up function.
   */
  @Before
  public void setUp() {
    validator = new EmailValidator();
    validator.setExchangeConfigurationWebBean(exchangeConfigurationWebBean);
    Mockito.when(exchangeConfigurationWebBean.getMaxRecipientEmail()).thenReturn("5");
  }

  /**
   * Test support method.
   */
  @Test
  public void testSupports() {
    assertTrue(validator.supports(EmailSendBean.class));
  }

  /**
   * Validate all fields.
   */
  @Test
  public void testValidateAll() {

    String sender = "exchange@yopmail.com";
    String recipient = "exchange1@yopmail.com";
    String object = "Test JUNIT validator";
    String content = "Test avec succès";
    FileInputStream file = Mockito.mock(FileInputStream.class);

    EmailSendBean emailSendBean = new EmailSendBean();
    emailSendBean.setSender(sender);
    emailSendBean.setRecipient(recipient);
    emailSendBean.setObject(object);
    emailSendBean.setContent(content);
    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNull(errors.getFieldError("sender"));
    assertNull(errors.getFieldError("recipient"));
    assertNull(errors.getFieldError("emailObject"));
    assertNull(errors.getFieldError("content"));
  }

  /**
   * Validate sender address with errors.
   */
  @Test
  public void testValidateSenderAddressWithError() {

    String sender = "exchange@yopmail";
    String recipient = "exchange1@yopmail.com";
    String object = "Test JUNIT validator";
    String content = "Test avec succès";
    FileInputStream file = Mockito.mock(FileInputStream.class);

    EmailSendBean emailSendBean = new EmailSendBean();
    emailSendBean.setSender(sender);
    emailSendBean.setRecipient(recipient);
    emailSendBean.setObject(object);
    emailSendBean.setContent(content);

    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("sender"));

    emailSendBean.setSender(null);
    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("sender"));
  }

  /**
   * Validate recipient address with errors.
   */
  @Test
  public void testValidateRecipientAddressWithError() {

    String sender = "exchange@yopmail.com";
    String recipient = "exchange1@yopmail";
    String object = "Test JUNIT validator";
    String content = "Test avec succès";
    FileInputStream file = Mockito.mock(FileInputStream.class);

    EmailSendBean emailSendBean = new EmailSendBean();
    emailSendBean.setSender(sender);
    emailSendBean.setRecipient(recipient);
    emailSendBean.setObject(object);
    emailSendBean.setContent(content);

    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("recipient"));

    emailSendBean.setRecipient(null);
    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("recipient"));
  }

  /**
   * Validate object with errors.
   */
  @Test
  public void testValidateObjectWithError() {

    String sender = "exchange@yopmail.com";
    String recipient = "exchange1@yopmail";
    String object = "";
    String content = "Test avec succès";
    FileInputStream file = Mockito.mock(FileInputStream.class);

    EmailSendBean emailSendBean = new EmailSendBean();
    emailSendBean.setSender(sender);
    emailSendBean.setRecipient(recipient);
    emailSendBean.setObject(object);
    emailSendBean.setContent(content);

    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("object"));

    emailSendBean.setObject(null);
    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("object"));
  }

  /**
   * Validate content with errors.
   */
  @Test
  public void testValidateContentWithError() {

    String sender = "exchange@yopmail.com";
    String recipient = "exchange1@yopmail";
    String object = "Test JUNIT validator";
    String content = "";
    FileInputStream file = Mockito.mock(FileInputStream.class);

    EmailSendBean emailSendBean = new EmailSendBean();
    emailSendBean.setSender(sender);
    emailSendBean.setRecipient(recipient);
    emailSendBean.setObject(object);
    emailSendBean.setContent(content);

    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("content"));

    emailSendBean.setContent(null);
    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("content"));
  }

  /**
   * Validate recipient address with errors.
   */
  @Test
  public void testValidateMultipleRecipientAddress() {

    String sender = "exchange@yopmail.com";
    String recipient = "exchange1@yopmail.com; exchange2@yopmail.com";
    String object = "Test JUNIT validator";
    String content = "Test avec succès";
    FileInputStream file = Mockito.mock(FileInputStream.class);

    EmailSendBean emailSendBean = new EmailSendBean();
    emailSendBean.setSender(sender);
    emailSendBean.setRecipient(recipient);
    emailSendBean.setObject(object);
    emailSendBean.setContent(content);

    // -->Validation failed
    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("recipient"));

    // -->Validation correct
    recipient = "exchange1@yopmail.com, exchange2@yopmail.com";
    emailSendBean.setRecipient(recipient);
    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNull(errors.getFieldError("recipient"));

    // -->Validation failed because of max recipient email raised
    recipient = "a@yopmail.com, b@yopmail.com, c@yopmail.com, d@yopmail.com, e@yopmail.com, aa@yopmail.com, bb@yopmail.com, cc@yopmail.com, dd@yopmail.com, ee@yopmail.com";
    emailSendBean.setRecipient(recipient);
    errors = new BeanPropertyBindingResult(emailSendBean, "emailSend");
    validator.validate(emailSendBean, errors);
    assertNotNull(errors.getFieldError("recipient"));
  }

}
