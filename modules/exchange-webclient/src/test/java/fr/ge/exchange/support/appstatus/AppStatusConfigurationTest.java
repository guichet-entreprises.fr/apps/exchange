/**
 * 
 */
package fr.ge.exchange.support.appstatus;

import static org.fest.assertions.Assertions.assertThat;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import net.sf.appstatus.core.AppStatus;
import net.sf.appstatus.core.check.ICheck;

/**
 * Check the AppStatus configuration.
 * 
 * @author $Author: LABEMONT $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/applicationContext-ge-exchange-webapp-config.xml" })
@WebAppConfiguration
public class AppStatusConfigurationTest {

  @Autowired
  private AppStatus appStatus;

  /**
   * Check that the URI server Checker is correctly parameterized.
   * 
   * @throws SecurityException
   *           introspection issue
   * @throws NoSuchFieldException
   *           introspection issue
   * @throws IllegalArgumentException
   *           introspection issue
   * @throws IllegalAccessException
   *           introspection issue
   */
  @Test
  public void checkAppStatusConfiguration()
    throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
    assertThat(appStatus.getServices()).isNotNull();

    Field checkersField = AppStatus.class.getDeclaredField("checkers");
    checkersField.setAccessible(true);
    @SuppressWarnings("unchecked")
    List < ICheck > checkers = (List < ICheck >) checkersField.get(appStatus);
    assertThat(checkers.size()).isEqualTo(4);
    boolean URICheckFound = false;
    for (ICheck checker : checkers) {
      if (checker instanceof SimpleUriCheck) {
        assertThat(checker.getName()).isEqualTo("Exchange WS Server");
        Field uriField = SimpleUriCheck.class.getDeclaredField("URI");
        uriField.setAccessible(true);
        assertThat(uriField.get(checker)).isEqualTo("http://localhost:8080/exchange-ws-server/status");
        URICheckFound = true;
      }
    }
    assertThat(URICheckFound).isTrue();

  }
}
