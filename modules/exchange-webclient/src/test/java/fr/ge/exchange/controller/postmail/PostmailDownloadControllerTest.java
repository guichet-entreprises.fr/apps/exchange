package fr.ge.exchange.controller.postmail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.exchange.ws.rest.IMailRestService;

/**
 * Testing PostmailController.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class PostmailDownloadControllerTest {

  /** Le controller PostmailController. **/
  @InjectMocks
  private PostmailController controller;

  /** The mail rest service. */
  @Mock
  private IMailRestService mailRestService;

  /**
   * Injection of mocks in controller.
   *
   * @throws Exception
   *           exception
   */
  @Before
  public void setup() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test download page.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testDownloadPage() throws Exception {

    HttpServletResponse httpResponse = Mockito.mock(HttpServletResponse.class);
    // ServletOutputStream servletOutputStream = Mockito.mock(ServletOutputStream.class);
    File tempFile = File.createTempFile("testDownloadPage", ".zip");
    FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
    ServletOutputStream servletOutputStream = new ServletOutputStream() {
      @Override
      public void write(int b) throws IOException {
        fileOutputStream.write(b);
      }

      @Override
      public void setWriteListener(WriteListener writeListener) {
        // TODO Auto-generated method stub
      }

      @Override
      public boolean isReady() {
        return true;
      }
    };

    String trackId = "2017-02-ABC-DEF-00";
    // InputStream fileInputStream = Thread.currentThread().getContextClassLoader()
    // .getResourceAsStream("datas/2017-02-ABC-DEF-00.zip");
    // String outputFile =
    // Thread.currentThread().getContextClassLoader().getResource("/datas/2017-02-ABC-DEF-00.zip").getFile();
    // String outputFile =
    // this.getClass().getClassLoader().getResource("datas/2017-02-ABC-DEF-00.zip").getFile();
    String outputFile = this.getClass().getClassLoader().getResource(".").getFile() + File.separator
      + "/datas/2017-02-ABC-DEF-00.zip";
    // String outputFile = PostmailDownloadControllerTest.class.getResource("/").getFile() +
    // "/datas/2017-02-ABC-DEF-00.zip";
    FileInputStream fileInputStream = new FileInputStream(outputFile);

    Response responseFile = Response.ok(fileInputStream).header(HttpHeaders.CONTENT_DISPOSITION, "2017-02-ABC-DEF-00.zip")
      .build();
    Mockito.when(mailRestService.download(trackId)).thenReturn(responseFile);
    Mockito.when(httpResponse.getOutputStream()).thenReturn(servletOutputStream);
    Mockito.doNothing().when(httpResponse).flushBuffer();

    // IOUtils ioUtils = Mockito.mock(IOUtils.class);
    // Mockito.when(IOUtils.copy(Matchers.any(InputStream.class), OutputStream.class))).;

    controller.download(trackId, httpResponse);

    fileOutputStream.close();
    Assert.assertTrue(FileUtils.sizeOf(tempFile) > 0);
  }

}
