/**
 * 
 */
package fr.ge.exchange.context;

import fr.ge.exchange.bean.ExchangeUserBean;

/**
 * Mock pour ExchangeUserBean.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public final class ExchangeUserBeanMock {

  /**
   * Constructeur de la classe.
   *
   */
  private ExchangeUserBeanMock() {
    super();
  }

  /**
   * Methode pour mocker facilement un utilisateur en ne passant que l'identifiant.
   * 
   * @param idUtilisateur
   *          : Identifiant de l'utilisateur
   */
  public static void init(final String idUtilisateur) {
    ExchangeUserBean userFormsBean = new ExchangeUserBean(idUtilisateur);
    ExchangeThreadContext.setUser(userFormsBean);
  }

  /**
   * Methode pour mocker facilement un utilisateur en passant toutes ses informations.
   * 
   * @param idUtilisateur
   *          : Identifiant de l'utilisateur
   * @param nom
   *          : nom de l'utilisateur
   * @param prenom
   *          : prenom de l'utilisateur
   * @param civilite
   *          : civilite de l'utilisateur
   * @param email
   *          : email de l'utilisateur
   * @param origine
   *          : Origine de l'utilisateur
   * @param validPhone
   *          : validPhone de l'utilisateur
   */
  public static void init(final String idUtilisateur, final String nom, final String prenom, final String civilite,
    final String email, final String origine, final Boolean validPhone) {
    ExchangeUserBean userFormsBean = new ExchangeUserBean(idUtilisateur);
    userFormsBean.setNom(nom);
    userFormsBean.setPrenom(prenom);
    userFormsBean.setCivilite(civilite);
    userFormsBean.setEmail(email);
    ExchangeThreadContext.setUser(userFormsBean);
  }
}
