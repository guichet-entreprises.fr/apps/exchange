package fr.ge.exchange.controller.home;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

/**
 * Testing HomeController.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class HomeControllerTest {

  /** Le controller HomeController. **/
  @InjectMocks
  private HomeController controller;

  /**
   * Injection of mocks in controller.
   *
   * @throws Exception
   *           exception
   */
  @Before
  public void setup() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test display home page.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testDisplayHomePage() throws Exception {
    Model model = Mockito.mock(Model.class);
    String actual = controller.displayHomePage(model);
    assertEquals(actual, "redirect:/postmail");
  }
}
