<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016) 
    This software is a computer program whose purpose is to maintain and administrate 
    standalone forms. This software is governed by the CeCILL license under French 
    law and abiding by the rules of distribution of free software. You can use, 
    modify and/ or redistribute the software under the terms of the CeCILL license 
    as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
    As a counterpart to the access to the source code and rights to copy, modify 
    and redistribute granted by the license, users are provided only with a limited 
    warranty and the software's author, the holder of the economic rights, and 
    the successive licensors have only limited liability. In this respect, the 
    user's attention is drawn to the risks associated with loading, using, modifying 
    and/or developing or reproducing the software by the user in light of its 
    specific status of free software, that may mean that it is complicated to 
    manipulate, and that also therefore means that it is reserved for developers 
    and experienced professionals having in-depth computer knowledge. Users are 
    therefore encouraged to load and test the software's suitability as regards 
    their requirements in conditions enabling the security of their systems and/or 
    data to be ensured and, more generally, to use and operate it in the same 
    conditions as regards security. The fact that you are presently reading this 
    means that you have had knowledge of the CeCILL license and that you accept 
    its terms. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>fr.ge.common.exchange</groupId>
        <artifactId>exchange</artifactId>
        <version>2.16.1.14-SNAPSHOT</version>
        <relativePath>../..</relativePath>
    </parent>

    <artifactId>exchange-webclient</artifactId>
    <packaging>war</packaging>
    <name>Exchange webclient</name>
    <description>Exchange private UI</description>

    <dependencies>
        <dependency>
            <groupId>fr.ge.common.layout</groupId>
            <artifactId>nash-layout</artifactId>
        </dependency>
		<dependency>
			<groupId>fr.ge.common.utils</groupId>
			<artifactId>common-web-utils</artifactId>
		</dependency>
        <dependency>
            <groupId>fr.ge.gent</groupId>
            <artifactId>tracker-client</artifactId>
        </dependency>
        <dependency>
            <groupId>fr.ge.core</groupId>
            <artifactId>ge-core</artifactId>
        </dependency>
        <dependency>
            <groupId>fr.ge.core</groupId>
            <artifactId>ge-core-web</artifactId>
        </dependency>
        <dependency>
            <groupId>fr.ge.ct</groupId>
            <artifactId>ct-authentification</artifactId>
        </dependency>
        <dependency>
            <groupId> fr.ge.common.i18n</groupId>
            <artifactId>ge-i18n-thymeleaf3</artifactId>
        </dependency>
        <dependency>
            <groupId>fr.ge.common.exchange</groupId>
            <artifactId>exchange-ws-contract</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-web</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework</groupId>
                    <artifactId>spring-beans</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
        </dependency>
        <dependency>
            <groupId>org.thymeleaf</groupId>
            <artifactId>thymeleaf-spring4</artifactId>
        </dependency>
        <dependency>
            <groupId>org.thymeleaf.extras</groupId>
            <artifactId>thymeleaf-extras-springsecurity4</artifactId>
        </dependency>
        <dependency>
            <groupId>cglib</groupId>
            <artifactId>cglib</artifactId>
        </dependency>
        <dependency>
            <groupId>org.pegdown</groupId>
            <artifactId>pegdown</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.ow2.asm</groupId>
                    <artifactId>asm</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>net.sf.appstatus</groupId>
            <artifactId>appstatus-web</artifactId>
        </dependency>
        <dependency>
            <groupId>net.sf.appstatus</groupId>
            <artifactId>appstatus-services-inprocess</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
        </dependency>
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.thymeleaf</groupId>
            <artifactId>thymeleaf-testing</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-rs-client</artifactId>
        </dependency>
        <dependency>
            <groupId>oro</groupId>
            <artifactId>oro</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-validator</groupId>
            <artifactId>commons-validator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.pdfbox</groupId>
            <artifactId>pdfbox</artifactId>
        </dependency>
        <dependency>
            <groupId>org.easytesting</groupId>
            <artifactId>fest-assert</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Logging -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
             <groupId>org.apache.logging.log4j</groupId>
             <artifactId>log4j-slf4j-impl</artifactId>
             <scope>runtime</scope>
        </dependency>
        <dependency>
             <groupId>org.apache.logging.log4j</groupId>
             <artifactId>log4j-api</artifactId>
             <scope>runtime</scope>
        </dependency>
        <dependency>
             <groupId>org.apache.logging.log4j</groupId>
             <artifactId>log4j-core</artifactId>
             <scope>runtime</scope>
        </dependency>
    </dependencies>

    <build>
        <!--
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <excludes>
                    <exclude>public/css/mixins/**</exclude>
                    <exclude>public/**/*.less</exclude>
                    <exclude>public/js/external/**</exclude>
                    <exclude>views/**/*.html</exclude>
                </excludes>
            </resource>
        </resources>
         -->
        <plugins>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <configuration>
                    <packagingExcludes>css/mixins/**, **/*.less</packagingExcludes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.github.eirslett</groupId>
                <artifactId>frontend-maven-plugin</artifactId>
                <configuration>
                    <srcdir>${basedir}/src/main/resources/public</srcdir>
                </configuration>
                <executions>
                    <execution>
                        <id>frontend-initialisation</id>
                        <goals>
                            <goal>install-node-and-npm</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>frontend-npm-install</id>
                        <goals>
                            <goal>npm</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>frontend-grunt</id>
                        <goals>
                            <goal>grunt</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
