CREATE TABLE postmail
( 
	trackerId varchar(18) NOT NULL PRIMARY KEY, 			--Track id
	deliveryId varchar(255), 								--Delivery id
	deliveryModeId varchar(25), 							--Delivery mode id
	deliveryCode varchar(5),  								--Delivery code 
	recipientname varchar(250) NOT NULL, 					--Recipient name
	recipientaddress varchar(150) NOT NULL, 				--Recipient address
	recipientpostalcode varchar(10) NOT NULL,				--Recipient postal code
	recipientcity varchar(55) NOT NULL,						--Recipient city
	attachmentpath varchar(255),							--Attachment name path
	datepostmail timestamp(6) DEFAULT current_timestamp,  	--Date and time of postmail
	numberpages bigint,										--Number of pages
	channel varchar(25),   									--The channel used
	urgency varchar(25),									--The urgency of the postmail
	numreco varchar(50),									--Reco Number 
	status varchar(25)										--Postmail status 
);

CREATE TABLE email
( 
	id bigserial NOT NULL PRIMARY KEY, 					--Email id
	sender varchar(255), 								--Sender email addresse
	recipient varchar(255), 							--Recipient email addresse
	object varchar(255), 								--Object or subject email
	dateemail timestamp(6) DEFAULT current_timestamp,  	--Date and time of the email
	status varchar(18)									--Email status 
);
