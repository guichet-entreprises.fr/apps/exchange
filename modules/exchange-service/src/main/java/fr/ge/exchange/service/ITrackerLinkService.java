/**
 * 
 */
package fr.ge.exchange.service;

import fr.ge.core.exception.TechniqueException;

/**
 * Interface for services to call Tracker.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface ITrackerLinkService {

    /**
     * Adds the post mail event to tracker.
     *
     * @param trackId
     *            track id
     * @param eventCode
     *            event code
     */
    void addPostMailEvent(final String trackId, final String eventCode);

    /**
     * Generation a tracker id, could be linked to a reference id.
     *
     * @param referenceId
     *            reference id to link the new tracker id to, nullable
     * @return the final tracker id, not null
     * @throws TechniqueException
     *             technique exception if the tracker id could not be generated
     */
    String createTrackerId(final String referenceId) throws TechniqueException;

    /**
     * Adds the update event to tracker.
     * 
     * @param trackId
     *            track id
     * @param eventCode
     *            event code
     */
    void addUpdatePostmailStatuts(final String trackId, final String eventCode);

}
