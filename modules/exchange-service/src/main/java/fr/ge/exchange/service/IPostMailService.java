/**
 * 
 */
package fr.ge.exchange.service;

import java.io.InputStream;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.MailMessage;

/**
 * Interface of services to post mails to competent authorities.
 */
public interface IPostMailService {

  /**
   * Generates a pdf from the document and the post mail information. Then, deposit this pdf to a
   * SFTP server for the "Imprimerie National" to be send to the competent authority. This service
   * also inserts a line in the table exchange_postmail to indicate that a mail has been posted.
   * 
   * @param documentToPost
   *          document to post to competent authorities
   * @param mailMessage
   *          information to post the mail like recipient, address
   * @param referenceId
   *          the business id to reference
   * @param attachLocalStorageDirectory
   *          local directory where you store the generated zip
   * @param attachSftpStorageDirectory
   *          SFTP directory where you store the generated zip for the Imprimerie Nationale
   * @return the tracker Id created
   * @throws TechniqueException
   *           if a technical issue occurs
   */
  String postMail(final InputStream documentToPost, final MailMessage mailMessage, final String referenceId,
    final String attachLocalStorageDirectory, final String attachSftpStorageDirectory) throws TechniqueException;

  /**
   * Retrieve a mail message by its id
   * 
   * @param trackerId
   *          the id, nullable
   * @return the mail message, or null if not found
   * @throws TechniqueException
   *           if a technical issue occurs
   */
  MailMessage getMailMessage(final String trackerId) throws TechniqueException;

}
