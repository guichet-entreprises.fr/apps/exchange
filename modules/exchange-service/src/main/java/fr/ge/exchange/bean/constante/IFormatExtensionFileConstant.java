/**
 * 
 */
package fr.ge.exchange.bean.constante;

/**
 * Interface of all extensions and mime type.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface IFormatExtensionFileConstant {

  /** Mime type extension for pdf files. */
  String APPLICATION_PDF_MIME_TYPE = "application/pdf";

  /** Error code exception for invalid JSON format. */
  String ERR_EXCH_INVALID_PARAMETER = "ERR_EXCH_INVALID_PARAMETER";

  /** Error code exception for invalid PDF format. */
  String ERR_EXCH_INVALID_PDF = "ERR_EXCH_INVALID_PDF";

  /** Error code exception for storage file into local file system. */
  String ERR_EXCH_DISK_ERROR = "ERR_EXCH_DISK_ERROR";

  /** Error code exception for transfering file into SFTP server. */
  String ERR_EXCH_SFTP_ERROR = "ERR_EXCH_INVALID_PARAMETER";

  /** Error code exception for registering postmail into database. */
  String ERR_EXCH_DB_ERROR = "ERR_EXCH_INVALID_PARAMETER";
}
