/**
 * 
 */
package fr.ge.exchange.service;

import java.io.InputStream;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;

/**
 * Interface of services to send emails to competent authorities.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface ISendEmailService {

  /**
   * Send email with an attachment to competent authority.
   *
   * @param emailInfo
   *          email information
   * @param attachment
   *          attachment to send
   * @throws TechniqueException
   *           technique exception
   */
  void sendEmail(final EmailSendBean emailInfo, final InputStream attachment) throws TechniqueException;

  /**
   * Send email with an attachment to competent authority without any attachment.
   * 
   * @param emailInfo
   *          email information
   * @throws TechniqueException
   *           technique exception
   */
  void sendEmail(final EmailSendBean emailInfo) throws TechniqueException;
}
