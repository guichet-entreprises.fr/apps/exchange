/**
 * 
 */
package fr.ge.exchange.bean;

import java.util.Date;

import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;

/**
 * Class representing a mail message.
 */
public class MailMessage {

  /**
   * Mail message's unique ID (Tracker Id). <br/>
   * Ex: 2017-09-ZLN-TTD-33
   */
  @Size(max = 18)
  private String trackerId;

  /**
   * Delivery id. delivery company dependent. <br/>
   * Ex : 654_1581_768712stanbatch@honestica.com768712
   */
  @Size(max = 100)
  private String deliveryId;

  /**
   * Recipient name.<br/>
   * Exemple : <br/>
   * <p>
   * Ministère de l'économie et des finances<br/>
   * Direction de la délégation à la gestion des entreprises<br/>
   * Service de la création des entreprises<br/>
   * Bureau de la simplication administrative et du plan au numérique
   */
  @Size(max = 250)
  private String recipientName;

  /** Recipient address. */
  @Size(max = 150)
  private String recipientAddress;

  /** Recipient postal code. */
  @Size(max = 10)
  private String recipientPostalCode;

  /**
   * Recipient city.<br/>
   * Plus grand nom français 45 lettre + l'espace pour écrire les Cedex et autres BP. <br/>
   * source : https://fr.wikipedia.org/wiki/Liste_des_noms_de_lieux_les_plus_longs
   */
  @Size(max = 55)
  private String recipientCity;

  /** Name of the attachmentPath. */
  @Size(max = 255)
  private String attachmentPath;

  /** Date and time of postmail. */
  private Date datePostmail;

  /** Number of pages of the postmail. */
  private int numberPages;

  /**
   * Imprimerie Nationale Id postmail.<br/>
   * Delivery mode code based on trackerId plus delivery company specific code, ex :
   * 2017-09-ZLN-TTD-33_RCA
   */
  @Size(max = 25)
  private String deliveryModeId;

  /**
   * Imprimerie Nationale Id postmail.<br/>
   * Delivery mode code based on trackerId plus delivery company specific delivery code
   */
  @Size(max = 5)
  private String deliveryCode;

  /** The channel used. */
  @Size(max = 25)
  private String channel;

  /** The urgency of the postmail. */
  @Size(max = 25)
  private String urgency;

  /** Status postmail. */
  @Size(max = 25)
  private String status;

  /** recommandé number for the mail. */
  @Size(max = 50)
  private String numreco;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof MailMessage) {
      MailMessage that = (MailMessage) obj;
      return StringUtils.equals(this.trackerId, that.trackerId) && StringUtils.equals(this.deliveryId, that.deliveryId)
        && StringUtils.equals(this.recipientName, that.recipientName)
        && StringUtils.equals(this.recipientAddress, that.recipientAddress)
        && StringUtils.equals(this.recipientPostalCode, that.recipientPostalCode)
        && StringUtils.equals(this.recipientCity, that.recipientCity)
        && StringUtils.equals(this.attachmentPath, that.attachmentPath)
        && StringUtils.equals(this.deliveryModeId, that.deliveryModeId) && StringUtils.equals(this.channel, that.channel)
        && StringUtils.equals(this.urgency, that.urgency) && StringUtils.equals(this.numreco, that.numreco)
        && StringUtils.equals(this.status, that.status);
    }
    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return super.hashCode();
  }

  /**
   * Accesseur sur l'attribut {@link #trackerId}.
   *
   * @return String trackId
   */
  public String getTrackerId() {
    return trackerId;
  }

  /**
   * Mutateur sur l'attribut {@link #trackerId}.
   *
   * @param trackerId
   *          la nouvelle valeur de l'attribut trackerId
   */
  public void setTrackerId(String trackerId) {
    this.trackerId = trackerId;
  }

  /**
   * Accesseur sur l'attribut {@link #deliveryId}.
   *
   * @return String deliveryId
   */
  public String getDeliveryId() {
    return deliveryId;
  }

  /**
   * Mutateur sur l'attribut {@link #deliveryId}.
   *
   * @param deliveryId
   *          la nouvelle valeur de l'attribut deliveryId
   */
  public void setDeliveryId(final String deliveryId) {
    this.deliveryId = deliveryId;
  }

  /**
   * Accesseur sur l'attribut {@link #recipientName}.
   *
   * @return String recipientName
   */
  public String getRecipientName() {
    return recipientName;
  }

  /**
   * Mutateur sur l'attribut {@link #recipientName}.
   *
   * @param recipientName
   *          la nouvelle valeur de l'attribut recipientName
   */
  public void setRecipientName(final String recipientName) {
    this.recipientName = recipientName;
  }

  /**
   * Accesseur sur l'attribut {@link #recipientAddress}.
   *
   * @return String recipientAddress
   */
  public String getRecipientAddress() {
    return recipientAddress;
  }

  /**
   * Mutateur sur l'attribut {@link #recipientAddress}.
   *
   * @param recipientAddress
   *          la nouvelle valeur de l'attribut recipientAddress
   */
  public void setRecipientAddress(final String recipientAddress) {
    this.recipientAddress = recipientAddress;
  }

  /**
   * Accesseur sur l'attribut {@link #recipientPostalCode}.
   *
   * @return String recipientPostalCode
   */
  public String getRecipientPostalCode() {
    return recipientPostalCode;
  }

  /**
   * Mutateur sur l'attribut {@link #recipientPostalCode}.
   *
   * @param recipientPostalCode
   *          la nouvelle valeur de l'attribut recipientPostalCode
   */
  public void setRecipientPostalCode(final String recipientPostalCode) {
    this.recipientPostalCode = recipientPostalCode;
  }

  /**
   * Accesseur sur l'attribut {@link #recipientCity}.
   *
   * @return String recipientCity
   */
  public String getRecipientCity() {
    return recipientCity;
  }

  /**
   * Mutateur sur l'attribut {@link #recipientCity}.
   *
   * @param recipientCity
   *          la nouvelle valeur de l'attribut recipientCity
   */
  public void setRecipientCity(final String recipientCity) {
    this.recipientCity = recipientCity;
  }

  /**
   * Accesseur sur l'attribut {@link #attachmentPath}.
   *
   * @return String attachmentPath
   */
  public String getAttachmentPath() {
    return attachmentPath;
  }

  /**
   * Mutateur sur l'attribut {@link #attachmentPath}.
   *
   * @param attachmentPath
   *          la nouvelle valeur de l'attribut attachmentPath
   */
  public void setAttachmentPath(final String attachmentPath) {
    this.attachmentPath = attachmentPath;
  }

  /**
   * Accesseur sur l'attribut {@link #datePostmail}.
   *
   * @return Date datePostmail
   */
  public Date getDatePostmail() {
    return datePostmail;
  }

  /**
   * Mutateur sur l'attribut {@link #datePostmail}.
   *
   * @param datePostmail
   *          la nouvelle valeur de l'attribut datePostmail
   */
  public void setDatePostmail(final Date datePostmail) {
    this.datePostmail = datePostmail;
  }

  /**
   * Accesseur sur l'attribut {@link #numberPages}.
   *
   * @return int numberPages
   */
  public int getNumberPages() {
    return numberPages;
  }

  /**
   * Mutateur sur l'attribut {@link #numberPages}.
   *
   * @param numberPages
   *          la nouvelle valeur de l'attribut numberPages
   */
  public void setNumberPages(final int numberPages) {
    this.numberPages = numberPages;
  }

  /**
   * Accesseur sur l'attribut {@link #deliveryModeId}.
   *
   * @return the delivery mode id
   */
  public String getDeliveryModeId() {
    return deliveryModeId;
  }

  /**
   * Mutateur sur l'attribut {@link #deliveryModeId}.
   *
   * @param deliveryModeId
   *          la nouvelle valeur de l'attribut deliveryModeId
   */
  public void setDeliveryModeId(final String deliveryModeId) {
    this.deliveryModeId = deliveryModeId;
  }

  /**
   * Accesseur sur l'attribut {@link #deliveryCode}.
   *
   * @return String deliveryCode
   */
  public String getDeliveryCode() {
    return deliveryCode;
  }

  /**
   * Mutateur sur l'attribut {@link #deliveryCode}.
   *
   * @param deliveryCode
   *          la nouvelle valeur de l'attribut deliveryCode
   */
  public void setDeliveryCode(String deliveryCode) {
    this.deliveryCode = deliveryCode;
  }

  /**
   * Accesseur sur l'attribut {@link #channel}.
   *
   * @return String channel
   */
  public String getChannel() {
    return channel;
  }

  /**
   * Mutateur sur l'attribut {@link #channel}.
   *
   * @param channel
   *          la nouvelle valeur de l'attribut channel
   */
  public void setChannel(final String channel) {
    this.channel = channel;
  }

  /**
   * Accesseur sur l'attribut {@link #urgency}.
   *
   * @return String urgency
   */
  public String getUrgency() {
    return urgency;
  }

  /**
   * Mutateur sur l'attribut {@link #urgency}.
   *
   * @param urgency
   *          la nouvelle valeur de l'attribut urgency
   */
  public void setUrgency(final String urgency) {
    this.urgency = urgency;
  }

  /**
   * Accesseur sur l'attribut {@link #status}.
   *
   * @return String status
   */
  public String getStatus() {
    return status;
  }

  /**
   * Mutateur sur l'attribut {@link #status}.
   *
   * @param status
   *          la nouvelle valeur de l'attribut status
   */
  public void setStatus(final String status) {
    this.status = status;
  }

  /**
   * Accesseur sur l'attribut {@link #numreco}.
   *
   * @return String numreco
   */
  public String getNumreco() {
    return numreco;
  }

  /**
   * Mutateur sur l'attribut {@link #numreco}.
   *
   * @param numreco
   *          la nouvelle valeur de l'attribut numreco
   */
  public void setNumreco(String numreco) {
    this.numreco = numreco;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return String.format(
      "MailMessage [trackerId=%s, deliveryId=%s, recipientName=%s, recipientAddress=%s, recipientPostalCode=%s, recipientCity=%s, attachmentPath=%s, datePostmail=%s, numberPages=%s, deliveryModeId=%s, deliveryCode=%s, channel=%s, urgency=%s, status=%s, numreco=%s]",
      trackerId, deliveryId, recipientName, recipientAddress, recipientPostalCode, recipientCity, attachmentPath, datePostmail,
      numberPages, deliveryModeId, deliveryCode, channel, urgency, status, numreco);
  }

}
