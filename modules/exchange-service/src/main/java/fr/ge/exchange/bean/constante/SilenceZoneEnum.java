/**
 * 
 */
package fr.ge.exchange.bean.constante;

/**
 * Enumeration with coordinates of the PDF silence zone reserved.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public enum SilenceZoneEnum {

  /** x = 195,5 mm. */
  X(552.756),

  /** y = 14,5 mm. */
  Y(41.102),

  /** w = 14,5 mm. */
  W(341.102),

  /** h = 14,5 mm. */
  H(41.102);

  /** point (1 point = 0,352778 mm). */
  private double point;

  /**
   * Instantie un nouveau address zone enum.
   *
   * @param point
   *          point
   */
  SilenceZoneEnum(final double point) {
    this.point = point;
  }

  /**
   * Accesseur sur l'attribut {@link #point}.
   *
   * @return double point
   */
  public double getPoint() {
    return this.point;
  }

}
