/**
 * 
 */
package fr.ge.exchange.utils;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.multipdf.Overlay;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.slf4j.Logger;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.constante.AddressZoneEnum;
import fr.ge.exchange.bean.constante.IAdressFormConstant;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;
import fr.ge.exchange.bean.constante.SilenceZoneEnum;
import fr.ge.exchange.postmail.PostmailGenerator;

/**
 * Utility class : PDFs generations and manipulations for Exchange.
 * 
 * @deprecated
 * @see {@link PostmailGenerator}
 */
public final class GenerationPdfUtils {

  /** La constante EXTENSION_PDF. */
  private static final String EXTENSION_PDF = ".pdf";

  /** La constante EXTENSION_ZIP. */
  private static final String EXTENSION_ZIP = ".zip";

  /** The constant EMPTY_ZONE_PDF which indicates that text zone is empty. */
  private static final String EMPTY_ZONE_PDF = "\r\n";

  /** The constante REGION_ADDRESS. */
  private static final String REGION_ADDRESS = "regionAddress";

  /** The constante REGION_SILENCE. */
  private static final String REGION_SILENCE = "regionSilence";

  /** Le logger fonctionnel. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /**
   * Constructor.
   */
  private GenerationPdfUtils() {
    // No constructor
  }

  /**
   * Generates a PDF from a fusion of a template and an address filter filled with parameters and
   * save it as a zip file.
   *
   * @param templateStream
   *          input stream of the template document
   * @param postMailInfo
   *          information to filling the filter
   * @param localPdfDirectoryStorage
   *          local directory to store the zip generated
   * @return the path of the ZIP file generated
   * @throws TechniqueException
   *           TechniqueException if : ERR_EXCH_PDF_ERROR, ERR_EXCH_DISK_ERROR or
   *           ERR_EXCH_INVALID_PDF
   */
  public static String overlapAndStorePdfWithAddressFilter(final InputStream templateStream,
    final String localPdfDirectoryStorage, String trackerId, String recipientName, String recipientAddress,
    String recipientPostalCode, String recipientCity) throws TechniqueException {
    PDDocument finalPdf = null;
    Overlay overlay = new Overlay();
    PDDocument template = null;
    File filterFilledFile = null;
    String zipFilePath = null;
    try {
      filterFilledFile = fillAndSaveAddressFilter(trackerId, recipientName, recipientAddress, recipientPostalCode, recipientCity);

      // Get the template document and check if the PDF is valid
      try {
        template = PDDocument.load(templateStream);
      } catch (IOException e) {
        String messageInvalidPdf = "Envoi de courrier + " + trackerId + ": fichier PDF invalide";
        LOGGER_FONC.error(messageInvalidPdf);
        throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_INVALID_PDF, messageInvalidPdf);
      }

      // Check if the content of the PDF is valid (no text in reserved zones)
      GenerationPdfUtils.checkReservedZoneEmpty(template.getPage(0), trackerId);

      // Fusion of the template with the address filter
      overlay.setInputPDF(template);
      overlay.setFirstPageOverlayFile(filterFilledFile.getAbsolutePath());
      overlay.setOverlayPosition(Overlay.Position.FOREGROUND);
      HashMap < Integer, String > overlayGuide = new HashMap < Integer, String >();
      overlayGuide.put(0, filterFilledFile.getAbsolutePath());
      finalPdf = overlay.overlay(overlayGuide);

      // we dont need anymore the filter filled => we delete it
      filterFilledFile.delete();
    } catch (IOException e) {
      if (filterFilledFile != null) {
        filterFilledFile.delete();
      }
      String messageErrorGenerationPdf = String
        .format("Erreur lors de la génération du PDF d'envoi de courrier ayant pour ID de suivi : %s.", trackerId);
      LOGGER_FONC.error(messageErrorGenerationPdf, e);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_PDF_ERROR, messageErrorGenerationPdf, e);
    }

    // -->Store zip file in local disk
    try {
      zipFilePath = generateAndSaveZipPostmailFile(trackerId, localPdfDirectoryStorage, finalPdf);
      // Close inputs
      template.close();
      overlay.close();
      finalPdf.close();
    } catch (IOException e) {
      String messageStoragePdf = String
        .format("Erreur lors de la sauvegarde du ZIP d'envoi de courrier ayant pour ID de suivi : %s.", trackerId);
      LOGGER_FONC.error(messageStoragePdf, e);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_DISK_ERROR, messageStoragePdf, e);
    }
    return zipFilePath;
  }

  /**
   * Check reserved zone empty.
   *
   * @param page
   *          page
   * @param trackId
   *          track id
   * @throws IOException
   *           if an i/o exception occurs
   * @throws TechniqueException
   *           technique exception reserved zone is not empty
   */
  private static void checkReservedZoneEmpty(final PDPage page, final String trackId) throws IOException, TechniqueException {
    PDFTextStripperByArea stripper = new PDFTextStripperByArea();
    Rectangle2D.Double regionAddress = new Rectangle2D.Double(AddressZoneEnum.X.getPoint(), AddressZoneEnum.Y.getPoint(),
      AddressZoneEnum.W.getPoint(), AddressZoneEnum.H.getPoint());
    Rectangle2D.Double regionSilence = new Rectangle2D.Double(SilenceZoneEnum.X.getPoint(), SilenceZoneEnum.Y.getPoint(),
      SilenceZoneEnum.W.getPoint(), SilenceZoneEnum.H.getPoint());
    stripper.addRegion(GenerationPdfUtils.REGION_ADDRESS, regionAddress);
    stripper.addRegion(GenerationPdfUtils.REGION_SILENCE, regionSilence);
    stripper.extractRegions(page);
    String textAddress = stripper.getTextForRegion(GenerationPdfUtils.REGION_ADDRESS);
    String textSilence = stripper.getTextForRegion("regionSilence");
    if (!GenerationPdfUtils.isTextEmptyZonePdf(textAddress) || !GenerationPdfUtils.isTextEmptyZonePdf(textSilence)) {
      String messageInvalidContentPdf = String
        .format("Envoi de courrier %s : des zones réservées à l'Imprimerie Nationale sont remplies sur le template", trackId);
      LOGGER_FONC.error(messageInvalidContentPdf);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_INVALID_PDF, messageInvalidContentPdf);
    }
  }

  /**
   * Check if a text corresponds to an empty PDF zone.
   *
   * @param text
   *          text
   * @return true, if empty
   */
  private static boolean isTextEmptyZonePdf(final String text) {
    return org.apache.commons.lang3.StringUtils.isBlank(text) || text.equals(EMPTY_ZONE_PDF);
  }

  /**
   * Fill the pdf filter with address parameters.
   *
   * @param postMailInfo
   *          post mail info to fill
   * @return the pdf filter filled file
   * @throws IOException
   *           Signale qu'une exception I/O est apparue
   */
  private static File fillAndSaveAddressFilter(String trackerId, String recipientName, String recipientAddress,
    String recipientPostalCode, String recipientCity) throws IOException {

    // XXX LAB : externaliser ce chargement de pdf
    PDDocument filter;
    filter = PDDocument.load(GenerationPdfUtils.class.getResourceAsStream("/pdf/addressFilter_1field.pdf"));
    // adding fonts to the document
    prepareFont(filter, PDType1Font.HELVETICA);
    PDDocumentCatalog docCatalog = filter.getDocumentCatalog();
    PDAcroForm acroForm = docCatalog.getAcroForm();

    PDField fieldAddress = acroForm.getField(IAdressFormConstant.ADRESS);
    fieldAddress.setValue(String.format("%s\n%s\n%s %s", recipientName, recipientAddress, recipientPostalCode, recipientCity));

    // Filling the trackId field
    PDField fieldTrackId = acroForm.getField(IAdressFormConstant.TRACK_ID);
    // changing the color of the track id, set it to white (1 1 1), for this field only
    COSDictionary cosDictionary = fieldTrackId.getCOSObject();
    COSBase defaultAppearance = cosDictionary.getDictionaryObject(COSName.DA);
    cosDictionary.setString(COSName.DA, (defaultAppearance != null ? defaultAppearance.toString() : "") + " 1 1 1 rg ");
    fieldTrackId.setValue(trackerId);

    // We must "flatten" (aplatir) the form, so it can be visible on the final pdf
    acroForm.flatten();
    // Temporary file containing the filter filled with the address
    String tempFile = "tmp_" + UUID.randomUUID().toString();
    File filterFilledFile = File.createTempFile(tempFile, EXTENSION_PDF);
    filter.save(filterFilledFile);
    LOGGER_FONC.debug("PostMail : sauvegarde temporaire du filtre d'adresse valorisée vers [{}]", tempFile);
    filter.close();
    return filterFilledFile;
  }

  /**
   * Add Fonts to the document
   * 
   * @param pdfDocument
   *          the pdf document to add the font to
   * @param fontsthe
   *          font to add
   */
  private static void prepareFont(PDDocument pdfDocument, PDFont... fonts) {
    PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
    PDAcroForm acroForm = docCatalog.getAcroForm();

    PDResources res = acroForm.getDefaultResources();
    if (res == null)
      res = new PDResources();

    for (PDFont font : fonts) {
      res.add(font);
    }

    acroForm.setDefaultResources(res);

  }

  /**
   * Generate a ZIP file from the PDF attachment to send to the Imprimerie Nationale.
   *
   * @param trackId
   *          track id
   * @param localPdfDirectoryStorage
   *          local pdf directory storage
   * @param finalPdf
   *          final pdf generated to put in ZIP file
   * @return string
   * @throws IOException
   *           Signale qu'une exception I/O est apparue
   */
  private static String generateAndSaveZipPostmailFile(final String trackId, final String localPdfDirectoryStorage,
    final PDDocument finalPdf) throws IOException {
    // Rule for name of PDF and ZIP : 'dateYYYY-MM-DD'_'idpostmail'.pdf
    String currentDay = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    String pdfName = currentDay + "_" + trackId + EXTENSION_PDF;
    String pdfFilePath = localPdfDirectoryStorage + File.separator + pdfName;
    File pdfFile = new File(pdfFilePath);
    finalPdf.save(pdfFile);
    LOGGER_FONC.debug("PostMail : sauvegarde temporaire du courrier PDF rempli vers [{}]", pdfFilePath);

    // generation of the zip file where you put the pdf
    String zipName = currentDay + "_" + trackId + EXTENSION_ZIP;
    String zipFilePath = localPdfDirectoryStorage + File.separator + zipName;
    File zipFile = new File(zipFilePath);
    ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFile));
    ZipEntry pdfEntry = new ZipEntry(pdfName);
    zipOut.putNextEntry(pdfEntry);
    FileInputStream fis = new FileInputStream(pdfFile);
    byte[] bytes = new byte[fis.available()];
    fis.read(bytes);
    zipOut.write(bytes);
    zipOut.closeEntry();
    zipOut.close();
    fis.close();
    LOGGER_FONC.debug("PostMail : sauvegarde du courrier ZIP sur [{}]", zipFilePath);

    // We don't need the pdf anymore => we delete it
    pdfFile.delete();

    return zipFilePath;
  }
}
