/**
 * 
 */
package fr.ge.exchange.domaine;

/**
 * Generator that create an id including the delivery mode, specifically for the Imprimerie
 * Nationale.<br/>
 * Imprimerie Nationale format: internalId_[R/V][N/C][R/A/L/E] :
 * <ul>
 * <li>internalId = GE file's id</li>
 * <li>[R/V] : Printing</li>
 * <ul>
 * <li>R pour Recto</li>
 * <li>V pour recto-Verso</li>
 * </ul>
 * <li>[N/C] : color
 * <ul>
 * <li>C pour Couleur</li>
 * <li>N pour Noir</li>
 * </ul>
 * <li>[R/A/L/E] = Delivery mode</li>
 * <ul>
 * <li>R pour LR (sans AR)</li>
 * <li>A pour LRAR</li>
 * <li>L pour Lettre</li>
 * <li>E pour Ecopli</li>
 * </ul>
 * </li>
 * </ul>
 */
public class ImprimerieNationaleDeliveryIdGenerator {

  /** Id used as base by the generator */
  private String trackerId;
  private char rectoverso = 'V';
  private char color = 'C';
  private char delivery = 'A';

  /** private constructor */
  private ImprimerieNationaleDeliveryIdGenerator(String trackerId) {
    this.trackerId = trackerId;
  }

  /** Use the recto */
  public ImprimerieNationaleDeliveryIdGenerator recto() {
    this.rectoverso = 'R';
    return this;
  }

  /** Use the recto/verso */
  public ImprimerieNationaleDeliveryIdGenerator rectoVerso() {
    this.rectoverso = 'V';
    return this;
  }

  /** Use the color print */
  public ImprimerieNationaleDeliveryIdGenerator color() {
    this.color = 'C';
    return this;
  }

  /** print in black and white */
  public ImprimerieNationaleDeliveryIdGenerator blackAndWhite() {
    this.color = 'N';
    return this;
  }

  /** Send as a simple letter */
  public ImprimerieNationaleDeliveryIdGenerator lettre() {
    this.delivery = 'L';
    return this;
  }

  /** Send as an economic letter */
  public ImprimerieNationaleDeliveryIdGenerator economie() {
    this.delivery = 'E';
    return this;
  }

  /** Send as a simple recommandé */
  public ImprimerieNationaleDeliveryIdGenerator recommande() {
    this.delivery = 'R';
    return this;
  }

  /** Send as a recommandé avec accusé de réception */
  public ImprimerieNationaleDeliveryIdGenerator recommandeAccuseReception() {
    this.delivery = 'A';
    return this;
  }

  public static ImprimerieNationaleDeliveryIdGenerator create(String trackerId) {
    return new ImprimerieNationaleDeliveryIdGenerator(trackerId);
  }

  /** Generate the id ; to be used after that the delivery option have been set. */
  public String buildId() {
    return String.format("%s_%c%c%c", trackerId, rectoverso, color, delivery);
  }

  /** Retrieve the code part of the generated Id */
  public String getDeliveryCode() {
    return String.format("%c%c%c", rectoverso, color, delivery);
  }

}
