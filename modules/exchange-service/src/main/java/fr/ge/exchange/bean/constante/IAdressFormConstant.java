/**
 * 
 */
package fr.ge.exchange.bean.constante;

/**
 * Constant of the form to fill the adress of a filter.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface IAdressFormConstant {

  /** recipient. */
  String RECIPIENT = "recipient";

  /** adress. */
  String ADRESS = "address";

  /** postal code. */
  String POSTAL_CODE = "postalCode";

  /** city. */
  String CITY = "city";

  /** track id. */
  String TRACK_ID = "trackId";

}
