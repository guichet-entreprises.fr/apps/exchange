/**
 * 
 */
package fr.ge.exchange.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.exchange.dao.SchemaVersionDAO;
import fr.ge.exchange.service.ISchemaVersionService;

/**
 * Implementation for ISchemaVersionService.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class SchemaVersionServiceImpl implements ISchemaVersionService {

  /** The schema version mapper. */
  @Autowired
  private SchemaVersionDAO schemaVersionDAO;

  @Override
  public String getVersion() {
    return schemaVersionDAO.getVersion();
  }
}
