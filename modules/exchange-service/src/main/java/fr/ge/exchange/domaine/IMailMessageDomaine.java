/**
 * 
 */
package fr.ge.exchange.domaine;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.MailMessage;

/**
 * Access point to the mail message domaine.
 */
public interface IMailMessageDomaine {

  /**
   * Update a mail message into the database.<br/>
   * Update the following fields :
   * <ul>
   * <li>numberPages</li>
   * <li>channel</li>
   * <li>urgency</li>
   * <li>status</li>
   * <li>numreco</li>
   * <li>deliveryCode</li>
   * <li>deliveryId</li>
   * </ul>
   * *
   * 
   * @throws TechniqueException
   *           if a technical problem occurs
   */
  void update(MailMessage postmailBean) throws TechniqueException;

  /**
   * Access to a mail message by its id
   * 
   * @param trackId
   *          the id
   * @return the object found, null otherwise
   * @throws TechniqueException
   *           if a technical problem occurs
   */
  MailMessage read(final String trackId) throws TechniqueException;

  /**
   * Inserts a new postmail into DB.
   * 
   * @param postMailInfo
   *          the postmail information
   * @throws TechniqueException
   *           if a technical problem occurs
   */
  void create(final MailMessage postMailInfo) throws TechniqueException;

}
