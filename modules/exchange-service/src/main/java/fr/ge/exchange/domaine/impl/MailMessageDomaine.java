/**
 * 
 */
package fr.ge.exchange.domaine.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.MailMessage;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;
import fr.ge.exchange.dao.MailMessageDAO;
import fr.ge.exchange.domaine.IMailMessageDomaine;

/**
 * Implementation of functionality for the Mail Message Object.
 */
public class MailMessageDomaine implements IMailMessageDomaine {

  /** Le logger fonctionnel. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /** The postmail DAO mapper. */
  @Autowired
  private MailMessageDAO mailMessageDAO;

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional(readOnly = false, value = "ge_exchange")
  public void create(final MailMessage mailMessage) throws TechniqueException {
    if (null == mailMessage || StringUtils.isBlank(mailMessage.getTrackerId())
      || StringUtils.isBlank(mailMessage.getRecipientName()) || StringUtils.isBlank(mailMessage.getRecipientAddress())
      || StringUtils.isBlank(mailMessage.getDeliveryModeId())) {
      LOGGER_FONC.error("The mail message is not consistant : {}", mailMessage);
      throw new TechniqueException(String.format("The mail message is not consistant : %s", mailMessage));
    }
    try {
      LOGGER_FONC.info("Persistence de postmail avec trackId {}.", mailMessage.getTrackerId());
      mailMessageDAO.create(mailMessage);
    } catch (RuntimeException e) {
      String trackId = mailMessage == null ? StringUtils.EMPTY : mailMessage.getTrackerId();
      String messageBdd = String.format("Insertion BDD en échec : Envoi de courrier %s.", trackId);
      LOGGER_FONC.error(messageBdd, e);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_DB_ERROR, messageBdd, e);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional(readOnly = true, value = "ge_exchange")
  public MailMessage read(final String trackId) throws TechniqueException {
    MailMessage mailMessage = null;
    try {
      mailMessage = mailMessageDAO.read(trackId);
    } catch (RuntimeException e) {
      LOGGER_FONC.error("Erreur lors la lecture dans la table postmail", e);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_DB_ERROR, "Recherche BDD en échec");
    }

    if (null == mailMessage || null == mailMessage.getTrackerId()) {
      LOGGER_FONC.error("Aucune ligne en BDD dans la table postmail pour l'ID suivi " + trackId);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_DB_ERROR, "Recherche BDD en échec");
    }

    return mailMessage;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional(readOnly = false, value = "ge_exchange")
  public void update(MailMessage mailMessage) throws TechniqueException {
    MailMessage mailMessageBDD = this.read(mailMessage.getTrackerId());
    if (null == mailMessageBDD) {
      LOGGER_FONC.warn("L'enregistrement {} est présent dans le fichier CSV mais non présent en base de données",
        mailMessage.getTrackerId());
    } else {

      if (null == mailMessageBDD.getStatus() || (!mailMessageBDD.getStatus().equals(mailMessage.getStatus()))) {
        try {
          mailMessageBDD.setStatus(mailMessage.getStatus());
          mailMessageBDD.setDeliveryId(mailMessage.getDeliveryId());
          mailMessageBDD.setNumberPages(mailMessage.getNumberPages());
          mailMessageBDD.setChannel(mailMessage.getChannel());
          mailMessageBDD.setUrgency(mailMessage.getUrgency());

          mailMessageDAO.update(mailMessageBDD);
          LOGGER_FONC.info("Mise à jour dans la table postmail pour le trackID [{}]", mailMessage.getTrackerId());

        } catch (RuntimeException e) {
          LOGGER_FONC.error("Erreur lors la mise à jour dans la table postmail pour le trackID [{}]", mailMessage.getTrackerId());
          throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_DB_ERROR, "Update BDD en échec");
        }
      } else {
        LOGGER_FONC.warn("Pas de mise à jour du statut du courrier [{}]", mailMessage.getTrackerId());
      }
    }

  }

}
