/**
 * 
 */
package fr.ge.exchange.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.graphics.color.PDOutputIntent;
import org.apache.xmpbox.XMPMetadata;
import org.apache.xmpbox.schema.AdobePDFSchema;
import org.apache.xmpbox.schema.DublinCoreSchema;
import org.apache.xmpbox.schema.PDFAIdentificationSchema;
import org.apache.xmpbox.schema.XMPBasicSchema;
import org.apache.xmpbox.type.BadFieldValueException;
import org.apache.xmpbox.xml.XmpSerializer;
import org.slf4j.Logger;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;

/**
 * Generate a PDF/A (https://en.wikipedia.org/wiki/PDF/A)
 */
public class PdfArchiveGenerator {

  /** Le logger fonctionnel. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /** Lists of the fonts to include in the archive PDF. */
  private static final URL[] knownFonts = getFonts();

  /**
   * Generate an Archive PDF from an existing PDF
   * 
   * @param pdfInputFile
   *          not null, from a real pdf file
   * @return the output stream for the generated PDF/A
   * @throws TechniqueException
   */
  public static PDDocument generateArchive(PDDocument pdfInputFile) throws TechniqueException {

    PDDocument generatePDFArchive = null;

    // Get the template document and check if the PDF is valid
    try {
      generatePDFArchive = generatePDFArchive(pdfInputFile);
    } catch (IOException | TransformerException e) {
      String messageInvalidPdf = "Fichier PDF invalide";
      LOGGER_FONC.error(messageInvalidPdf);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_INVALID_PDF, messageInvalidPdf, e);
    }

    return generatePDFArchive;
  }

  /**
   * Method that does the actual transformation to Archive PDF.<br/>
   * the conversion is quite limited as of now and is not complete.
   * 
   * @param document
   *          the document to convert, not null
   * @return the converted document, not null
   * @throws IOException
   *           if an IO error occurs
   * @throws TransformerException
   *           if created the archive data is impossible (xmp related)
   */
  private static PDDocument generatePDFArchive(PDDocument document) throws IOException, TransformerException {

    PDDocument documentA = new PDDocument(document.getDocument());

    String SCNGE = "guichet-entreprises.fr";
    Calendar now = Calendar.getInstance();
    String creatorTool = "PDFBox 2";
    String filename = String.format("PDF Archive %s",
      document.getDocumentInformation().getTitle() != null ? document.getDocumentInformation().getTitle() : "");

    PDDocumentInformation documentInformation = documentA.getDocumentInformation();
    documentInformation.setAuthor(SCNGE);
    documentInformation.setCreationDate(now);
    documentInformation.setCreator(creatorTool);
    documentInformation.setModificationDate(now);
    documentInformation.setTitle(filename);
    documentInformation.setProducer(SCNGE);

    loadFonts(documentA, knownFonts);

    /*
     * Include XMP Metadata Block It is required to have XMP metadata defined in the PDF. At least,
     * the PDFA Schema (giving details on the version of PDF/A specification reached by the
     * document) must be present. These lines create the XMP metadata for a PDF/A-1b document
     */
    // add XMP metadata
    XMPMetadata xmp = XMPMetadata.createXMPMetadata();

    try {
      DublinCoreSchema dc = xmp.createAndAddDublinCoreSchema();
      dc.setTitle(filename);
      dc.setTextPropertyValue("title", filename);
      dc.setTextPropertyValue("creator", SCNGE);

      AdobePDFSchema adobePDFSchema = xmp.createAndAddAdobePDFSchema();
      adobePDFSchema.setProducer(SCNGE);

      XMPBasicSchema xmpBasicSchema = xmp.createAndAddXMPBasicSchema();
      xmpBasicSchema.setCreatorTool(creatorTool);
      xmpBasicSchema.setCreateDate(now);
      xmpBasicSchema.setModifyDate(now);

      PDFAIdentificationSchema id = xmp.createAndAddPFAIdentificationSchema();
      id.setPart(1);
      id.setConformance("B");

      XmpSerializer serializer = new XmpSerializer();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      serializer.serialize(xmp, baos, true);

      PDMetadata metadata = new PDMetadata(documentA);
      metadata.importXMPMetadata(baos.toByteArray());
      documentA.getDocumentCatalog().setMetadata(metadata);
    } catch (BadFieldValueException e) {
      // won't happen here, as the provided value is valid
      throw new IllegalArgumentException(e);
    }

    /*
     * Include Color Profile It is mandatory to include the color profile used by the document.
     * Different profiles can be used. This example takes one present in pdfbox
     */
    // sRGB output intent
    InputStream colorProfile = PdfArchiveGenerator.class.getResourceAsStream("/org/apache/pdfbox/resources/pdfa/sRGB.icc");
    PDOutputIntent intent = new PDOutputIntent(documentA, colorProfile);
    intent.setInfo("sRGB IEC61966-2.1");
    intent.setOutputCondition("sRGB IEC61966-2.1");
    intent.setOutputConditionIdentifier("sRGB IEC61966-2.1");
    intent.setRegistryName("http://www.color.org");
    documentA.getDocumentCatalog().addOutputIntent(intent);

    return documentA;
  }

  /**
   * Build the list of the fonts.
   * 
   * @return the not null list of fonts
   */
  private static URL[] getFonts() {
    String baseDir = "fonts";
    String[] fontFilenames = new String[] {"times.ttf", "timesbd.ttf", "timesbi.ttf", "timesi.ttf", "arial.ttf", "arialbd.ttf",
      "ariali.ttf" };
    List < URL > fontFiles = new ArrayList <>();

    for (String fontFilename : fontFilenames) {
      URL fontUrl = PdfArchiveGenerator.class.getClassLoader().getResource(String.format("%s/%s", baseDir, fontFilename));
      if (fontUrl != null && fontUrl.getFile() != null) {
        fontFiles.add(fontUrl);
        LOGGER_FONC.info("Added {} to the fonts list.", fontUrl);
      } else {
        LOGGER_FONC.warn("Unable to access {} to add it to the fonts list.", fontUrl);
      }
    }

    return fontFiles.toArray(new URL[fontFiles.size()]);
  }

  /**
   * Include the necessary fonts into the document.
   * 
   * @param document
   *          not null
   * @param fontFiles
   *          not null, may be empty
   * @throws IOException
   *           if any file access problem occur
   */
  private static void loadFonts(PDDocument document, URL... fontFiles) throws IOException {

    for (URL fontfile : fontFiles) {
      if (fontfile != null) {
        // load the font as this needs to be embedded
        PDFont font = PDType0Font.load(document, fontfile.openStream());

        // A PDF/A file needs to have the font embedded if the font is used for text rendering
        // in rendering modes other than text rendering mode 3.
        //
        // This requirement includes the PDF standard fonts, so don't use their static PDFType1Font
        // classes such as
        // PDFType1Font.HELVETICA.
        //
        // As there are many different font licenses it is up to the developer to check if the
        // license
        // terms for the
        // font loaded allows embedding in the PDF.
        //
        if (!font.isEmbedded()) {
          throw new IllegalStateException(
            "PDF/A compliance requires that all fonts used for text rendering in rendering modes other than rendering mode 3 are embedded.");
        }

        LOGGER_FONC.warn("Attention, font {} not added to the page, so not really embedded.", font.getName());
      }
    }
  }

}
