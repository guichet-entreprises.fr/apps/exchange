/**
 * 
 */
package fr.ge.exchange.utils;

import java.io.IOException;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class : PDFs generations and manipulations for Exchange.
 */
public class PDFBoxUtils {
    /** Utility class logs. */
    public static final Logger LOGGER = LoggerFactory.getLogger(PDFBoxUtils.class);

    /**
     * Check the document for null pages and correct them by adding empty
     * content to the page.<br/>
     * A null page has no content, whereas an empty page has a content that is
     * empty.
     * 
     * @param document
     *            the document to check and correct, not null
     * @return the number of corrected pages, -1 if a problem occurs
     */
    public static int correctNullPages(PDDocument document) {

        int correctedPages = 0;
        for (PDPage page : document.getPages()) {
            COSDictionary pageDictionary = page.getCOSObject();
            COSBase contents = pageDictionary.getDictionaryObject(COSName.CONTENTS);
            if (contents == null) {
                try {
                    PDFBoxUtils.doEmptyPage(document, page);
                } catch (IOException e) {
                    LOGGER.warn("Unable to correct null pages : {}.", e.getMessage());
                    return -1;
                }
                correctedPages++;
            }
        }
        return correctedPages;
    }

    /**
     * Add a page at the index position.
     * 
     * @param doc
     *            not null, the document to insert the page to
     * @param index
     *            the position, 0-based
     * @param page
     *            the page to insert, not null
     * @throws IOException
     *             if any problem occurs
     */
    public static void addPage(PDDocument doc, final int index) throws IOException {
        PDPage page = new PDPage(PDRectangle.A4);

        // Use the normal method if the page comes after all other pages
        if (index >= doc.getNumberOfPages())
            doc.addPage(page);
        else {
            doc.getPages().insertBefore(page, doc.getPage(index));
        }
        doEmptyPage(doc, page);
    }

    /**
     * Empty this page. Empty is not null
     * 
     * @param doc
     *            the document containing the page, not null
     * @param page
     *            the page to empty, not null
     * @throws IOException
     *             if any problem occurs
     */
    public static void doEmptyPage(PDDocument doc, PDPage page) throws IOException {
        PDPageContentStream contentStream = new PDPageContentStream(doc, page);

        // Define a text content stream using the selected font, moving the
        // cursor ...
        contentStream.beginText();
        contentStream.endText();

        // Make sure that the content stream is closed:
        contentStream.close();
    }

}
