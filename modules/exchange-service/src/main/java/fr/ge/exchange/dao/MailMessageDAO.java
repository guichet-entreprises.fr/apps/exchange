package fr.ge.exchange.dao;

import org.apache.ibatis.annotations.Param;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.MailMessage;

/**
 * {@link MailMessage} DAO.
 */
public interface MailMessageDAO {

  /**
   * Get a mailMessage.
   * 
   * @param trackerId
   *          the trackerId
   * @return the mailMessage
   * @throws TechniqueException
   *           if a technical problem occurs
   */
  MailMessage read(@Param("trackerId") final String trackerId) throws TechniqueException;

  /**
   * Inserts a mailMessage.
   * 
   * @param mailMessage
   *          the mail information
   * @throws TechniqueException
   *           if a technical problem occurs
   */
  void create(@Param("mailMessage") final MailMessage mailMessage) throws TechniqueException;

  /**
   * Update the following fields from a mailMessage :
   * <ul>
   * <li>numberPages</li>
   * <li>channel</li>
   * <li>urgency</li>
   * <li>status</li>
   * <li>numreco</li>
   * <li>deliveryCode</li>
   * <li>deliveryId</li>
   * </ul>
   * 
   * @param mailMessage
   *          the mail information
   * @throws TechniqueException
   *           if a technical problem occurs
   */
  void update(@Param("mailMessage") final MailMessage mailMessage) throws TechniqueException;

}
