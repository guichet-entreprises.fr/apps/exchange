package fr.ge.exchange.dao;

import org.apache.ibatis.annotations.Param;

import fr.ge.exchange.bean.PostmailBean;

/**
 * {@link PostmailBean} mapper. XXX LAB : vérifier si c'est pas à virer ça aussi
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface PostmailDAO {

  /**
   * Gets an postmail.
   * 
   * @param trackerId
   *          the trackerId
   * @return the postmail
   */
  PostmailBean getPostmail(@Param("trackerId") final String trackerId);

  /**
   * Inserts a postmail.
   * 
   * @param postmailBean
   *          the postmail information
   */
  void insertPostmail(@Param("postmailBean") final PostmailBean postmailBean);

  /**
   * Update a postmail.
   * 
   * @param postmailBean
   *          the postmail information
   */
  void updatePostmail(@Param("postmailBean") final PostmailBean postmailBean);

}
