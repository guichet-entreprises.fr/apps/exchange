/**
 * 
 */
package fr.ge.exchange.bean.constante;

/**
 * Interface regroupant les codes des messages à envoyer au Tracker.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface ICodeMessageTracker {

  /** Evènement lié à l'nevoi d'un courrier à l'imprimerie nationale. */
  String POSTMAIL_POSTED = "tracker.postmail.posted";

  /** Evènement lié à la mise à jour des statuts de courrier. */
  String POSTMAIL_UPDATED = "tracker.postmail.updated";

}
