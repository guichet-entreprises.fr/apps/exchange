/**
 * 
 */
package fr.ge.exchange.bean;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * Class representing a postmail.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class PostmailBean {

  /** Reference Id, nullable. */
  private String referenceId; // XXX LAB : retirer et mettre au pire dans une classe fille, utilisée
                              // uniquement à l'exterieur

  /** Tracker Id. */
  private String trackerId;

  /** Document id. */
  private String deliveryId;

  /** Recipient name. */
  private String recipientName;

  /** Recipient address. */
  private String recipientAddress;

  /** Recipient postal code. */
  private String recipientPostalCode;

  /** Recipient city. */
  private String recipientCity;

  /** Name of the attachmentPath. */
  private String attachmentPath;

  /** Date and time of postmail. */
  private Date datePostmail;

  /** Number of pages of the postmail. */
  private int numberPages;

  /** Imprimerie Nationale Id postmail. */
  private String impNatId;

  /** The channel used. */
  private String channel;

  /** The urgency of the postmail. */
  private String urgency;

  /** Status postmail. */
  private String status;

  /** recommandé number for the mail. */
  private String numreco;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof PostmailBean) {
      PostmailBean that = (PostmailBean) obj;
      return StringUtils.equals(this.referenceId, that.referenceId) && StringUtils.equals(this.trackerId, that.trackerId)
        && StringUtils.equals(this.deliveryId, that.deliveryId) && StringUtils.equals(this.recipientName, that.recipientName)
        && StringUtils.equals(this.recipientAddress, that.recipientAddress)
        && StringUtils.equals(this.recipientPostalCode, that.recipientPostalCode)
        && StringUtils.equals(this.recipientCity, that.recipientCity)
        && StringUtils.equals(this.attachmentPath, that.attachmentPath) && StringUtils.equals(this.impNatId, that.impNatId)
        && StringUtils.equals(this.channel, that.channel) && StringUtils.equals(this.urgency, that.urgency)
        && StringUtils.equals(this.status, that.status);
    }
    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return super.hashCode();
  }

  /**
   * Accesseur sur l'attribut {@link #referenceId}.
   *
   * @return String referenceId
   */
  public String getReferenceId() {
    return referenceId;
  }

  /**
   * Accesseur sur l'attribut {@link #trackerId}.
   *
   * @return String trackId
   */
  public String getTrackerId() {
    return trackerId;
  }

  /**
   * Mutateur sur l'attribut {@link #trackerId}.
   *
   * @param trackerId
   *          la nouvelle valeur de l'attribut trackId
   */
  public void setTrackId(String trackerId) {
    this.trackerId = trackerId;
  }

  /**
   * Mutateur sur l'attribut {@link #referenceId}.
   *
   * @param referenceId
   *          la nouvelle valeur de l'attribut referenceId
   */
  public void setReferenceId(final String referenceId) {
    this.referenceId = referenceId;
  }

  /**
   * Accesseur sur l'attribut {@link #deliveryId}.
   *
   * @return String deliveryId
   */
  public String getDeliveryId() {
    return deliveryId;
  }

  /**
   * Mutateur sur l'attribut {@link #deliveryId}.
   *
   * @param deliveryId
   *          la nouvelle valeur de l'attribut deliveryId
   */
  public void setDeliveryId(final String deliveryId) {
    this.deliveryId = deliveryId;
  }

  /**
   * Accesseur sur l'attribut {@link #recipientName}.
   *
   * @return String recipientName
   */
  public String getRecipientName() {
    return recipientName;
  }

  /**
   * Mutateur sur l'attribut {@link #recipientName}.
   *
   * @param recipientName
   *          la nouvelle valeur de l'attribut recipientName
   */
  public void setRecipientName(final String recipientName) {
    this.recipientName = recipientName;
  }

  /**
   * Accesseur sur l'attribut {@link #recipientAddress}.
   *
   * @return String recipientAddress
   */
  public String getRecipientAddress() {
    return recipientAddress;
  }

  /**
   * Mutateur sur l'attribut {@link #recipientAddress}.
   *
   * @param recipientAddress
   *          la nouvelle valeur de l'attribut recipientAddress
   */
  public void setRecipientAddress(final String recipientAddress) {
    this.recipientAddress = recipientAddress;
  }

  /**
   * Accesseur sur l'attribut {@link #recipientPostalCode}.
   *
   * @return String recipientPostalCode
   */
  public String getRecipientPostalCode() {
    return recipientPostalCode;
  }

  /**
   * Mutateur sur l'attribut {@link #recipientPostalCode}.
   *
   * @param recipientPostalCode
   *          la nouvelle valeur de l'attribut recipientPostalCode
   */
  public void setRecipientPostalCode(final String recipientPostalCode) {
    this.recipientPostalCode = recipientPostalCode;
  }

  /**
   * Accesseur sur l'attribut {@link #recipientCity}.
   *
   * @return String recipientCity
   */
  public String getRecipientCity() {
    return recipientCity;
  }

  /**
   * Mutateur sur l'attribut {@link #recipientCity}.
   *
   * @param recipientCity
   *          la nouvelle valeur de l'attribut recipientCity
   */
  public void setRecipientCity(final String recipientCity) {
    this.recipientCity = recipientCity;
  }

  /**
   * Accesseur sur l'attribut {@link #attachmentPath}.
   *
   * @return String attachmentPath
   */
  public String getAttachmentPath() {
    return attachmentPath;
  }

  /**
   * Mutateur sur l'attribut {@link #attachmentPath}.
   *
   * @param attachmentPath
   *          la nouvelle valeur de l'attribut attachmentPath
   */
  public void setAttachmentPath(final String attachmentPath) {
    this.attachmentPath = attachmentPath;
  }

  /**
   * Accesseur sur l'attribut {@link #datePostmail}.
   *
   * @return Date datePostmail
   */
  public Date getDatePostmail() {
    return datePostmail;
  }

  /**
   * Mutateur sur l'attribut {@link #datePostmail}.
   *
   * @param datePostmail
   *          la nouvelle valeur de l'attribut datePostmail
   */
  public void setDatePostmail(final Date datePostmail) {
    this.datePostmail = datePostmail;
  }

  /**
   * Accesseur sur l'attribut {@link #numberPages}.
   *
   * @return int numberPages
   */
  public int getNumberPages() {
    return numberPages;
  }

  /**
   * Mutateur sur l'attribut {@link #numberPages}.
   *
   * @param numberPages
   *          la nouvelle valeur de l'attribut numberPages
   */
  public void setNumberPages(final int numberPages) {
    this.numberPages = numberPages;
  }

  /**
   * Accesseur sur l'attribut {@link #impNatId}.
   *
   * @return int impNatId
   */
  public String getImpNatId() {
    return impNatId;
  }

  /**
   * Mutateur sur l'attribut {@link #impNatId}.
   *
   * @param impNatId
   *          la nouvelle valeur de l'attribut impNatId
   */
  public void setImpNatId(final String impNatId) {
    this.impNatId = impNatId;
  }

  /**
   * Accesseur sur l'attribut {@link #channel}.
   *
   * @return String channel
   */
  public String getChannel() {
    return channel;
  }

  /**
   * Mutateur sur l'attribut {@link #channel}.
   *
   * @param channel
   *          la nouvelle valeur de l'attribut channel
   */
  public void setChannel(final String channel) {
    this.channel = channel;
  }

  /**
   * Accesseur sur l'attribut {@link #urgency}.
   *
   * @return String urgency
   */
  public String getUrgency() {
    return urgency;
  }

  /**
   * Mutateur sur l'attribut {@link #urgency}.
   *
   * @param urgency
   *          la nouvelle valeur de l'attribut urgency
   */
  public void setUrgency(final String urgency) {
    this.urgency = urgency;
  }

  /**
   * Accesseur sur l'attribut {@link #status}.
   *
   * @return String status
   */
  public String getStatus() {
    return status;
  }

  /**
   * Mutateur sur l'attribut {@link #status}.
   *
   * @param status
   *          la nouvelle valeur de l'attribut status
   */
  public void setStatus(final String status) {
    this.status = status;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return super.toString();
  }

  /**
   * Return the full address of the recipient: {@link #recipientAddress} +
   * {@link #recipientPostalCode} + {@link #recipientCity}
   * 
   * @return the full address
   */
  public String getFullAddress() {
    StringBuilder fullAddress = new StringBuilder();
    fullAddress.append(this.getRecipientAddress()).append(" ");
    fullAddress.append(this.getRecipientPostalCode()).append(" ");
    fullAddress.append(this.getRecipientCity());
    return fullAddress.toString();
  }

  /**
   * Accesseur sur l'attribut {@link #numreco}.
   *
   * @return String numreco
   */
  public String getNumreco() {
    return numreco;
  }

  /**
   * Mutateur sur l'attribut {@link #numreco}.
   *
   * @param numreco
   *          la nouvelle valeur de l'attribut numreco
   */
  public void setNumreco(String numreco) {
    this.numreco = numreco;
  }

}
