/**
 * 
 */
package fr.ge.exchange.bean.constante;

/**
 * Interface of all the code exception possible in ge-exchange-service project.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface ICodeExceptionConstante {

  /** Error code exception for the generation of the pdf to post. */
  String ERR_EXCH_PDF_ERROR = "ERR_EXCH_PDF_ERROR";

  /** Error code exception for invalid JSON format. */
  String ERR_EXCH_INVALID_PARAMETER = "ERR_EXCH_INVALID_PARAMETER";

  /** Error code exception for invalid PDF format. */
  String ERR_EXCH_INVALID_PDF = "ERR_EXCH_INVALID_PDF";

  /** Error code exception for storage file into local file system. */
  String ERR_EXCH_DISK_ERROR = "ERR_EXCH_DISK_ERROR";

  /** Error code exception for transfering file into SFTP server. */
  String ERR_EXCH_SFTP_ERROR = "ERR_EXCH_SFTP_ERROR";

  /** Error code exception for registering postmail into database. */
  String ERR_EXCH_DB_ERROR = "ERR_EXCH_DB_ERROR";

  /** Error code exception for invalid JSON format. **/
  String ERR_EXCH_INVALID_FORMAT = "ERR_EXCH_INVALID_FORMAT";

  /** Error code exception for invalid track id. **/
  String ERR_EXCH_INVALID_ID = "ERR_EXCH_INVALID_ID";

  /** Error code exception for error when you call tracker for generation of ID. */
  String ERR_EXCH_TRACKER = "ERR_EXCH_TRACKER";

  /** Error code exception for sending email. */
  String ERR_EXCH_EMAIL = "ERR_EXCH_EMAIL";

  /** Error code exception for invalid csv file. */
  String ERR_EXCH_INVALID_CSV = "ERR_EXCH_INVALID_CSV";
}
