package fr.ge.exchange.dao;

/**
 * Mapper for SchemaVersion table.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface SchemaVersionDAO {

  /**
   * Gets the actual database version.
   * 
   * @return the version
   */
  String getVersion();

}
