/**
 * 
 */
package fr.ge.exchange.service.impl;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.MailMessage;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;
import fr.ge.exchange.bean.constante.ImprimerieNationaleResultHeaders;

/**
 * Utility class to parse the CSV results file of mail message processings.
 */
public class MailMessageResultsParser {
  /** The functional logger. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();
  /** The technical logger */
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  /**
   * Parse and extract MailMessage update from a csv file.
   * 
   * @param pathCsvFileLocal
   *          the csv file, not null
   * @return the list of MailMessage to update, not null
   * @throws TechniqueException
   *           if any technical problem occurs
   */
  public static List < MailMessage > parse(final String pathCsvFileLocal) throws TechniqueException {
    CSVFormat csvFileFormat = CSVFormat.EXCEL.withHeader(ImprimerieNationaleResultHeaders.FILE_HEADER_MAPPING).withDelimiter(';');
    CSVParser csvParser = null;
    List < MailMessage > messageUpdates = new ArrayList < MailMessage >();
    try {
      // initialisation du CSVParser Object
      csvParser = new CSVParser(new FileReader(pathCsvFileLocal), csvFileFormat);

      // Liste des enregistrement CSV
      List < CSVRecord > csvRecords = csvParser.getRecords();
      if (null == csvRecords || csvRecords.size() <= 1) {
        LOGGER_FONC.error("Pas d'entête dans le fichier csv {}.", pathCsvFileLocal);
        throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_INVALID_CSV,
          String.format("Fichier csv invalide %s.", pathCsvFileLocal));
      }

      for (int i = 1; i < csvRecords.size(); i++) {
        CSVRecord csvRecord = csvRecords.get(i);
        LOGGER_FONC.debug("Affichage de l'enregistrement du fichier n° {}.",
          csvRecord.get(ImprimerieNationaleResultHeaders.ID_FICHIER));

        MailMessage messageUpdate = new MailMessage();
        messageUpdate.setTrackerId(csvRecord.get(ImprimerieNationaleResultHeaders.FILLER_01));
        messageUpdate.setStatus(csvRecord.get(ImprimerieNationaleResultHeaders.STATUT));
        messageUpdate.setNumberPages(Integer.parseInt(csvRecord.get(ImprimerieNationaleResultHeaders.NB_PAGES)));
        messageUpdate.setChannel(csvRecord.get(ImprimerieNationaleResultHeaders.FILIERE));
        messageUpdate.setUrgency(csvRecord.get(ImprimerieNationaleResultHeaders.URGENCY));
        messageUpdate.setDeliveryId(csvRecord.get(ImprimerieNationaleResultHeaders.DELIVERY_ID));
        messageUpdate.setDeliveryCode(csvRecord.get(ImprimerieNationaleResultHeaders.FILLER_02));
        messageUpdate.setNumreco(csvRecord.get(ImprimerieNationaleResultHeaders.NUMRECO));
        messageUpdates.add(messageUpdate);
      }

    } catch (IOException e) {
      LOGGER_FONC.error(String.format("Erreur lors la lecture du fichier csv de mise à jour des statuts (%s).", pathCsvFileLocal),
        e);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_DB_ERROR,
        String.format("Erreur de lecture du fichier csv(%s).", pathCsvFileLocal), e);
    } finally {
      if (pathCsvFileLocal != null) {
        FileUtils.getFile(pathCsvFileLocal).delete();
      }
      if (null != csvParser) {
        try {
          csvParser.close();
        } catch (IOException e) {
          LOGGER_TECH.warn("unable to close the csv file ({}), ignored.", pathCsvFileLocal);
        }
      }
    }
    return messageUpdates;
  }

}
