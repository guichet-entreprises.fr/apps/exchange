/**
 * 
 */
package fr.ge.exchange.service.impl;

import java.io.InputStream;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.ct.ses.clients.SFTPClient;
import fr.ge.exchange.bean.MailMessage;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;
import fr.ge.exchange.bean.constante.ICodeMessageTracker;
import fr.ge.exchange.dao.PostmailDAO;
import fr.ge.exchange.domaine.IMailMessageDomaine;
import fr.ge.exchange.domaine.ImprimerieNationaleDeliveryIdGenerator;
import fr.ge.exchange.postmail.PostmailGenerator;
import fr.ge.exchange.service.IPostMailService;
import fr.ge.exchange.service.ITrackerLinkService;

/**
 * Implementation of services to post mails to competent authority.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class PostMailServiceImpl implements IPostMailService {

  /** The functional logger. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();
  /** The technical logger */
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  /** The PDF Generator */
  private PostmailGenerator postmailGenerator = new PostmailGenerator();

  /** Domaine service dedicated to mail message. */
  @Autowired
  private IMailMessageDomaine mailMessageDomaine;

  /** The postmail DAO mapper. */
  @Autowired
  private PostmailDAO postmailDAO;

  /** client service for SFTP manipulation. */
  @Autowired
  private SFTPClient sftpClient;

  /** service to call tracker. */
  @Autowired
  private ITrackerLinkService trackerLink;

  /**
   * {@inheritDoc}<br/>
   * If set, the {@link MailMessage#trackerId} is overwritten with a new Tracker Id.
   */
  @Override
  public String postMail(final InputStream documentToPost, final MailMessage mailMessage, final String referenceId,
    final String attachLocalStorageDirectory, final String attachSftpStorageDirectory) throws TechniqueException {

    // -->Validate JSON Format
    if (null == mailMessage) {
      String messageInvalidParameter = "Message empty or void, nothing will be send.";
      LOGGER_FONC.error(messageInvalidParameter);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_INVALID_PARAMETER, messageInvalidParameter);
    }

    // generation track id with tracker (if track id exists, we generated one, if its not, it
    // creates a new one)
    mailMessage.setTrackerId(trackerLink.createTrackerId(referenceId));

    // create the id for the Imprimerie Nationale
    ImprimerieNationaleDeliveryIdGenerator recommandeAccuseReception = ImprimerieNationaleDeliveryIdGenerator
      .create(mailMessage.getTrackerId()).rectoVerso().color().recommandeAccuseReception();
    mailMessage.setDeliveryId(recommandeAccuseReception.buildId());
    mailMessage.setDeliveryModeId(recommandeAccuseReception.getDeliveryCode());

    // -->Overlap PDF + Store ZIP file in local disk
    String zipPath = postmailGenerator.generatePostmailFromFile(documentToPost, attachLocalStorageDirectory,
      mailMessage.getDeliveryId(), mailMessage.getRecipientName(), mailMessage.getRecipientAddress(),
      mailMessage.getRecipientPostalCode(), mailMessage.getRecipientCity());

    try {
      // -->Store file in Imprimerie Nationale system
      this.storeOnRemoteLocation(zipPath, attachSftpStorageDirectory, mailMessage.getDeliveryId());
    } catch (TechniqueException e) {
      LOGGER_FONC.error("Unable to send the message {}.", mailMessage);
      throw e;
    }
    // -->Persistence execution into DB
    mailMessage.setAttachmentPath(zipPath);
    this.mailMessageDomaine.create(mailMessage);

    // -->Tracker notification
    trackerLink.addPostMailEvent(mailMessage.getTrackerId(), ICodeMessageTracker.POSTMAIL_POSTED);

    return mailMessage.getTrackerId();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional(readOnly = true, value = "ge_exchange")
  public MailMessage getMailMessage(final String trackerId) throws TechniqueException {
    return mailMessageDomaine.read(trackerId);
  }

  /**
   * Store zip attachment in imprimerie nationale sftp.
   *
   * @param localFileToSend
   *          zip file saved in local to send to Imprimerie Nationale
   * @param sftpRelativePath
   *          sftp relative path to store the file
   * @param postMailTrackerId
   *          tracker id
   * @throws TechniqueException
   *           technique exception
   */
  private void storeOnRemoteLocation(final String localFileToSend, final String sftpRelativePath, final String postMailTrackerId)
    throws TechniqueException {
    try {
      LOGGER_FONC.debug("Envoi du courrier ZIP [{}] vers le SFTP de l'Imprimerie Nationale : [{}]", localFileToSend,
        sftpRelativePath);
      sftpClient.televerser(localFileToSend, sftpRelativePath);
    } catch (TechniqueException e) {
      String messageErrorSftp = "Envoi de courrier + " + postMailTrackerId + ": erreur dépôt SFTP";
      LOGGER_FONC.error(messageErrorSftp, e);
      throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_SFTP_ERROR, messageErrorSftp, e);
    }
  }

}
