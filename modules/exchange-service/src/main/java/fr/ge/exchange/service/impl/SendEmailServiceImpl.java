/**
 * 
 */
package fr.ge.exchange.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.ct.email.SmtpClient;
import fr.ge.ct.email.model.TemplateEmailEnum;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;
import fr.ge.exchange.service.ISendEmailService;
import fr.ge.exchange.utils.PreconditionsGE;

/**
 * Implementation of services to send emails to competent authority.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class SendEmailServiceImpl implements ISendEmailService {

  /** La constante LOGGER_FONC. */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /** email utils. */
  @Autowired
  private SmtpClient smtpClient;

  /**
   * {@inheritDoc}
   * 
   */
  @Override
  public void sendEmail(final EmailSendBean emailInfo, final InputStream attachment) throws TechniqueException {
    // Check mandatory parameters
    this.checkPreconditions(emailInfo);
    this.checkPreconditionsAttachement(emailInfo, attachment);

    // Get list of recipients
    List < String > listRecipient = this.getListRecipient(emailInfo.getRecipient());

    // Send mail
    Map < String, String > mapData = new HashMap < String, String >();
    mapData.put("body", emailInfo.getContent());
    smtpClient.envoyerEmail(emailInfo.getSender(), listRecipient, emailInfo.getObject(), null, emailInfo.getAttachmentPDFName(),
      attachment, TemplateEmailEnum.DEFAULT_TEMPLATE.getNomFichierTemplate(), mapData, true);
  }

  /**
   * 
   * {@inheritDoc}
   */
  @Override
  public void sendEmail(final EmailSendBean emailInfo) throws TechniqueException {
    // Check mandatory parameters
    this.checkPreconditions(emailInfo);

    // Get list of recipients
    List < String > listRecipient = this.getListRecipient(emailInfo.getRecipient());

    // Send mail
    smtpClient.envoyerEmail(emailInfo.getSender(), listRecipient, emailInfo.getObject(), emailInfo.getContent(), null, null, null,
      null, false);
  }

  /**
   * Get recipients list from a string seperated by ';'.
   *
   * @param recipients
   *          string representing recipients separated by ';'
   * @return list of recipients
   */
  private List < String > getListRecipient(final String recipients) {
    return new ArrayList < String >(Arrays.asList(recipients.split(",")));
  }

  /**
   * Vérifie les préconditions à l'envoi de mail.
   *
   * @param emailInfo
   *          email info
   * @throws TechniqueException
   *           technique exception
   */
  private void checkPreconditions(final EmailSendBean emailInfo) throws TechniqueException {
    PreconditionsGE preconditionsGE = new PreconditionsGE();
    preconditionsGE.checkNotNull(emailInfo, "Le bean EmailSendBean est null");
    preconditionsGE.checkStringNotBlank(emailInfo.getRecipient(), "L'adresse mail destinataire est obligatoire");
    preconditionsGE.checkStringNotBlank(emailInfo.getObject(), "L'objet du mail est obligatoire.");
    preconditionsGE.checkStringNotBlank(emailInfo.getContent(), "Le corps du mail est obligatoire.");
    preconditionsGE.checkStringNotBlank(emailInfo.getSender(), "L'adresse mail expéditeur est obligatoire.");
  }

  /**
   * Vérifie les préconditions à l'envoi de mail avec pièce jointe.
   * 
   * @param emailInfo
   *          email info
   * @param attachment
   * @throws TechniqueException
   */
  private void checkPreconditionsAttachement(final EmailSendBean emailInfo, final InputStream attachment)
    throws TechniqueException {
    PreconditionsGE preconditionsGE = new PreconditionsGE();
    preconditionsGE.checkNotNull(attachment, "Le bean EmailSendBean est null");
    preconditionsGE.checkStringNotBlank(emailInfo.getAttachmentPDFName(), "Le nom du fichier PDF est obligatoire.");
  }
}
