/**
 * 
 */
package fr.ge.exchange.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;

/**
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
public class PreconditionsGE {

  /** Logger fonctionnel */
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /** Code erreur par défaut */
  private String codeErreur = ICodeExceptionConstante.ERR_EXCH_INVALID_PARAMETER;

  /**
   * Constructeur de la classe.
   *
   */
  public PreconditionsGE() {
  }

  /**
   * Constructeur de la classe.
   *
   * @param codeErreur
   *          TODO code erreur d'origine inconnue
   */
  public PreconditionsGE(String codeErreur) {
    this.codeErreur = codeErreur;
  }

  /**
   * Accesseur sur l'attribut {@link #codeErreur}.
   *
   * @return String codeErreur
   */
  public String getCodeErreur() {
    return codeErreur;
  }

  /**
   * Mutateur sur l'attribut {@link #codeErreur}.
   *
   * @param codeErreur
   *          la nouvelle valeur de l'attribut codeErreur
   */
  public void setCodeErreur(String codeErreur) {
    this.codeErreur = codeErreur;
  }

  /**
   * Vérifie si la référence passée en paramètres n'est pas null, si la référence est nulle lève une
   * exception technique sinon renvoie la référence.
   * 
   * @param <T>
   *          type générique
   * @param reference
   *          reference de l'objet à vérifier
   * @param strReference
   *          nom de la référence
   * @return this inutilement
   * @throws TechniqueException
   *           Exception technique
   */
  public <T> PreconditionsGE checkNotNull(T reference, String strReference) throws TechniqueException {
    if (reference == null) {
      LOGGER_FONC.error(strReference);
      throw new TechniqueException(codeErreur, strReference);
    } else {
      return this;
    }
  }

  /**
   * Vérifie si le string passé en paramètre n'est pas vide ou null, lève une exception technique
   * sinon renvoie le string.
   * 
   * @param <T>
   *          type générique inutile
   * @param reference
   *          référence du string à vérifier
   * @param strReference
   *          nom du string à vérifier
   * @return le string si aucune exception
   * @throws TechniqueException
   *           exception technique
   */
  public <T> PreconditionsGE checkStringNotBlank(String reference, String strReference) throws TechniqueException {
    if (reference == null || reference.isEmpty() || StringUtils.isBlank(reference)) {
      LOGGER_FONC.error(strReference);
      throw new TechniqueException(codeErreur, strReference);
    } else {
      return this;
    }
  }

}
