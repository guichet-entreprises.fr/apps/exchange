/**
 * 
 */
package fr.ge.exchange.bean.constante;

/**
 * Enumeration with coordinates of the PDF zone reserved for the address.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public enum AddressZoneEnum {

  /** x = 90 mm. */
  X(255.118),

  /** y = 40 mm. */
  Y(113.386),

  /** w = 120 mm. */
  W(340.157),

  /** h = 45 mm. */
  H(127.559);

  /** point (1 point = 0,352778 mm). */
  private double point;

  /**
   * Instantie un nouveau address zone enum.
   *
   * @param point
   *          point
   */
  AddressZoneEnum(final double point) {
    this.point = point;
  }

  /**
   * Accesseur sur l'attribut {@link #point}.
   *
   * @return double point
   */
  public double getPoint() {
    return this.point;
  }

}
