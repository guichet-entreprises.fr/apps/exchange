/**
 * 
 */
package fr.ge.exchange.bean.constante;

/**
 * Constante des entetes postmail.
 * 
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
public interface ImprimerieNationaleResultHeaders {

  /** Header du fichier csv du sftp. */
  String[] FILE_HEADER_MAPPING = {"doc_date", "doc_time", "company_name", "service_name", "user_name", "filiere", "urgency",
    "Nom du modele", "Destinataire", "numreco", "statut", "nb_pages", "archive", "Date depot poste", "id_fichier", "jobpath",
    "ld_doc", "nimmat", "filler_01", "filler_02", "filler_03", "filler_04", "filler_05", "latest_date", "IdMailPiece", "IdJob" };

  /** Attribut filler_01 du csv du correspondant au trackid en BDD. */
  String FILLER_01 = "filler_01";

  /** Attribut fichier du csv du sftp. */
  String ID_FICHIER = "id_fichier";

  /** Attribut statut du csv. */
  String STATUT = "statut";

  /** Attribut nb_pages du csv. */
  String NB_PAGES = "nb_pages";

  /** Attribut numreco du csv. */
  String NUMRECO = "numreco";

  /** Attribut filiere du csv. */
  String FILIERE = "filiere";

  /** Attribut urgency du csv. */
  String URGENCY = "urgency";

  /** Attribut doc id du csv. */
  String DELIVERY_ID = "ld_doc";

  /** Attribut filler qui contient la partie mode d'impression */
  String FILLER_02 = "filler_02";
}
