/**
 * 
 */
package fr.ge.exchange.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.MessageTrackerProvider;
import fr.ge.exchange.service.ITrackerLinkService;
import fr.ge.exchange.utils.UidFactoryImpl;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * Implementation for services to call Tracker.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class TrackerLinkServiceImpl implements ITrackerLinkService {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /** Le logger technique. */
    private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

    /** tracker facade. */
    @Autowired
    private ITrackerFacade trackerFacade;

    /**
     * Message Provider.
     */
    private MessageTrackerProvider messageTrackerProvider = MessageTrackerProvider.getInstance();

    /**
     * {@inheritDoc}
     */
    public void addPostMailEvent(final String trackId, final String eventCode) {
        String messageTracker = null;
        try {
            messageTracker = messageTrackerProvider.getMessage(eventCode, trackId);
            trackerFacade.post(trackId, messageTracker);
            LOGGER_FONC.info("Appel au tracker effectué => Post mail id [{}] : ajout du message [{}]", trackId, messageTracker);
        } catch (RuntimeException e) {
            LOGGER_TECH.warn(String.format("Erreur lors de l'appel Tracker pour l'envoi de courrier post mail : message [%s] associé à l'id de suivi [%s].", messageTracker, trackId), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createTrackerId(String referenceId) throws TechniqueException {
        String finalTrackId = null;
        try {
            finalTrackId = trackerFacade.createUid();
            LOGGER_FONC.info("Appel Tracker => Génération Track ID : {} (reference : {}).", finalTrackId, referenceId);
        } catch (RuntimeException e) {
            String messageErrorTracker = String.format("Erreur lors de l'appel Tracker pour la génération du Track ID final (%s) avec la référence %s.", finalTrackId, referenceId);
            LOGGER_TECH.error(messageErrorTracker, e);
            finalTrackId = new UidFactoryImpl().uid();
        }
        if (StringUtils.isNotBlank(referenceId)) {
            LOGGER_FONC.info("Attaching tracker Id {} to reference Id {} ...", finalTrackId, referenceId);
            String textualResponse = trackerFacade.link(finalTrackId, referenceId);
            // useless answer from tracker server, let's log it
            LOGGER_FONC.info(textualResponse);
        }
        return finalTrackId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addUpdatePostmailStatuts(String trackId, String eventCode) {
        String messageTracker = null;
        try {
            messageTracker = messageTrackerProvider.getMessage(eventCode, trackId);
            trackerFacade.post(trackId, messageTracker);
            LOGGER_FONC.info("Appel au tracker effectué => Post mail id [{}] : ajout du message [{}]", trackId, messageTracker);
        } catch (RuntimeException e) {
            LOGGER_TECH.warn(String.format("Erreur lors de l'appel Tracker pour la mise à jour du statut du courrier post mail : message [%s] associé à l'id de suivi [%s].", messageTracker, trackId),
                    e);
        }

    }
}
