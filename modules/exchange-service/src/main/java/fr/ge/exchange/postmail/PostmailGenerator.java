/**
 * 
 */
package fr.ge.exchange.postmail;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.multipdf.Overlay;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.slf4j.Logger;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.exchange.bean.constante.AddressZoneEnum;
import fr.ge.exchange.bean.constante.IAdressFormConstant;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;
import fr.ge.exchange.bean.constante.SilenceZoneEnum;
import fr.ge.exchange.domaine.ImprimerieNationaleDeliveryIdGenerator;
import fr.ge.exchange.utils.PDFBoxUtils;
import fr.ge.exchange.utils.PdfArchiveGenerator;

/**
 * Utility class : PDFs generations and manipulations for Exchange.
 */
public class PostmailGenerator {

    /** La constante EXTENSION_PDF. */
    private static final String EXTENSION_PDF = ".pdf";

    /** La constante EXTENSION_ZIP. */
    private static final String EXTENSION_ZIP = ".zip";

    /** The constant EMPTY_ZONE_PDF which indicates that text zone is empty. */
    private static final String EMPTY_ZONE_PDF = "\r\n";

    /** The constante REGION_ADDRESS. */
    private static final String REGION_ADDRESS = "regionAddress";

    /** The constante REGION_SILENCE. */
    private static final String REGION_SILENCE = "regionSilence";

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /**
     * classpath location of the address filter file. That's where the address
     * is set. <br>
     * Default value : /pdf/addressFilter_1field.pdf
     */
    private String addressFilterFile = "/pdf/addressFilter_1field.pdf";

    /**
     * Flag to generate a PDF/A file (not fully functionnal). Default : false.
     */
    private boolean doGenerateArchivePDF = false;

    /**
     * Constructor. <br/>
     * the file filter default to {@link #addressFilterFile}
     */
    public PostmailGenerator() {
        // No op
    }

    /**
     * Constructor.
     * 
     * @param filterFilename
     *            path of the filter file within the classpath, not null,
     *            consistent
     */
    public PostmailGenerator(String filterFilename) {
        this.addressFilterFile = filterFilename;
    }

    /**
     * Generates a PDF from a fusion of a template and an address filter filled
     * with parameters and save it as a zip file.
     *
     * @param documentStream
     *            input stream of the document
     * @param localPdfDirectoryStorage
     *            local directory to store the zip generated
     * @param deliveryId
     *            the unique id to trace the message, contains the trackerId and
     *            the print code (see
     *            {@link ImprimerieNationaleDeliveryIdGenerator})
     * @param recipientName
     *            the name of the recipient, not empty
     * @param recipientAddress
     *            the address of the recipient, not empty
     * @param recipientPostalCode
     *            the postal code of the recipient
     * @param recipientCity
     *            the city of the recipient
     * @return the path of the generated ZIP file
     * @throws TechniqueException
     *             TechniqueException if : ERR_EXCH_PDF_ERROR,
     *             ERR_EXCH_DISK_ERROR or ERR_EXCH_INVALID_PDF
     */
    public String generatePostmailFromFile(final InputStream documentStream, final String localPdfDirectoryStorage, String deliveryId, String recipientName, String recipientAddress,
            String recipientPostalCode, String recipientCity) throws TechniqueException {
        PDDocument finalPdf = null;
        Overlay overlay = new Overlay();
        PDDocument document = null;
        File filterFilledFile = null;
        String zipFilePath = null;
        try {
            filterFilledFile = fillAndSaveAddressFilter(deliveryId, recipientName, recipientAddress, recipientPostalCode, recipientCity);

            // Get the template document and check if the PDF is valid
            try {
                document = PDDocument.load(documentStream);
                int correctedPages = PDFBoxUtils.correctNullPages(document);
                LOGGER_FONC.info("The document {} have been checked for null pages and {} pages have been corrected.", deliveryId, correctedPages);
            } catch (IOException e) {
                String messageInvalidPdf = String.format("Envoi de courrier %s : fichier PDF invalide.", deliveryId);
                LOGGER_FONC.error(messageInvalidPdf);
                throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_INVALID_PDF, messageInvalidPdf);
            }

            // Check if the content of the PDF is valid (no text in reserved
            // zones)
            boolean isAddressZoneEmpty = PostmailGenerator.checkReservedZoneEmpty(document.getPage(0), deliveryId);
            if (isAddressZoneEmpty == false) {
                LOGGER_FONC.info("No enough space on the first page to write the address, adding a new empty page as the first page for {}.", deliveryId);
                // if address zone is not free, two new pages will be added
                // for preparing rectoVerso printing
                PDFBoxUtils.addPage(document, 0);
                PDFBoxUtils.addPage(document, 0);

            }

            // Fusion of the template with the address filter
            overlay.setInputPDF(document);
            overlay.setFirstPageOverlayFile(filterFilledFile.getAbsolutePath());
            overlay.setOverlayPosition(Overlay.Position.FOREGROUND);
            HashMap<Integer, String> overlayGuide = new HashMap<Integer, String>();
            overlayGuide.put(0, filterFilledFile.getAbsolutePath());
            finalPdf = overlay.overlay(overlayGuide);

            // we dont need anymore the filter filled => we delete it
            filterFilledFile.delete();
        } catch (IOException e) {
            if (filterFilledFile != null) {
                filterFilledFile.delete();
            }
            String messageErrorGenerationPdf = String.format("Erreur lors de la génération du PDF d'envoi de courrier ayant pour ID de suivi : %s.", deliveryId);
            LOGGER_FONC.error(messageErrorGenerationPdf, e);
            throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_PDF_ERROR, messageErrorGenerationPdf, e);
        }

        //
        // Generate the PDF/A
        //
        if (doGenerateArchivePDF) {
            PDDocument finalPdfA = PdfArchiveGenerator.generateArchive(finalPdf);
            try {
                finalPdf.close();
            } catch (IOException e) {
                String messagePdf = String.format("Erreur lors de la fermeture du document d'origine après la génération du PDF/A : %s.", deliveryId);
                LOGGER_FONC.error(messagePdf, e);
                throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_DISK_ERROR, messagePdf, e);
            }
            finalPdf = finalPdfA;
        }

        //
        // -->Store zip file in local disk
        //
        try {
            zipFilePath = generateAndSaveZipPostmailFile(deliveryId, localPdfDirectoryStorage, finalPdf);
            // Close inputs
            document.close();
            overlay.close();
            finalPdf.close();
        } catch (IOException e) {
            String messageStoragePdf = String.format("Erreur lors de la sauvegarde du ZIP d'envoi de courrier ayant pour ID de suivi : %s.", deliveryId);
            LOGGER_FONC.error(messageStoragePdf, e);
            throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_DISK_ERROR, messageStoragePdf, e);
        }
        return zipFilePath;
    }

    /**
     * Check reserved zone empty.
     *
     * @param page
     *            page
     * @param trackId
     *            track id
     * @return true if the zone is free, false otherwise
     * @throws IOException
     *             if an i/o exception occurs
     * @throws TechniqueException
     *             technique exception reserved zone is not empty
     */
    private static boolean checkReservedZoneEmpty(final PDPage page, final String trackId) throws IOException, TechniqueException {
        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
        Rectangle2D.Double regionAddress = new Rectangle2D.Double(AddressZoneEnum.X.getPoint(), AddressZoneEnum.Y.getPoint(), AddressZoneEnum.W.getPoint(), AddressZoneEnum.H.getPoint());
        Rectangle2D.Double regionSilence = new Rectangle2D.Double(SilenceZoneEnum.X.getPoint(), SilenceZoneEnum.Y.getPoint(), SilenceZoneEnum.W.getPoint(), SilenceZoneEnum.H.getPoint());
        stripper.addRegion(PostmailGenerator.REGION_ADDRESS, regionAddress);
        stripper.addRegion(PostmailGenerator.REGION_SILENCE, regionSilence);
        stripper.extractRegions(page);
        String textAddress = stripper.getTextForRegion(PostmailGenerator.REGION_ADDRESS);
        String textSilence = stripper.getTextForRegion("regionSilence");
        if (!PostmailGenerator.isTextEmptyZonePdf(textAddress) || !PostmailGenerator.isTextEmptyZonePdf(textSilence)) {
            String messageInvalidContentPdf = String.format("Envoi de courrier %s : des zones réservées à l'Imprimerie Nationale sont remplies sur le template", trackId);
            LOGGER_FONC.warn(messageInvalidContentPdf);
            return false;
            // throw new
            // TechniqueException(ICodeExceptionConstante.ERR_EXCH_INVALID_PDF,
            // messageInvalidContentPdf);
        }
        return true;
    }

    /**
     * Check if a text corresponds to an empty PDF zone.
     *
     * @param text
     *            text
     * @return true, if empty
     */
    private static boolean isTextEmptyZonePdf(final String text) {
        return org.apache.commons.lang3.StringUtils.isBlank(text) || text.equals(EMPTY_ZONE_PDF);
    }

    /**
     * Fill the pdf filter with address parameters.
     *
     * @param postMailInfo
     *            post mail info to fill
     * @return the pdf filter filled file
     * @throws IOException
     *             Signale qu'une exception I/O est apparue
     */
    private File fillAndSaveAddressFilter(String trackerId, String recipientName, String recipientAddress, String recipientPostalCode, String recipientCity) throws IOException {

        // XXX LAB : externaliser ce chargement de pdf
        PDDocument filter;

        filter = PDDocument.load(PostmailGenerator.class.getResourceAsStream(addressFilterFile));
        LOGGER_FONC.info("Loading filter {} for document {}.", addressFilterFile, trackerId);

        // adding fonts to the document
        // pb avec dernier template
        // prepareFont(filter, PDType1Font.HELVETICA);
        PDDocumentCatalog docCatalog = filter.getDocumentCatalog();
        PDAcroForm acroForm = docCatalog.getAcroForm();

        PDField fieldAddress = acroForm.getField(IAdressFormConstant.ADRESS);
        fieldAddress.setValue(String.format("%s\n%s\n%s %s", recipientName, recipientAddress, recipientPostalCode, recipientCity));

        // Filling the trackId field
        PDField fieldTrackId = acroForm.getField(IAdressFormConstant.TRACK_ID);
        // changing the color of the track id, set it to white (1 1 1), for this
        // field only
        COSDictionary cosDictionary = fieldTrackId.getCOSObject();
        COSBase defaultAppearance = cosDictionary.getDictionaryObject(COSName.DA);
        cosDictionary.setString(COSName.DA, (defaultAppearance != null ? defaultAppearance.toString() : "") + " 1 1 1 rg ");
        fieldTrackId.setValue(trackerId);

        // We must "flatten" (aplatir) the form, so it can be visible on the
        // final pdf
        acroForm.flatten();
        // Temporary file containing the filter filled with the address
        String tempFile = "tmp_" + UUID.randomUUID().toString();
        File filterFilledFile = File.createTempFile(tempFile, EXTENSION_PDF);
        filter.save(filterFilledFile);
        LOGGER_FONC.debug("PostMail : sauvegarde temporaire du filtre d'adresse valorisée vers [{}]", tempFile);
        filter.close();
        return filterFilledFile;
    }

    /**
     * Generate a ZIP file from the PDF attachment to send to the Imprimerie
     * Nationale.
     *
     * @param trackId
     *            track id
     * @param localPdfDirectoryStorage
     *            local pdf directory storage
     * @param finalPdf
     *            final pdf generated to put in ZIP file
     * @return string
     * @throws IOException
     *             Signale qu'une exception I/O est apparue
     */
    private static String generateAndSaveZipPostmailFile(final String trackId, final String localPdfDirectoryStorage, final PDDocument finalPdf) throws IOException {
        // Rule for name of PDF and ZIP : 'dateYYYY-MM-DD'_'idpostmail'.pdf
        String currentDay = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        String pdfName = currentDay + "_" + trackId + EXTENSION_PDF;
        String pdfFilePath = localPdfDirectoryStorage + File.separator + pdfName;
        File pdfFile = new File(pdfFilePath);
        finalPdf.save(pdfFile);
        LOGGER_FONC.debug("PostMail : sauvegarde temporaire du courrier PDF rempli vers [{}]", pdfFilePath);

        // generation of the zip file where you put the pdf
        String zipName = currentDay + "_" + trackId + EXTENSION_ZIP;
        String zipFilePath = localPdfDirectoryStorage + File.separator + zipName;
        File zipFile = new File(zipFilePath);
        try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFile)); FileInputStream fis = new FileInputStream(pdfFile)) //
        {
            ZipEntry pdfEntry = new ZipEntry(pdfName);
            zipOut.putNextEntry(pdfEntry);
            byte[] bytes = new byte[fis.available()];
            fis.read(bytes);
            zipOut.write(bytes);
            zipOut.closeEntry();
        }
        LOGGER_FONC.debug("PostMail : sauvegarde du courrier ZIP sur [{}]", zipFilePath);

        // We don't need the pdf anymore => we delete it
        pdfFile.delete();

        return zipFilePath;
    }

    /**
     * Mutateur sur l'attribut {@link #addressFilterFile}.
     *
     * @param addressFilterFile
     *            la nouvelle valeur de l'attribut addressFilterFile
     */
    public void setAddressFilterFile(String addressFilterFile) {
        this.addressFilterFile = addressFilterFile;
    }

    /**
     * Mutateur sur l'attribut {@link #doGenerateArchivePDF}.
     *
     * @param doGenerateArchivePDF
     *            la nouvelle valeur de l'attribut doGenerateArchivePDF
     */
    public void setDoGenerateArchivePDF(boolean doGenerateArchivePDF) {
        this.doGenerateArchivePDF = doGenerateArchivePDF;
    }

}
