package fr.ge.ct.email;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.ct.email.model.SmtpConfiguration;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;

/**
 * La classe utilitaire pour envoi de mails.
 */
public class SmtpClient {

    /** La constante LOGGER_FONC. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /**
     * Supplier of Message, created from the javamail Session and the senders's
     * email address.
     */
    private final Function<String, MimeMessage> mimeMessageSupplier;

    /** Consumer of Message, the Transport layer of javamail. */
    private final Consumer<Message> messageSender;

    /** Le template engine mail. */
    @Autowired
    private TemplateEngine templateEngineMail;

    /** SMTP configuration. */
    @Autowired
    private SmtpConfiguration smtpConfiguration;

    /** Maximum chunk size. */
    @Value("${attachment.max.size:5000000}")
    private Long maxChunkSize;

    /**
     * Instantiates a new smtp client.
     */
    public SmtpClient() {
        this.messageSender = message -> {
            try {
                Transport.send(message);
            } catch (final MessagingException e) {
                // don't know what else to do here
                throw new RuntimeException(e);
            }
        };

        this.mimeMessageSupplier = senderEmail -> {
            final Properties properties = this.getMailProperties();
            properties.put("mail.smtp.from", senderEmail);
            return new MimeMessage(Session.getInstance(properties, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(SmtpClient.this.smtpConfiguration.getUser(), SmtpClient.this.smtpConfiguration.getPassword());
                }
            }));
        };
    }

    /**
     * Envoi d'un email.
     *
     * @param senderEmail
     *            sender email
     * @param recipients
     *            recipients
     * @param object
     *            object
     * @param body
     *            body
     * @param attachmentName
     *            name of the PDF attachment
     * @param attachment
     *            attachment
     * @param template
     *            mail body template name
     * @param mapData
     *            data to inject into the template context
     * @param withAttachment
     *            is there an attachment ?
     * @throws TechniqueException
     *             technique exception
     */
    public void envoyerEmail(final String senderEmail, final List<String> recipients, final String object, final String body, final String attachmentName, final InputStream attachment,
            final String template, final Map<String, String> mapData, final boolean withAttachment) throws TechniqueException {
        final String preparedSenderEmail = this.filteredSenderAddress(senderEmail);
        try {
            String preparedBody = body;
            if (null != mapData && null != template) {
                preparedBody = this.constructBody(mapData, template);
            }
            if (withAttachment) {
                LOGGER_FONC.debug("Construction de la pièce jointe au format PDF");
                final List<File> chunks = new PdfSplitter(this.maxChunkSize).load(attachment).split().asFiles();
                if (chunks.size() == 1) {
                    this.callMessageSender(preparedSenderEmail, recipients, object, preparedBody, attachmentName, chunks.get(0), withAttachment);
                } else {
                    final String[] chunkNameSplit = attachmentName.split("\\.");
                    final String chunkNameBase = StringUtils.join(chunkNameSplit, ".", 0, chunkNameSplit.length - 1);
                    final String chunkNameExtension = "." + chunkNameSplit[chunkNameSplit.length - 1];
                    int i = 1;
                    for (final File chunk : chunks) {
                        final String chunkObject = object + " (" + i + "/" + chunks.size() + ")";
                        final String chunkName = chunkNameBase + "-" + i + chunkNameExtension;
                        if (i == 1) {
                            this.callMessageSender(preparedSenderEmail, recipients, chunkObject, preparedBody, chunkName, chunk, withAttachment);
                        } else {
                            final String chunkBody = String.format("Veuillez trouver ci-joint la partie n°%s sur %s de la pièce jointe.", i, chunks.size());
                            this.callMessageSender(preparedSenderEmail, recipients, chunkObject, chunkBody, chunkName, chunk, withAttachment);
                        }
                        ++i;
                    }
                }
            } else {
                this.callMessageSender(preparedSenderEmail, recipients, object, preparedBody, attachmentName, null, withAttachment);
            }
        } catch (MessagingException | IOException | TechniqueException | RuntimeException e) {
            LOGGER_FONC.error("L'envoi du mail '{}' aux destinataires [{}] avec la pièce jointe '{}' a échoué.", object, recipients, attachmentName, e);
            throw new TechniqueException(ICodeExceptionConstante.ERR_EXCH_EMAIL, e.getMessage(), e);
        }
        LOGGER_FONC.info("Email envoyé avec succès de : {} à {}.", preparedSenderEmail, recipients);
    }

    /**
     * Filter the email address of the sender according to the smtp
     * configuration, is replace by the default no-reply address.
     *
     * @param emailExpediteur
     *            the email expediteur
     * @return the validated address
     */
    private String filteredSenderAddress(final String emailExpediteur) {

        final List<String> domains = this.smtpConfiguration.getAllowedDomains();
        if (emailExpediteur != null && domains != null & !domains.contains(emailExpediteur.substring(emailExpediteur.lastIndexOf('@') + 1))) {
            // the sender's email address is not in an allowed domain, it has to
            // be replaced by the
            // default one
            return this.smtpConfiguration.getDefaultNoreplyAddress();
        }
        return emailExpediteur;
    }

    /**
     * Construct message of the mail with text and attachment, then sends the
     * message.
     *
     * @param senderEmail
     *            sender email
     * @param recipients
     *            recipients
     * @param object
     *            object
     * @param body
     *            body
     * @param attachmentName
     *            attachment name
     * @param pdfFile
     *            the pdf file
     * @param withAttachment
     *            with attachment ?
     * @throws AddressException
     *             address exception
     * @throws MessagingException
     *             messaging exception
     * @throws InvalidPasswordException
     *             invalid password exception
     * @throws IOException
     *             Signale qu'une exception I/O est apparue
     * @throws TechniqueException
     *             technique exception
     */
    private void callMessageSender(final String senderEmail, final List<String> recipients, final String object, final String body, final String attachmentName, final File pdfFile,
            final boolean withAttachment) throws AddressException, MessagingException, InvalidPasswordException, IOException, TechniqueException {
        final MimeMessage message = this.mimeMessageSupplier.apply(senderEmail);
        LOGGER_FONC.debug("Construction du courriel");

        message.setFrom(new InternetAddress(senderEmail));
        message.setSubject(object);
        message.setSentDate(Calendar.getInstance().getTime());

        // Construction of recipients
        for (final String recipient : recipients) {
            LOGGER_FONC.info("Le destinataire du mail est : {}.", recipient);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        }

        if (withAttachment) {
            LOGGER_FONC.debug("Ajout d'une nouvelle pièce jointe {} au courriel", attachmentName);
            final Multipart multipart = new MimeMultipart();
            final BodyPart bodyPartMessage = new MimeBodyPart();
            bodyPartMessage.setContent(body, "text/html; charset=utf-8");
            multipart.addBodyPart(bodyPartMessage);

            // Construction of the PDF attachment to send with the mail
            LOGGER_FONC.debug("Construction de la pièce jointe au format PDF");
            final DataSource source = new FileDataSource(pdfFile.getAbsolutePath());
            final MimeBodyPart bodyPartPieceJointe = new MimeBodyPart();
            bodyPartPieceJointe.setDataHandler(new DataHandler(source));
            bodyPartPieceJointe.setFileName(attachmentName);
            multipart.addBodyPart(bodyPartPieceJointe);

            message.setContent(multipart);
        } else {
            message.setContent(body, "text/html; charset=utf-8");
        }
        LOGGER_FONC.debug("Construction avec succès du corps du courriel au format HTML");
        this.messageSender.accept(message);
    }

    /**
     * Retourne une session javax.mail.
     *
     * @return session
     */
    public Session manageSession() {
        final Properties props = this.getMailProperties();
        Session session = null;
        if (StringUtils.isNotBlank(this.smtpConfiguration.getUser()) && StringUtils.isNotBlank(this.smtpConfiguration.getPassword())) {
            session = Session.getInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(SmtpClient.this.smtpConfiguration.getUser(), SmtpClient.this.smtpConfiguration.getPassword());
                }
            });
        } else {
            session = Session.getDefaultInstance(props);
        }
        LOGGER_FONC.debug("Construction avec succès d'une nouvelle session pour l'envoi de courriel à partir des fichiers de propriétés");
        return session;
    }

    /**
     * Construit un fichier properties contenant la configuration d'envoi de
     * mail pour JavaxMail.<br/>
     * Not null known properties :
     * <ul>
     * <li>mail.smtp.port</li>
     * <li>mail.smtp.host</li>
     * <li>mail.smtp.auth</li>
     * <li>mail.smtp.starttls.enable</li>
     * <li>mail.smtp.user</li>
     * <li>mail.smtp.connectiontimeout</li>
     * <li>mail.smtp.timeout</li>
     *
     * @return properties
     */
    public Properties getMailProperties() {
        final Properties properties = new Properties();
        properties.put("mail.smtp.port", this.smtpConfiguration.getPort());
        properties.put("mail.smtp.host", this.smtpConfiguration.getHost());
        properties.put("mail.smtp.auth", this.smtpConfiguration.getAuth());
        properties.put("mail.smtp.starttls.enable", this.smtpConfiguration.getStarttlsEnable());
        properties.put("mail.smtp.user", this.smtpConfiguration.getUser());
        properties.put("mail.smtp.connectiontimeout", this.smtpConfiguration.getConnectionTimeout());
        properties.put("mail.smtp.timeout", this.smtpConfiguration.getTimeout());

        return properties;
    }

    /**
     * Construire le corps du mail.
     *
     * @param map
     *            le map
     * @param template
     *            template mail
     * @return le corps du mail
     * @throws IOException
     *             quand erreur
     */
    public String constructBody(final Map<String, String> map, final String template) throws IOException {
        final Context ctx = new Context();
        if (null != map && !map.isEmpty()) {
            for (final Map.Entry<String, String> e : map.entrySet()) {
                ctx.setVariable(e.getKey(), e.getValue());
            }
        }
        String corps = null;
        if (null != template) {
            corps = this.templateEngineMail.process(template, ctx);
            LOGGER_FONC.debug("Génération avec succès du courriel à partir du template {} et des données de contexte", template);
        }

        return corps;
    }

    /**
     * Mutateur sur l'attribut {@link #templateEngineMail}.
     *
     * @param templateEngineMail
     *            la nouvelle valeur de l'attribut templateEngineMail
     */
    public void setTemplateEngineMail(final TemplateEngine templateEngineMail) {
        this.templateEngineMail = templateEngineMail;
    }

    /**
     * Sets the max chunk size.
     *
     * @param maxChunkSize
     *            the new max chunk size
     */
    public void setMaxChunkSize(final Long maxChunkSize) {
        this.maxChunkSize = maxChunkSize;
    }

}
