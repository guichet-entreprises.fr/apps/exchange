/**
 * 
 */
package fr.ge.ct.email.model;

/**
 * L'énumération des templates de mail envoyés spécifiquement par Exchange-WEBCLIENT.
 * 
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public enum TemplateEmailEnum {

  /** Le template du mail par défaut (en text brut). */
  DEFAULT_TEMPLATE("templateDefaultMail.html");

  /** nom fichiertemplate. */
  private String nomFichierTemplate;

  /**
   * Instantie une nouvelle énumération.
   *
   * @param nomFichiertemplate
   *          nom fichiertemplate
   */
  TemplateEmailEnum(final String nomFichiertemplate) {
    this.nomFichierTemplate = nomFichiertemplate;
  }

  /**
   * Accesseur sur l'attribut {@link #nomFichierTemplate}.
   *
   * @return String nomFichierTemplate
   */
  public String getNomFichierTemplate() {
    return nomFichierTemplate;
  }
}
