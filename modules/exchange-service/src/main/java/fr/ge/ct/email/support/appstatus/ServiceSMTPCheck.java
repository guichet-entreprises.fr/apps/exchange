/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.ct.email.support.appstatus;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import fr.ge.core.log.GestionnaireTrace;
import fr.ge.ct.email.model.SmtpConfiguration;
import net.sf.appstatus.core.check.AbstractCheck;
import net.sf.appstatus.core.check.CheckResultBuilder;
import net.sf.appstatus.core.check.ICheckResult;

/**
 * Checking if the SMTP server is alive.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ServiceSMTPCheck extends AbstractCheck {

  /** The technical logger. */
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  /** The SMTP Configuration Bean. **/
  private SmtpConfiguration smtpConfiguration = SmtpConfiguration.getInstance();

  @Override
  public String getGroup() {
    return "Services";
  }

  @Override
  public String getName() {
    return "EXCHANGE_SMTP";
  }

  @Override
  public ICheckResult checkStatus() {
    CheckResultBuilder result = new CheckResultBuilder();
    result.from(this);
    boolean isConnected = false;
    try {
      // Teste les méthodes paramétrées.
      isConnected = isSMTPConnected();
      result.code(OK);
    } catch (Exception e) {
      LOGGER_TECH.error("Impossible de se connecter sur le serveur SMTP " + SmtpConfiguration.getInstance().getHost(), e);
      result.code(FATAL);
      result.resolutionSteps(e.toString());
    }

    result.description("SMTP Connexion : " + isConnected);
    return result.build();
  }

  /**
   * Checking if the SMTP server is alive.
   * 
   * @return true if alive
   * @throws MessagingException
   *           throws Exception if the connection is failed
   */
  private boolean isSMTPConnected() throws MessagingException {
    Session session = null;
    Properties properties = new Properties();
    properties.put("mail.smtp.port", smtpConfiguration.getPort());
    properties.put("mail.smtp.host", smtpConfiguration.getHost());
    properties.put("mail.smtp.auth", smtpConfiguration.getAuth());
    properties.put("mail.smtp.starttls.enable", smtpConfiguration.getStarttlsEnable());
    properties.put("mail.smtp.user", smtpConfiguration.getUser());

    if (StringUtils.isNotBlank(smtpConfiguration.getUser()) && StringUtils.isNotBlank(smtpConfiguration.getPassword())) {
      session = Session.getInstance(properties, new javax.mail.Authenticator() {
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
          return new PasswordAuthentication(smtpConfiguration.getUser(), smtpConfiguration.getPassword());
        }
      });
    } else {
      session = Session.getDefaultInstance(properties);
    }
    Transport transport = session.getTransport("smtp");
    int port = Integer.parseInt(smtpConfiguration.getPort());
    transport.connect(smtpConfiguration.getHost(), port, smtpConfiguration.getUser(), smtpConfiguration.getPassword());
    transport.close();
    return true;
  }

}
