package fr.ge.ct.email;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.exchange.utils.PDFBoxUtils;

/**
 * PDF file splitter.
 *
 * @author jpauchet
 */
public class PdfSplitter {

    /** Maximum chunk size. */
    private final Long maxChunkSize;

    /** The original PDF. */
    private PDDocument originalPdf;

    /** The chunks. */
    private List<PDDocument> chunks;

    /**
     * Constructor.
     *
     * @param maxChunkSize
     *            maximum chunk size
     */
    public PdfSplitter(final Long maxChunkSize) {
        this.maxChunkSize = maxChunkSize;
    }

    /**
     * Loads a PDF.
     *
     * @param originalPdf
     *            the PDF
     */
    public PdfSplitter load(final PDDocument originalPdf) {
        this.originalPdf = originalPdf;
        PDFBoxUtils.correctNullPages(this.originalPdf);
        return this;
    }

    /**
     * Loads a PDF.
     *
     * @param originalPdf
     *            the PDF
     */
    public PdfSplitter load(final InputStream originalPdf) {
        try {
            this.originalPdf = PDDocument.load(originalPdf);
            PDFBoxUtils.correctNullPages(this.originalPdf);
        } catch (final IOException e) {
            throw new TechnicalException("Error reading the attachment to send", e);
        }
        return this;
    }

    /**
     * Splits a PDF in smaller chunks.
     */
    public PdfSplitter split() {
        this.chunks = new ArrayList<>();
        if (this.findDocumentLength(this.originalPdf, false) <= this.maxChunkSize) {
            this.chunks.add(this.originalPdf);
            return this;
        }
        final Map<Integer, Long> pagesLength = this.findPagesLength();
        int chunkStart = 1;
        Long chunkLength = 0L;
        boolean oddAlone = false;
        for (int pageNum = 1; pageNum <= this.originalPdf.getNumberOfPages(); ++pageNum) {
            if (oddAlone) {
                oddAlone = false;
                this.addChunk(chunkStart, pageNum);
                chunkStart = pageNum + 1;
                chunkLength = 0L;
            } else {
                chunkLength += pagesLength.get(pageNum);
                if (pageNum == this.originalPdf.getNumberOfPages()) {
                    this.addChunk(chunkStart, pageNum);
                } else if (chunkLength + pagesLength.get(pageNum + 1) > this.maxChunkSize) {
                    // a chunk size must no go over the limit
                    if (pageNum % 2 == 1) {
                        if (pageNum - chunkStart > 0) {
                            // a chunk must end with an even page
                            --pageNum;
                        } else {
                            // a chunk must start with an odd page
                            oddAlone = true;
                        }
                    }
                    this.addChunk(chunkStart, pageNum);
                    chunkStart = pageNum + 1;
                    chunkLength = 0L;
                }
            }
        }
        return this;
    }

    /**
     * Finds each page length.
     *
     * @return the pages length
     */
    private Map<Integer, Long> findPagesLength() {
        final Map<Integer, Long> pagesLength = new HashMap<>();
        int pageNum = 1;
        try {
            final List<PDDocument> pages = new Splitter().split(this.originalPdf);
            for (final PDDocument page : pages) {
                pagesLength.put(pageNum++, this.findDocumentLength(page, true));
            }
        } catch (final IOException e) {
            throw new TechnicalException("Error splitting a document into several pages", e);
        }
        return pagesLength;
    }

    /**
     * Finds a document length.
     *
     * @param document
     *            the document
     * @return the length
     */
    private long findDocumentLength(final PDDocument document, final boolean close) {
        long length = 0;
        try {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            document.save(baos);
            length = baos.size();
            if (close) {
                document.close();
            }
        } catch (final IOException e) {
            throw new TechnicalException("Error writing a document to find its length", e);
        }
        return length;
    }

    /**
     * Extract a chunk from the original PDF.
     *
     * @param chunkStart
     *            the original PDF start index
     * @param chunkEnd
     *            the original PDF end index
     */
    private void addChunk(final int chunkStart, final int chunkEnd) {
        try {
            final Splitter splitter = new Splitter();
            splitter.setStartPage(chunkStart);
            splitter.setEndPage(chunkEnd);
            splitter.setSplitAtPage(Integer.MAX_VALUE);
            this.chunks.addAll(splitter.split(this.originalPdf));
        } catch (final IOException e) {
            throw new TechnicalException("Error building an attachment chunk", e);
        }
    }

    /**
     * Gets the chunks as documents.
     *
     * @return the chunks
     */
    public List<PDDocument> asDocuments() {
        return this.chunks;
    }

    /**
     * Gets the chunks as files.
     *
     * @return the chunks
     */
    public List<File> asFiles() {
        final List<File> files = new ArrayList<>();
        if (this.chunks == null) {
            return files;
        }
        try {
            for (final PDDocument chunk : this.chunks) {
                final File chunkFile = File.createTempFile("chunk-" + RandomStringUtils.randomAlphabetic(10), ".pdf");
                chunk.save(chunkFile);
                chunk.close();
                chunkFile.deleteOnExit();
                files.add(chunkFile);
            }
        } catch (final IOException e) {
            throw new TechnicalException("Error writing the file", e);
        }
        return files;
    }

}
