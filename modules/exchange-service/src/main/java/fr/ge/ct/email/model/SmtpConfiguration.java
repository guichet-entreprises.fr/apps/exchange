/**
 * 
 */
package fr.ge.ct.email.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Singleton for SMTP configuration.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class SmtpConfiguration implements Serializable {

  /** UID. */
  private static final long serialVersionUID = 6878234751159346514L;

  /** Le port de connexion au serveur SMTP. */
  private String port;

  /** L'adresse du serveur client. */
  private String host;

  /** Communication timeout in milliseconds (default is 30 000 ms). */
  private int timeout = 30000;

  /** Connection timeout in milliseconds (default is 10 000 ms). */
  private int connectionTimeout = 10000;

  /** Si auth=true alors utilise la commande "AUTH" pour s'authentifier. */
  private String auth;

  /** Si starttlsEnable=true alors autorise la commande STARTTLS pour passer à une connexion TLS. */
  private String starttlsEnable;

  /** Le user pour se connecter au serveur SMTP. */
  private String user;

  /** Le mot de passe pour se connecter au serveur SMTP. */
  private String password;

  /** List of allowed domain to send email from */
  List < String > allowedDomains = new ArrayList <>();

  /** The default no-reply address to use when there's no valid sender's email address */
  private String defaultNoreplyAddress;

  /** Instance statique du bean {@link #SmtpConfiguration}. */
  private static SmtpConfiguration instance = new SmtpConfiguration();

  /**
   * Récupère l'instance unique de {@link #SmtpConfiguration}.
   *
   * @return unique instance de {@link #SmtpConfiguration}
   */
  public static SmtpConfiguration getInstance() {
    return instance;
  }

  /**
   * Accesseur sur l'attribut {@link #port}.
   *
   * @return String port
   */
  public String getPort() {
    return this.port;
  }

  /**
   * Mutateur sur l'attribut {@link #port}.
   *
   * @param port
   *          la nouvelle valeur de l'attribut port
   */
  public void setPort(final String port) {
    this.port = port;
  }

  /**
   * Accesseur sur l'attribut {@link #host}.
   *
   * @return String host
   */
  public String getHost() {
    return this.host;
  }

  /**
   * Mutateur sur l'attribut {@link #host}.
   *
   * @param host
   *          la nouvelle valeur de l'attribut host
   */
  public void setHost(final String host) {
    this.host = host;
  }

  /**
   * Accesseur sur l'attribut {@link #timeout}.
   *
   * @return int timeout
   */
  public int getTimeout() {
    return timeout;
  }

  /**
   * Mutateur sur l'attribut {@link #timeout}.
   *
   * @param timeout
   *          la nouvelle valeur de l'attribut timeout
   */
  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }

  /**
   * Accesseur sur l'attribut {@link #connectionTimeout}.
   *
   * @return int connectionTimeout
   */
  public int getConnectionTimeout() {
    return connectionTimeout;
  }

  /**
   * Mutateur sur l'attribut {@link #connectionTimeout}.
   *
   * @param connectionTimeout
   *          la nouvelle valeur de l'attribut connectionTimeout
   */
  public void setConnectionTimeout(int connectionTimeout) {
    this.connectionTimeout = connectionTimeout;
  }

  /**
   * Accesseur sur l'attribut {@link #auth}.
   *
   * @return String auth
   */
  public String getAuth() {
    return this.auth;
  }

  /**
   * Mutateur sur l'attribut {@link #auth}.
   *
   * @param auth
   *          la nouvelle valeur de l'attribut auth
   */
  public void setAuth(final String auth) {
    this.auth = auth;
  }

  /**
   * Accesseur sur l'attribut {@link #starttlsEnable}.
   *
   * @return String starttlsEnable
   */
  public String getStarttlsEnable() {
    return this.starttlsEnable;
  }

  /**
   * Mutateur sur l'attribut {@link #starttlsEnable}.
   *
   * @param starttlsEnable
   *          la nouvelle valeur de l'attribut starttlsEnable
   */
  public void setStarttlsEnable(final String starttlsEnable) {
    this.starttlsEnable = starttlsEnable;
  }

  /**
   * Accesseur sur l'attribut {@link #user}.
   *
   * @return String user
   */
  public String getUser() {
    return this.user;
  }

  /**
   * Mutateur sur l'attribut {@link #user}.
   *
   * @param user
   *          la nouvelle valeur de l'attribut user
   */
  public void setUser(final String user) {
    this.user = user;
  }

  /**
   * Accesseur sur l'attribut {@link #password}.
   *
   * @return String password
   */
  public String getPassword() {
    return this.password;
  }

  /**
   * Mutateur sur l'attribut {@link #password}.
   *
   * @param password
   *          la nouvelle valeur de l'attribut password
   */
  public void setPassword(final String password) {
    this.password = password;
  }

  /**
   * Accesseur sur l'attribut {@link #allowedDomains}.
   *
   * @return List<String> allowedDomains
   */
  public List < String > getAllowedDomains() {
    return allowedDomains;
  }

  /**
   * Comma separated list of domain. Mutateur sur l'attribut {@link #allowedDomains}.
   *
   * @param allowedDomains
   *          la nouvelle valeur de l'attribut allowedDomains
   */
  public void setAllowedDomains(String allowedDomains) {
    if (allowedDomains != null) {
      // split the string by the comma char, trim the value, add it to the list
      List < String > domains = new ArrayList <>();
      Arrays.asList(StringUtils.split(allowedDomains, ',')).stream().map(s -> s.trim()).forEach(domains::add);
      this.allowedDomains = domains;
    }
  }

  /**
   * Accesseur sur l'attribut {@link #defaultNoreplyAddress}.
   *
   * @return String defaultNoreplyAddress
   */
  public String getDefaultNoreplyAddress() {
    return defaultNoreplyAddress;
  }

  /**
   * Mutateur sur l'attribut {@link #defaultNoreplyAddress}.
   *
   * @param defaultNoreplyAddress
   *          la nouvelle valeur de l'attribut defaultNoreplyAddress
   */
  public void setDefaultNoreplyAddress(String defaultNoreplyAddress) {
    this.defaultNoreplyAddress = defaultNoreplyAddress;
  }

}
