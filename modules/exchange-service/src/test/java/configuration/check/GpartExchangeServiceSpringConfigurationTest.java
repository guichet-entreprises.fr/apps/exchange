/**
 * 
 */
package configuration.check;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Classe permettant de tester le démarrage du contexte Spring.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/applicationContext-gpart-exchange-service-config.xml" })
public class GpartExchangeServiceSpringConfigurationTest {

  /**
   * Test.
   */
  @Test
  @Ignore("Dépendance avec autes projets fait que le TU ne marche pas")
  public void testSpringConfiguration() {
    // On teste juste la configuration Spring au chargement
  }

}
