/**
 * 
 */
package fr.ge.ct.email;

import static org.fest.assertions.Assertions.assertThat;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.ct.email.model.SmtpConfiguration;

/**
 * Check the email address filtering. <br/>
 * Use of introspection to access the method.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/gpart-exchange-service-properties-files-config-test.xml",
  "classpath:spring/gpart-exchange-service-email-config.xml" })
public class SmtpClientSenderCheckerTest {

  /** La classe utilitaire de test. */
  @Autowired
  private SmtpClient smtpClient;// = new SmtpClient();

  /** Email expéditeur externe aux domaines gérés. **/
  protected final String emailExpediteurExterne = "utilisateur@ailleurs.com";

  /** Email expéditeur interne aux domaines gérés. **/
  protected final String emailExpediteurInterne = "utilisateur@guichet-entreprises.fr";

  /**
   * Méthode exécutée en début de JUNIT.
   * 
   * @throws NoSuchFieldException
   * @throws SecurityException
   * @throws IllegalAccessException
   * @throws IllegalArgumentException
   */
  @Before
  public void setUp() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {

  }

  /**
   * Test envoyer email avec PJ.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testExpediteurEmail() throws Exception {

    Method filteredSenderMethod = smtpClient.getClass().getDeclaredMethod("filteredSenderAddress", String.class);
    filteredSenderMethod.setAccessible(true);

    String filteredEmail = (String) filteredSenderMethod.invoke(smtpClient, emailExpediteurExterne);
    assertThat(filteredEmail).isEqualTo(SmtpConfiguration.getInstance().getDefaultNoreplyAddress());
    filteredEmail = (String) filteredSenderMethod.invoke(smtpClient, emailExpediteurInterne);
    assertThat(filteredEmail).isEqualTo(emailExpediteurInterne);
    filteredEmail = (String) filteredSenderMethod.invoke(smtpClient, "ceci n'est pas un email");
    assertThat(filteredEmail).isEqualTo(SmtpConfiguration.getInstance().getDefaultNoreplyAddress());
  }
}
