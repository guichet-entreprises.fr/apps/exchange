/**
 * 
 */
package fr.ge.ct.email.model;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Testing default SmtpConfiguration configuration
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/gpart-exchange-service-properties-files-config-test.xml",
  "classpath:spring/gpart-exchange-service-email-config.xml" })
@DirtiesContext
public class SmtpConfigurationNoConfTest {

  @Autowired
  private SmtpConfiguration smtpConfiguration;

  /**
   * Check default configuration.
   */
  @Test
  public void checkDefaultConfiguration() {

    // SmtpConfiguration smtpConfiguration = SmtpConfiguration.getInstance();
    assertThat(smtpConfiguration.getDefaultNoreplyAddress()).isEqualTo("no-reply@guichet-entreprises.fr");
    assertThat(smtpConfiguration.getAllowedDomains().get(0)).isEqualTo("guichet-entreprises.fr");
    assertThat(smtpConfiguration.getAllowedDomains().get(1)).isEqualTo("projet-ge.fr");
  }
}
