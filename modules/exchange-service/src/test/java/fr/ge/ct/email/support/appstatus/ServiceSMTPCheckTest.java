package fr.ge.ct.email.support.appstatus;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.ct.email.model.SmtpConfiguration;
import fr.ge.ct.email.support.appstatus.ServiceSMTPCheck;
import net.sf.appstatus.core.check.ICheckResult;

/**
 * Tests {@link ServiceSMTPCheck}.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Ignore
public class ServiceSMTPCheckTest {

  /** SMTP availability check. */
  @InjectMocks
  private ServiceSMTPCheck serviceSMTPCheck;

  @Mock
  private SmtpConfiguration smtpConfiguration = Mockito.mock(SmtpConfiguration.class);

  /**
   * Mock injection.
   */
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Testing SMTP status.
   */
  @Test
  public void testCheckStatus() {
    Mockito.when(smtpConfiguration.getAuth()).thenReturn("true");
    Mockito.when(smtpConfiguration.getPort()).thenReturn("3025");
    Mockito.when(smtpConfiguration.getHost()).thenReturn("localhost");
    Mockito.when(smtpConfiguration.getStarttlsEnable()).thenReturn("true");
    Mockito.when(smtpConfiguration.getUser()).thenReturn("test");
    Mockito.when(smtpConfiguration.getPassword()).thenReturn("test");

    ICheckResult resultTrue = serviceSMTPCheck.checkStatus();
    assertThat(resultTrue.getCode()).isEqualTo(0);
  }

  /**
   * Testing SMTP status.
   */
  @Test
  public void testCheckStatusWithNoLoginPassword() {
    Mockito.when(smtpConfiguration.getAuth()).thenReturn("true");
    Mockito.when(smtpConfiguration.getPort()).thenReturn("3025");
    Mockito.when(smtpConfiguration.getHost()).thenReturn("localhost");
    Mockito.when(smtpConfiguration.getStarttlsEnable()).thenReturn("true");
    Mockito.when(smtpConfiguration.getUser()).thenReturn("");
    Mockito.when(smtpConfiguration.getPassword()).thenReturn("");

    ICheckResult resultTrue = serviceSMTPCheck.checkStatus();
    assertThat(resultTrue.getCode()).isEqualTo(0);
  }

  /**
   * Testing SMTP failed status.
   */
  @Test
  public void testCheckStatusFailed() {
    Mockito.when(smtpConfiguration.getAuth()).thenReturn("true");
    Mockito.when(smtpConfiguration.getPort()).thenReturn("30200");
    Mockito.when(smtpConfiguration.getHost()).thenReturn("localhost");
    Mockito.when(smtpConfiguration.getStarttlsEnable()).thenReturn("true");
    Mockito.when(smtpConfiguration.getUser()).thenReturn("test");
    Mockito.when(smtpConfiguration.getPassword()).thenReturn("test");

    ICheckResult resultTrue = serviceSMTPCheck.checkStatus();
    assertThat(resultTrue.getCode()).isEqualTo(2);
  }

  /**
   * Testing SMTP status wth no login.
   */
  @Test
  public void testCheckStatusWithNoLogin() {
    Mockito.when(smtpConfiguration.getAuth()).thenReturn("true");
    Mockito.when(smtpConfiguration.getPort()).thenReturn("3025");
    Mockito.when(smtpConfiguration.getHost()).thenReturn("localhost");
    Mockito.when(smtpConfiguration.getStarttlsEnable()).thenReturn("true");
    Mockito.when(smtpConfiguration.getPassword()).thenReturn("test");

    ICheckResult resultTrue = serviceSMTPCheck.checkStatus();
    assertThat(resultTrue.getCode()).isEqualTo(2);
  }

  /**
   * Testing SMTP status with no password.
   */
  @Test
  public void testCheckStatusWithNoPassword() {
    Mockito.when(smtpConfiguration.getAuth()).thenReturn("true");
    Mockito.when(smtpConfiguration.getPort()).thenReturn("3025");
    Mockito.when(smtpConfiguration.getHost()).thenReturn("localhost");
    Mockito.when(smtpConfiguration.getStarttlsEnable()).thenReturn("true");
    Mockito.when(smtpConfiguration.getUser()).thenReturn("test");

    ICheckResult resultTrue = serviceSMTPCheck.checkStatus();
    assertThat(resultTrue.getCode()).isEqualTo(2);
  }

  /**
   * Testing SMTP status with empty password.
   */
  @Test
  public void testCheckStatusWithEmptyPassword() {
    Mockito.when(smtpConfiguration.getAuth()).thenReturn("true");
    Mockito.when(smtpConfiguration.getPort()).thenReturn("30200");
    Mockito.when(smtpConfiguration.getHost()).thenReturn("localhost");
    Mockito.when(smtpConfiguration.getStarttlsEnable()).thenReturn("true");
    Mockito.when(smtpConfiguration.getUser()).thenReturn("test");
    Mockito.when(smtpConfiguration.getPassword()).thenReturn("");

    ICheckResult resultTrue = serviceSMTPCheck.checkStatus();
    assertThat(resultTrue.getCode()).isEqualTo(2);
  }

  /**
   * Testing SMTP status with empty login.
   */
  @Test
  public void testCheckStatusWithEmptyLogin() {
    Mockito.when(smtpConfiguration.getAuth()).thenReturn("true");
    Mockito.when(smtpConfiguration.getPort()).thenReturn("30200");
    Mockito.when(smtpConfiguration.getHost()).thenReturn("localhost");
    Mockito.when(smtpConfiguration.getStarttlsEnable()).thenReturn("true");
    Mockito.when(smtpConfiguration.getUser()).thenReturn("");
    Mockito.when(smtpConfiguration.getPassword()).thenReturn("test");

    ICheckResult resultTrue = serviceSMTPCheck.checkStatus();
    assertThat(resultTrue.getCode()).isEqualTo(2);
  }

}
