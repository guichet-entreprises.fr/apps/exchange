/**
 * 
 */
package fr.ge.ct.email.model;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Classe JUNIT de test de la classe EmailUtil.
 * 
 * @author aolubi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/gpart-exchange-service-properties-files-config-test.xml",
  "classpath:spring/gpart-exchange-service-email-config.xml" })
@DirtiesContext
public class SmtpConfigurationTest {

  /**
   * Méthode exécutée en début de JUNIT.
   */
  @Before
  public void setUp() {

  }

  /**
   * Test SMTP Configuration
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testConfigurationDomains() throws Exception {

    SmtpConfiguration smtpConfiguration = SmtpConfiguration.getInstance();
    smtpConfiguration.setAllowedDomains("domain1.fr, domain2.com,domain3.co.uk");
    assertThat(smtpConfiguration.getAllowedDomains().get(0)).isEqualTo("domain1.fr");
    assertThat(smtpConfiguration.getAllowedDomains().get(1)).isEqualTo("domain2.com");
    assertThat(smtpConfiguration.getAllowedDomains().get(2)).isEqualTo("domain3.co.uk");
  }

}
