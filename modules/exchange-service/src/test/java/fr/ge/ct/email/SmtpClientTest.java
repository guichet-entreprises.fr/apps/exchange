/**
 *
 */
package fr.ge.ct.email;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.MimeMultipart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.thymeleaf.TemplateEngine;

import fr.ge.AbstractTest;
import fr.ge.core.exception.TechniqueException;
import fr.ge.ct.email.model.SmtpConfiguration;
import fr.ge.ct.email.model.TemplateEmailEnum;

/**
 * Classe JUNIT de test de la classe EmailUtil.
 *
 * @author aolubi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/gpart-exchange-service-properties-files-config-test.xml", "classpath:spring/gpart-exchange-service-email-config.xml" })
public class SmtpClientTest extends AbstractTest {

    /** La classe utilitaire de test. */
    @Autowired
    private SmtpClient smtpClient;

    /** template engine mail. */
    @Autowired
    private TemplateEngine templateEngineMail;

    /** Email de destination. **/
    protected final String emailDeclarant = "declarant@test.com";

    /** Email expéditeur externe aux domaines gérés. **/
    protected final String emailExpediteurExterne = "utilisateur@ailleurs.com";

    /** Email expéditeur interne aux domaines gérés. **/
    protected final String emailExpediteurInterne = "utilisateur@guichet-entreprises.fr";

    /** Send messages */
    protected final List<Message> messagesSend = new ArrayList<>();

    /**
     * Méthode exécutée en début de JUNIT.
     *
     * @throws NoSuchFieldException
     * @throws SecurityException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    @Before
    public void setUp() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        // new client, new messages
        this.messagesSend.clear();

        // smtpClient = new SmtpClient();
        // smtpClient.setTemplateEngineMail(templateEngineMail);
        final Field messageSenderField = this.smtpClient.getClass().getDeclaredField("messageSender");
        messageSenderField.setAccessible(true);

        // define new Sender to mock
        final Consumer<Message> mockSender = message -> {
            this.messagesSend.add(message);
        };
        // modify the sender mecanism
        messageSenderField.set(this.smtpClient, mockSender);
    }

    /**
     * Test envoyer email avec PJ.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testExpediteurEmail() throws Exception {

        final List<String> listeEmailDestinataires = new ArrayList<>();
        listeEmailDestinataires.add(this.emailDeclarant);

        final String object = "ObjetTest";
        final String body = "<p><u>TEST html</u></p>";
        final String pdfName = "Demande_dossier.pdf";
        final File file = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestOK.pdf");
        final InputStream pdfInputStream = new FileInputStream(file);
        final Map<String, String> mapData = new HashMap<>();
        mapData.put("body", body);

        // expediteur externe
        this.smtpClient.envoyerEmail(this.emailExpediteurExterne, listeEmailDestinataires, object, body, pdfName, pdfInputStream, TemplateEmailEnum.DEFAULT_TEMPLATE.getNomFichierTemplate(), mapData,
                true);

        assertThat(this.messagesSend.size()).isEqualTo(1);
        final Message messageByExternal = this.messagesSend.get(this.messagesSend.size() - 1);
        assertThat(messageByExternal.getFrom().length).isEqualTo(1);
        assertThat(messageByExternal.getFrom()[0].toString()).isEqualTo(SmtpConfiguration.getInstance().getDefaultNoreplyAddress());

        // expediteur interne
        this.smtpClient.envoyerEmail(this.emailExpediteurInterne, listeEmailDestinataires, object, body, pdfName, new FileInputStream(file), TemplateEmailEnum.DEFAULT_TEMPLATE.getNomFichierTemplate(),
                mapData, true);

        assertThat(this.messagesSend.size()).isEqualTo(2);
        final Message messageByInternal = this.messagesSend.get(this.messagesSend.size() - 1);
        assertThat(messageByInternal.getFrom().length).isEqualTo(1);
        assertThat(messageByInternal.getFrom()[0].toString()).isEqualTo(this.emailExpediteurInterne);

    }

    /**
     * Test envoyer email avec PJ.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testNomPieceJointe() throws Exception {

        final List<String> listeEmailDestinataires = new ArrayList<>();
        listeEmailDestinataires.add(this.emailDeclarant);

        final String object = "ObjetTest";
        final String body = "<p><u>TEST ééééééL</u></p>";
        final String pdfName = "Demande de dossier.pdf";
        final File file = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestOK.pdf");
        final InputStream templateStream = new FileInputStream(file);
        final Map<String, String> mapData = new HashMap<>();
        mapData.put("body", body);

        this.smtpClient.envoyerEmail(this.emailExpediteurExterne, listeEmailDestinataires, object, body, pdfName, templateStream, TemplateEmailEnum.DEFAULT_TEMPLATE.getNomFichierTemplate(), mapData,
                true);

        assertThat(this.messagesSend.size()).isEqualTo(1);
        final Message message = this.messagesSend.get(0);
        assertThat(message.getContentType()).isEqualTo("text/plain");
        assertThat(message.getDataHandler().getContent()).isNotNull();
        assertThat(message.getDataHandler().getContent()).isInstanceOf(Multipart.class);

        // XXX LAB : finir ici

    }

    /**
     * Test envoyer email avec PJ.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEnvoyerEmailAvecPJ() throws Exception {
        final List<String> listeEmailDestinataires = Arrays.asList(this.emailDeclarant);
        final String object = "ObjetTest";
        final String body = "<p><u>TEST ééééééL</u></p>";
        final String pdfName = "test.pdf";
        final File file = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestOK.pdf");
        final InputStream templateStream = new FileInputStream(file);
        final Map<String, String> mapData = new HashMap<>();
        mapData.put("body", body);

        this.smtpClient.envoyerEmail(this.emailExpediteurExterne, listeEmailDestinataires, object, body, pdfName, templateStream, TemplateEmailEnum.DEFAULT_TEMPLATE.getNomFichierTemplate(), mapData,
                true);

        assertThat(this.messagesSend.size()).isEqualTo(1);

        final Message message0 = this.messagesSend.get(0);
        assertThat(message0.getSubject()).isEqualTo(object);
        final MimeMultipart message0Content = (MimeMultipart) message0.getContent();
        assertThat(message0Content.getBodyPart(0).getFileName()).isEqualTo(null);
        assertThat(message0Content.getBodyPart(0).getContent()).isNotNull();
        assertThat(message0Content.getBodyPart(1).getFileName()).isEqualTo(pdfName);

        try {
            this.smtpClient.envoyerEmail(this.emailExpediteurExterne, listeEmailDestinataires, object, body, pdfName, null, null, null, true);
            fail("Le contrôle doit jeter une exception");
        } catch (final TechniqueException e) {
            assertThat(e).isInstanceOf(TechniqueException.class);
        }

    }

    /**
     * Test envoyer email avec PJ découpée.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEnvoyerEmailAvecPJDecoupee() throws Exception {
        final List<String> listeEmailDestinataires = Arrays.asList(this.emailDeclarant);
        final String object = "ObjetTest";
        final String body = "<p><u>TEST ééééééL</u></p>";
        final String pdfName = "test.pdf";
        final InputStream attachment = new ByteArrayInputStream(this.resourceAsBytes("10,2Mo.pdf"));
        final Map<String, String> mapData = new HashMap<>();
        mapData.put("body", body);
        this.smtpClient.envoyerEmail(this.emailExpediteurExterne, listeEmailDestinataires, object, body, pdfName, attachment, TemplateEmailEnum.DEFAULT_TEMPLATE.getNomFichierTemplate(), mapData,
                true);

        assertThat(this.messagesSend.size()).isEqualTo(5);

        final Message message0 = this.messagesSend.get(0);
        assertThat(message0.getSubject()).isEqualTo("ObjetTest (1/5)");
        final MimeMultipart message0Content = (MimeMultipart) message0.getContent();
        assertThat(message0Content.getBodyPart(0).getFileName()).isEqualTo(null);
        assertThat(message0Content.getBodyPart(0).getContent()).isNotNull();
        assertThat(message0Content.getBodyPart(1).getFileName()).isEqualTo("test-1.pdf");

        final Message message1 = this.messagesSend.get(1);
        assertThat(message1.getSubject()).isEqualTo("ObjetTest (2/5)");
        final MimeMultipart message1Content = (MimeMultipart) message1.getContent();
        assertThat(message1Content.getBodyPart(0).getFileName()).isEqualTo(null);
        assertThat(message1Content.getBodyPart(0).getContent()).isEqualTo("Veuillez trouver ci-joint la partie n°2 sur 5 de la pièce jointe.");
        assertThat(message1Content.getBodyPart(1).getFileName()).isEqualTo("test-2.pdf");

        final Message message2 = this.messagesSend.get(2);
        assertThat(message2.getSubject()).isEqualTo("ObjetTest (3/5)");
        final MimeMultipart message2Content = (MimeMultipart) message2.getContent();
        assertThat(message2Content.getBodyPart(0).getFileName()).isEqualTo(null);
        assertThat(message2Content.getBodyPart(0).getContent()).isEqualTo("Veuillez trouver ci-joint la partie n°3 sur 5 de la pièce jointe.");
        assertThat(message2Content.getBodyPart(1).getFileName()).isEqualTo("test-3.pdf");

        final Message message3 = this.messagesSend.get(3);
        assertThat(message3.getSubject()).isEqualTo("ObjetTest (4/5)");
        final MimeMultipart message3Content = (MimeMultipart) message3.getContent();
        assertThat(message3Content.getBodyPart(0).getFileName()).isEqualTo(null);
        assertThat(message3Content.getBodyPart(0).getContent()).isEqualTo("Veuillez trouver ci-joint la partie n°4 sur 5 de la pièce jointe.");
        assertThat(message3Content.getBodyPart(1).getFileName()).isEqualTo("test-4.pdf");

        final Message message4 = this.messagesSend.get(4);
        assertThat(message4.getSubject()).isEqualTo("ObjetTest (5/5)");
        final MimeMultipart message4Content = (MimeMultipart) message4.getContent();
        assertThat(message4Content.getBodyPart(0).getFileName()).isEqualTo(null);
        assertThat(message4Content.getBodyPart(0).getContent()).isEqualTo("Veuillez trouver ci-joint la partie n°5 sur 5 de la pièce jointe.");
        assertThat(message4Content.getBodyPart(1).getFileName()).isEqualTo("test-5.pdf");
    }

    /**
     * Test envoyer email sans PJ.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEnvoyerEmailSansPJ() throws Exception {

        final List<String> listeEmailDestinataires = new ArrayList<>();
        listeEmailDestinataires.add(this.emailDeclarant);

        final String object = "ObjetTest";
        final String body = "<p><u>TEST ééééééL</u></p>";
        final String pdfName = "test.pdf";
        final Map<String, String> mapData = new HashMap<>();
        mapData.put("body", body);

        this.smtpClient.envoyerEmail(this.emailExpediteurExterne, listeEmailDestinataires, object, body, pdfName, null, TemplateEmailEnum.DEFAULT_TEMPLATE.getNomFichierTemplate(), mapData, false);

        try {
            this.smtpClient.envoyerEmail(this.emailExpediteurExterne, listeEmailDestinataires, object, body, pdfName, null, null, null, false);
        } catch (final TechniqueException e) {
            assertThat(e).isInstanceOf(TechniqueException.class);
        }
    }

}
