/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.ct.email;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.ge.AbstractTest;

/**
 * Tests {@link PdfSplitter}.
 *
 * @author jpauchet
 */
@RunWith(BlockJUnit4ClassRunner.class)
@WebAppConfiguration
public class PdfSplitterTest extends AbstractTest {

    /**
     * Tests {@link PdfSplitter#split()}.
     *
     * @throws Exception
     */
    @Test
    public void testSplit() throws Exception {
        final PDDocument pdf = PDDocument.load(new ByteArrayInputStream(this.resourceAsBytes("10,2Mo.pdf")));
        final PdfSplitter pdfSplitter = new PdfSplitter(5000000L).load(pdf).split();
        final List<PDDocument> documents = pdfSplitter.asDocuments();
        final List<File> files = pdfSplitter.asFiles();
        for (int i = 0; i < documents.size(); ++i) {
            final PDDocument document = documents.get(i);
            final File file = files.get(i);
            System.out.println("Chunk " + (i + 1) + " : " + document.getNumberOfPages() + " page(s), " //
                    + FileUtils.byteCountToDisplaySize(file.length()) + " (" + file.length() + ")");
        }
        // 1) Pages 1-7 are < 5 Mo but the last page must be even so we keep 1-6
        // 2) Pages 7-8 are > 5 Mo so we keep 7 alone
        // 3) Chunks must start with an odd page so page 8 must be alone
        assertThat(documents.size(), equalTo(5));
        assertThat(documents.get(0).getNumberOfPages(), equalTo(6));
        assertThat(documents.get(1).getNumberOfPages(), equalTo(1));
        assertThat(documents.get(2).getNumberOfPages(), equalTo(1));
        assertThat(documents.get(3).getNumberOfPages(), equalTo(18));
        assertThat(documents.get(4).getNumberOfPages(), equalTo(14));
    }

}
