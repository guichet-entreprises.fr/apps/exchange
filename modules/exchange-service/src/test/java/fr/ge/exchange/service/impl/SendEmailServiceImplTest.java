package fr.ge.exchange.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.core.exception.TechniqueException;
import fr.ge.ct.email.SmtpClient;
import fr.ge.ct.email.model.TemplateEmailEnum;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;

/**
 * Services test for {@link SendEmailServiceImpl}.
 */
public class SendEmailServiceImplTest {

  /** send email service. */
  @InjectMocks
  private SendEmailServiceImpl sendEmailService;

  /** mock email utils. */
  @Mock
  private SmtpClient smtpClient;

  /**
   * Inject mocks.
   */
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test send email.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSendEmail() throws Exception {
    EmailSendBean emailInfo = new EmailSendBean();
    emailInfo.setContent("Email content");
    emailInfo.setRecipient("recipient1@test.com,recipient2@test.com,recipient3@test.com");
    emailInfo.setObject("Object");
    emailInfo.setAttachmentPDFName("test.pdf");

    // attachment non renseignée
    try {
      sendEmailService.sendEmail(emailInfo, null);
      fail("TechniqueException attendue");
    } catch (TechniqueException e) {
      assertThat(e.getCode()).isEqualTo("ERR_EXCH_INVALID_PARAMETER");
    }

    // email expéditaire non renseigné
    File file = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestOK.pdf");
    InputStream templateStream = new FileInputStream(file);
    try {
      sendEmailService.sendEmail(emailInfo, templateStream);
      fail("TechniqueException attendue");
    } catch (TechniqueException e) {
      assertThat(e.getCode()).isEqualTo("ERR_EXCH_INVALID_PARAMETER");
    }
  }

  /**
   * Test send email.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSendEmail2() throws Exception {
    EmailSendBean emailInfo = new EmailSendBean();
    emailInfo.setContent("Email content");
    emailInfo.setRecipient("recipient1@test.com,recipient2@test.com,recipient3@test.com");
    emailInfo.setObject("Object");
    emailInfo.setAttachmentPDFName("test.pdf");
    emailInfo.setSender("projet-ge@test.com");
    File file = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestOK.pdf");
    InputStream templateStream = new FileInputStream(file);
    sendEmailService.sendEmail(emailInfo, templateStream);

    String template = TemplateEmailEnum.DEFAULT_TEMPLATE.getNomFichierTemplate();
    Map < String, String > mapData = new HashMap < String, String >();
    mapData.put("body", "Email content");
    Mockito.verify(smtpClient).envoyerEmail(Mockito.eq("projet-ge@test.com"),
      Mockito.eq(Arrays.asList("recipient1@test.com", "recipient2@test.com", "recipient3@test.com")), Mockito.eq("Object"),
      Mockito.eq(null), Mockito.eq("test.pdf"), Mockito.eq(templateStream), Mockito.eq(template), Mockito.eq(mapData),
      Mockito.eq(true));
  }

  /**
   * Test send email without any attachment.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testSendEmailWithoutAnyAttachment() throws Exception {
    EmailSendBean emailInfo = new EmailSendBean();
    emailInfo.setContent("Email content");
    emailInfo.setRecipient("recipient1@test.com,recipient2@test.com,recipient3@test.com");
    emailInfo.setObject("Object");

    // attachment non renseignée
    try {
      sendEmailService.sendEmail(emailInfo);
      fail("TechniqueException attendue");
    } catch (TechniqueException e) {
      assertThat(e.getCode()).isEqualTo("ERR_EXCH_INVALID_PARAMETER");
    }

    // email expéditaire non renseigné
    try {
      sendEmailService.sendEmail(emailInfo);
      fail("TechniqueException attendue");
    } catch (TechniqueException e) {
      assertThat(e.getCode()).isEqualTo("ERR_EXCH_INVALID_PARAMETER");
    }

    emailInfo = new EmailSendBean();
    emailInfo.setContent("Email content");
    emailInfo.setRecipient("recipient1@test.com,recipient2@test.com,recipient3@test.com");
    emailInfo.setObject("Object");
    emailInfo.setSender("projet-ge@test.com");
    sendEmailService.sendEmail(emailInfo);
  }
}
