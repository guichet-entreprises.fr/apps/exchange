package fr.ge.exchange.postmail;

import static org.fest.assertions.Assertions.assertThat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.Test;
import org.mockito.Mock;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.PostmailBean;
import fr.ge.exchange.utils.PdfArchiveGenerator;

/**
 * Test generation of PDFs.
 */
public class PostmailGeneratorVariationsTest {

    @Mock
    PostmailGenerator postMailGenerator = new PostmailGenerator("/pdf/addressFilter_1field.pdf");

    /**
     * Test génération avec plusieurs filtre d'adresse.<br/>
     * Le contenu de la barrette technique est de la forme :
     * AAAA-MM-xxx-xxx-NN_[RV][NC][RALE].
     * 
     * @throws Exception
     *             exception
     */
    @Test
    public void testAddressFilter() throws Exception {

        // SCN Guichet Entreprises
        // Ministère de l'économie et des finances
        // Bâtiment NECKER - Télédoc 766
        // 120, rue de Bercy
        // 75572 PARIS Cedex 12

        // 2017-09-LAB-XYV-vv_BBB : X mesures : [B blanc, M mesures], Y fond :
        // [B blanc, F fond],
        // vv version : [01, 02]

        String[] trackIds = new String[] { "2017-10-LAB-IMP-01_VCA", "2017-10-LAB-IMP-02_VNL", "2017-09-LAB-BBV-01_VCA", "2017-09-LAB-MFV-01_VCA", "2017-09-LAB-MBV-01_VNL", "2017-09-LAB-MFV-02_VNL",
                "2017-09-LAB-BBV-02_VNL" };
        String[] filterFiles = { "/pdf/address_page_v1.0_test.pdf", "/pdf/address_page_v2.0_test.pdf", "/pdf/addressFilter_1field.pdf", "/pdf/addressFilter_v1_mesures_fondbleu.pdf",
                "/pdf/addressFilter_v1_mesures.pdf", "/pdf/addressFilter_v2_mesures_fondbleu.pdf", "/pdf/addressFilter_v2.pdf" };
        assertThat(trackIds.length).isEqualTo(filterFiles.length);
        for (int i = 0; i < trackIds.length; i++) {
            String trackId = trackIds[i];
            File file = new File(this.getClass().getResource("/").getFile() + "pdf/exemple_dossier_IN.pdf");
            InputStream documentInputStream = new FileInputStream(file);
            PostmailBean postMailInfo = new PostmailBean();
            postMailInfo.setTrackId(trackId);
            // postMailInfo.setRecipientName("SCN Guichet Entreprises" + "\n" +
            // "Ministère de l'économie
            // et des finances");
            // postMailInfo.setRecipientAddress("Bâtiment NECKER - Télédoc 766"
            // + "\n" + "120, rue de
            // Bercy");
            // postMailInfo.setRecipientPostalCode("75196");
            // postMailInfo.setRecipientCity("Paris Cedex 04");
            postMailInfo.setRecipientName("IMPRIMERIE NATIONALE\nSEBASTIEN AQUILO\n");
            postMailInfo.setRecipientAddress("104 AVENUE DU PRESIDENT KENNEDY");
            postMailInfo.setRecipientPostalCode("75016");
            postMailInfo.setRecipientCity("PARIS");
            String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";
            String zipFilePath = new PostmailGenerator(filterFiles[i]).generatePostmailFromFile(documentInputStream, storageDirectoryPdf, postMailInfo.getTrackerId(), postMailInfo.getRecipientName(),
                    postMailInfo.getRecipientAddress(), postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());

            // Check if the zipFilePath is correct
            String expectedZipStorage = this.getClass().getResource("/").getFile() + "pdf" + File.separator + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "_"
                    + trackId + ".zip";
            assertThat(zipFilePath).isEqualTo(expectedZipStorage);

            // Récupération du pdf dans le ZIP généré, et on vérifie que
            // l'adresse du filtre a bien été
            // ajouté
            ZipFile zipFile = new ZipFile(expectedZipStorage);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            ZipEntry entry = entries.nextElement();
            InputStream pdfStream = zipFile.getInputStream(entry);
            PDDocument expectedPdf = PDDocument.load(pdfStream);
            assertThat(expectedPdf).isNotNull();
            String pdfText = extractPdfText(expectedPdf);
            assertThat(pdfText).contains(trackId);
            assertThat(pdfText).contains("IMPRIMERIE NATIONAL");

            // Close inputs
            zipFile.close();
            expectedPdf.close();
            documentInputStream.close();
        }
    }

    /**
     * Test génération avec plusieurs filtre d'adresse.<br/>
     * Le contenu de la barrette technique est de la forme :
     * AAAA-MM-xxx-xxx-NN_[RV][NC][RALE].
     * 
     * @throws Exception
     *             exception
     */
    @Test
    public void testImpressionTest() throws Exception {

        // SCN Guichet Entreprises
        // Ministère de l'économie et des finances
        // Bâtiment NECKER - Télédoc 766
        // 120, rue de Bercy
        // 75572 PARIS Cedex 12

        String[] trackIds = new String[] { "2017-11-LAB-IMP-1X_VCR", "2017-11-LAB-IMP-10_VCA", "2017-11-LAB-IMP-20_VCR", "2017-11-LAB-IMP-21_VCA", "2017-11-LAB-IMP-2X_VCR" };
        String[] filterFiles = { "/pdf/addressFilter_1field.pdf", "/pdf/address_page_v1.0_test.pdf", "/pdf/address_page_v2.0_test.pdf", "/pdf/address_page_v2.1_test.pdf",
                "/pdf/address_page_v2.1_final.pdf" };
        String[] inputFiles = { "/pdf/exemple_dossier_IN.pdf" };
        assertThat(trackIds.length).isEqualTo(filterFiles.length);
        for (int i = 0; i < trackIds.length; i++) {
            String trackId = trackIds[i];
            File file = new File(this.getClass().getResource(inputFiles[0]).getFile());
            InputStream documentInputStream = new FileInputStream(file);
            PostmailBean postMailInfo = new PostmailBean();
            postMailInfo.setTrackId(trackId);
            postMailInfo.setRecipientName("SCN Guichet Entreprises" + "\n" + "Ministère de l'économie et des finances");
            postMailInfo.setRecipientAddress("Bâtiment NECKER - Télédoc 766" + "\n" + "120, rue de Bercy");
            postMailInfo.setRecipientPostalCode("75572");
            postMailInfo.setRecipientCity("Paris Cedex 12");
            postMailInfo.setRecipientCity("PARIS");
            String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";
            String zipFilePath = new PostmailGenerator(filterFiles[i]).generatePostmailFromFile(documentInputStream, storageDirectoryPdf, postMailInfo.getTrackerId(), postMailInfo.getRecipientName(),
                    postMailInfo.getRecipientAddress(), postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());

            // Check if the zipFilePath is correct
            String expectedZipStorage = this.getClass().getResource("/").getFile() + "pdf" + File.separator + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "_"
                    + trackId + ".zip";
            assertThat(zipFilePath).isEqualTo(expectedZipStorage);

            // Récupération du pdf dans le ZIP généré, et on vérifie que
            // l'adresse du filtre a bien été
            // ajouté
            ZipFile zipFile = new ZipFile(expectedZipStorage);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            ZipEntry entry = entries.nextElement();
            InputStream pdfStream = zipFile.getInputStream(entry);
            PDDocument expectedPdf = PDDocument.load(pdfStream);
            assertThat(expectedPdf).isNotNull();
            String pdfText = extractPdfText(expectedPdf);
            assertThat(pdfText).contains(trackId);

            // Close inputs
            zipFile.close();
            expectedPdf.close();
            documentInputStream.close();
        }
    }

    /**
     * Test génération avec plusieurs filtre d'adresse.<br/>
     * Le contenu de la barrette technique est de la forme :
     * AAAA-MM-xxx-xxx-NN_[RV][NC][RALE].
     * 
     * @throws Exception
     *             exception
     */
    @Test
    public void testImpressionPolices() throws Exception {

        // SCN Guichet Entreprises
        // Ministère de l'économie et des finances
        // Bâtiment NECKER - Télédoc 766
        // 120, rue de Bercy
        // 75572 PARIS Cedex 12

        String[] trackIds = new String[] { "2017-11-LAB-IXX-01_VCA", "2017-11-LAB-IXX-02_VCR" };
        String[] filterFiles = { "/pdf/addressFilter_1field.pdf", "/pdf/address_page_v2.1_final.pdf" };
        String[] inputFiles = { "/pdf/impression_tests_file_v1.1.pdf", "/pdf/impression_tests_file_v1.1.pdf" };
        assertThat(trackIds.length).isEqualTo(filterFiles.length);
        for (int i = 0; i < trackIds.length; i++) {
            String trackId = trackIds[i];
            File file = new File(this.getClass().getResource(inputFiles[i]).getFile());
            InputStream documentInputStream = new FileInputStream(file);
            PostmailBean postMailInfo = new PostmailBean();
            postMailInfo.setTrackId(trackId);
            postMailInfo.setRecipientName("SCN Guichet Entreprises" + "\n" + "Ministère de l'économie et des finances");
            postMailInfo.setRecipientAddress("Bâtiment NECKER - Télédoc 766" + "\n" + "120, rue de Bercy");
            postMailInfo.setRecipientPostalCode("75572");
            postMailInfo.setRecipientCity("Paris Cedex 12");
            postMailInfo.setRecipientCity("PARIS");
            String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";
            String zipFilePath = new PostmailGenerator(filterFiles[i]).generatePostmailFromFile(documentInputStream, storageDirectoryPdf, postMailInfo.getTrackerId(), postMailInfo.getRecipientName(),
                    postMailInfo.getRecipientAddress(), postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());

            // Check if the zipFilePath is correct
            String expectedZipStorage = this.getClass().getResource("/").getFile() + "pdf" + File.separator + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "_"
                    + trackId + ".zip";
            assertThat(zipFilePath).isEqualTo(expectedZipStorage);

            // Récupération du pdf dans le ZIP généré, et on vérifie que
            // l'adresse du filtre a bien été
            // ajouté
            ZipFile zipFile = new ZipFile(expectedZipStorage);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            ZipEntry entry = entries.nextElement();
            InputStream pdfStream = zipFile.getInputStream(entry);
            PDDocument expectedPdf = PDDocument.load(pdfStream);
            assertThat(expectedPdf).isNotNull();
            String pdfText = extractPdfText(expectedPdf);
            assertThat(pdfText).contains(trackId);

            // Close inputs
            zipFile.close();
            expectedPdf.close();
            documentInputStream.close();
        }
    }

    /**
     * Test generate PDF from a fusion of a template with failure because File
     * has been closed.
     * 
     * @throws TechniqueException
     * @throws IOException
     * 
     */
    @Test(expected = TechniqueException.class)
    public void testFailedBecauseFileClosed() throws TechniqueException, IOException {
        PostmailBean postMailInfo = new PostmailBean();
        postMailInfo.setTrackId("VHB-LKD-55");
        postMailInfo.setRecipientName("Hôtel de Ville de Paris");
        postMailInfo.setRecipientAddress("Place de l'Hôtel de Ville");
        postMailInfo.setRecipientPostalCode("75572");
        postMailInfo.setRecipientCity("Paris Cedex 12");
        String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";

        File fileOK = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestOK.pdf");
        InputStream documentInputStream = new FileInputStream(fileOK);
        documentInputStream.close();

        postMailGenerator.setAddressFilterFile("/pdf/addressFilter_1field.pdf");
        postMailGenerator.setDoGenerateArchivePDF(false);

        postMailGenerator.generatePostmailFromFile(documentInputStream, storageDirectoryPdf, postMailInfo.getTrackerId(), postMailInfo.getRecipientName(), postMailInfo.getRecipientAddress(),
                postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());
    }

    /**
     * Test generate PDF from a fusion of a template with failure
     * storageDirectoryPdf does not exist
     * 
     * @throws TechniqueException
     * @throws IOException
     * 
     */
    @Test(expected = TechniqueException.class)
    public void testFailedBecauseDirectoryDoesNotExist() throws TechniqueException, IOException {
        PostmailBean postMailInfo = new PostmailBean();
        postMailInfo.setTrackId("VHB-LKD-55");
        postMailInfo.setRecipientName("Hôtel de Ville de Paris");
        postMailInfo.setRecipientAddress("Place de l'Hôtel de Ville");
        postMailInfo.setRecipientPostalCode("75572");
        postMailInfo.setRecipientCity("Paris Cedex 12");
        String storageDirectoryPdf = "this_directory_does_not_exist";

        File fileKO = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestKO.pdf");
        InputStream documentInputStream = new FileInputStream(fileKO);

        postMailGenerator.generatePostmailFromFile(documentInputStream, storageDirectoryPdf, postMailInfo.getTrackerId(), postMailInfo.getRecipientName(), postMailInfo.getRecipientAddress(),
                postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());
    }

    /**
     * When the address zone is not free, two new pages are added, if the flag
     * is set.
     */
    @Test
    public void testAddingPageBecauseAddressZoneNotEmpty() throws Exception {

        PostmailBean postMailInfo = new PostmailBean();
        postMailInfo.setTrackId("VHB-LKD-55");
        postMailInfo.setRecipientName("Hôtel de Ville de Paris");
        postMailInfo.setRecipientAddress("Place de l'Hôtel de Ville");
        postMailInfo.setRecipientPostalCode("75572");
        postMailInfo.setRecipientCity("Paris Cedex 12");
        String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";

        // Content of the PDF is invalid because reserved zones are not empty =>
        // error invalid PDF
        File fileKO = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestKO.pdf");
        InputStream documentInputStream = new FileInputStream(fileKO);
        String zipFilePath = new PostmailGenerator("/pdf/addressFilter_1field.pdf").generatePostmailFromFile(documentInputStream, storageDirectoryPdf, postMailInfo.getTrackerId(),
                postMailInfo.getRecipientName(), postMailInfo.getRecipientAddress(), postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());

        // Check if the zipFilePath is correct
        String expectedZipStorage = this.getClass().getResource("/").getFile() + "pdf" + File.separator + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "_"
                + postMailInfo.getTrackerId() + ".zip";
        assertThat(zipFilePath).isEqualTo(expectedZipStorage);

        // Récupération du pdf dans le ZIP généré, et on vérifie que l'adresse
        // du filtre a bien été ajouté
        ZipFile zipFile = new ZipFile(expectedZipStorage);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        ZipEntry entry = entries.nextElement();
        InputStream pdfStream = zipFile.getInputStream(entry);
        PDDocument expectedPdf = PDDocument.load(pdfStream);

        assertThat(expectedPdf).isNotNull();
        String pdfText = extractPdfText(expectedPdf);
        assertThat(pdfText).contains(postMailInfo.getTrackerId());
        assertThat(pdfText).contains(postMailInfo.getRecipientName());
        // check that the number of pages is pair: if address zone is not free,
        // two new pages were added for preparing rectoVerso printing
        assertThat(expectedPdf.getNumberOfPages() % 2).isEqualTo(0);

        zipFile.close();
    }

    /**
     * Test d'un PDF archive.
     * 
     * @throws Exception
     *             exception
     */
    @Test
    public void testDoubleProducePdfArchive() throws Exception {

        // SCN Guichet Entreprises
        // Ministère de l'économie et des finances
        // Bâtiment NECKER - Télédoc 766
        // 120, rue de Bercy
        // 75572 PARIS Cedex 12

        // 2017-09-LAB-XYV-vv_BBB : X mesures : [B blanc, M mesures], Y fond :
        // [B blanc, F fond],
        // vv version : [01, 02]

        String[] trackIds = new String[] { "2017-10-ARC-HIV-01_VCA" };
        String[] filterFiles = { "/pdf/addressFilter_1field.pdf" };
        assertThat(trackIds.length).isEqualTo(filterFiles.length);
        for (int i = 0; i < trackIds.length; i++) {
            String trackId = trackIds[i];
            File file = new File(this.getClass().getResource("/").getFile() + "pdf/exemple_dossier_IN.pdf");
            InputStream documentInputStream = new FileInputStream(file);
            PostmailBean postMailInfo = new PostmailBean();
            postMailInfo.setTrackId(trackId);
            postMailInfo.setRecipientName("SCN Guichet Entreprises" + "\n" + "Ministère de l'économie et des finances");
            postMailInfo.setRecipientAddress("Bâtiment NECKER" + "\n" + "120, rue de Bercy - Télédoc 766");
            postMailInfo.setRecipientPostalCode("75572");
            postMailInfo.setRecipientCity("Paris Cedex 12");
            String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";
            String zipFilePath = new PostmailGenerator(filterFiles[i]).generatePostmailFromFile(documentInputStream, storageDirectoryPdf, postMailInfo.getTrackerId(), postMailInfo.getRecipientName(),
                    postMailInfo.getRecipientAddress(), postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());

            ZipFile zipFile = new ZipFile(zipFilePath);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            ZipEntry entry = entries.nextElement();
            InputStream pdfStream = zipFile.getInputStream(entry);

            // double the tranformation
            PDDocument expectedPdf = PdfArchiveGenerator.generateArchive(PDDocument.load(pdfStream));

            // Check on the final PDF that the address zone is correct.
            String pdfText = extractPdfText(expectedPdf);
            assertThat(pdfText).contains(trackId);
            assertThat(pdfText).contains("SCN Guichet Entreprises");
            assertThat(pdfText).contains("120, rue de Bercy - Télédoc 766");

            // Close inputs
            zipFile.close();
            documentInputStream.close();
        }
    }

    /**
     * Extract pdf text.
     *
     * @param pdfDoc
     *            pdf doc
     * @return string
     * @throws IOException
     *             Signale qu'une exception I/O est apparue
     */
    private static String extractPdfText(final PDDocument pdfDoc) throws IOException {
        try {
            return new PDFTextStripper().getText(pdfDoc);
        } finally {
            pdfDoc.close();
        }
    }

}
