/**
 * 
 */
package fr.ge.exchange;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Test;

/**
 * @author $Author: LABEMONT $
 * @version $Revision: 0 $
 */
public class RandomTest {

  @Test
  public void generateRandom() {

    Map < Integer, Integer > values = new HashMap <>();
    int maxValues = 10000;
    int value;

    for (int i = 0; i < maxValues; i++) {
      value = generateValue(false);
      // value = generateValue17();
      values.put(value, values.get(value) != null ? values.get(value) + 1 : 1);
    }

    int nbCollision = 0;
    for (int checkValue : values.keySet()) {
      value = values.get(checkValue);
      if (value > 1) {
        System.out.printf("Collision de %d(%d)\n", checkValue, value);
        nbCollision++;
      }
    }

    System.out.printf("Collisions : %d", nbCollision);

  }

  /**
   * Basic Generator Java 1.7
   * 
   * @return
   */
  private int generateValue17() {
    return ThreadLocalRandom.current().nextInt(0, 1000000);
  }

  private int generateValue(boolean withPause) {
    if (withPause) {
      try {
        Thread.sleep(new SecureRandom().nextInt(100));
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return NumberFactory.generateValue();
  }

  @Test
  public void checkSeedFactory() {
    for (int i = 0; i < 100; i++) {
      System.out.println(SeedFactory.generateSeed());
      try {
        Thread.sleep(10l);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  /**
   * Generate int values, using SeedFactory
   * 
   * @author $Author: LABEMONT $
   * @version $Revision: 0 $
   */
  private static class NumberFactory {
    private static final long MAX_TIME_DIFFERENCE = 10;// 00 * 60 * 24;
    private static SecureRandom generator;
    private static Long generationDate;

    /**
     * 
     */
    private static void checkAndRegenerate() {
      if (generator == null || generationDate == null || System.currentTimeMillis() - generationDate > MAX_TIME_DIFFERENCE) {
        generator = new SecureRandom(SeedFactory.generateSeed());
        generationDate = System.currentTimeMillis();
      }
    }

    /**
     * 
     * @return
     */
    public static int generateValue() {
      checkAndRegenerate();
      // 2^23 = 8388608
      return generator.nextInt(9999000) + (int) (System.currentTimeMillis() % 1000);
    }
  }

  /**
   * Generate a seed.
   * 
   * @author $Author: LABEMONT $
   * @version $Revision: 0 $
   */
  private static class SeedFactory {

    private static final long MAX_TIME_DIFFERENCE = 10;// 00 * 60 * 24;
    private static byte[] seed;
    private static Long generationDate;

    private static void checkAndRegenerate() {
      if (seed == null || generationDate == null || System.currentTimeMillis() - generationDate > MAX_TIME_DIFFERENCE) {
        seed = ByteBuffer.allocate(16).putLong(System.nanoTime()).array();
        seed = new BigInteger(seed).shiftRight(new SecureRandom().nextInt(16)).toByteArray();
        generationDate = System.currentTimeMillis();
      }
    }

    /**
     * 
     * @return
     */
    public static byte[] generateSeed() {
      checkAndRegenerate();
      return seed;
    }
  }
}
