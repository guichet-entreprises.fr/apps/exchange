/**
 * 
 */
package fr.ge.exchange.domaine;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

/**
 * Test Imprimerie Nationale DeliveryId : internalId_[R/V][N/C][R/A/L/E]
 * 
 * @author $Author: LABEMONT $
 * @version $Revision: 0 $
 */
public class ImprimerieNationaleDeliveryIdGeneratorTest {

  @Test
  public void deliveryIdGeneration() {
    String trackerId = "2018-01-AAA-BB-11";
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).buildId()).isEqualTo(trackerId + "_VCA");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).recto().buildId()).isEqualTo(trackerId + "_RCA");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).blackAndWhite().buildId()).isEqualTo(trackerId + "_VNA");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).recommande().buildId()).isEqualTo(trackerId + "_VCR");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).lettre().buildId()).isEqualTo(trackerId + "_VCL");
    assertThat(
      ImprimerieNationaleDeliveryIdGenerator.create(trackerId).rectoVerso().color().recommandeAccuseReception().buildId())
        .isEqualTo(trackerId + "_VCA");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).recommande().recommandeAccuseReception().lettre()
      .economie().buildId()).isEqualTo(trackerId + "_VCE");
  }

  @Test
  public void deliveryModeGeneration() {
    String trackerId = "2018-01-AAA-BB-11";
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).getDeliveryCode()).isEqualTo("VCA");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).recto().getDeliveryCode()).isEqualTo("RCA");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).blackAndWhite().getDeliveryCode()).isEqualTo("VNA");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).recommande().getDeliveryCode()).isEqualTo("VCR");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).lettre().getDeliveryCode()).isEqualTo("VCL");
    assertThat(
      ImprimerieNationaleDeliveryIdGenerator.create(trackerId).rectoVerso().color().recommandeAccuseReception().getDeliveryCode())
        .isEqualTo("VCA");
    assertThat(ImprimerieNationaleDeliveryIdGenerator.create(trackerId).recommande().recommandeAccuseReception().lettre()
      .economie().getDeliveryCode()).isEqualTo("VCE");
  }

}
