package fr.ge.exchange.dao;

import static org.fest.assertions.Assertions.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;

import java.util.Arrays;

import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.MailMessage;
import fr.ge.exchange.test.AbstractDbTest;

/**
 * Tests Postmail.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-ge-exchange-service-config-test.xml" })
public class MailMessageDAOTest extends AbstractDbTest {

  /** The postmail mapper. */
  @Autowired
  private MailMessageDAO mailMessageDAO;

  /**
   * Setup.
   *
   * @throws Exception
   *           exception
   */
  @Before
  public void setUp() throws Exception {
    this.initDb(this.resourceName("dataset.xml"));
  }

  /**
   * Tests read
   * 
   * @throws TechniqueException
   */
  @Test
  public void testGetPostmail() throws TechniqueException {
    MatcherAssert.assertThat(this.mailMessageDAO.read("2016-11-VHB-LKD-55"), allOf(Arrays.asList(notNullValue(),
      hasProperty("trackerId", equalTo("2016-11-VHB-LKD-55")), hasProperty("recipientCity", equalTo("Vincennes")))));
  }

  /**
   * Test create
   * 
   * @throws TechniqueException
   */
  @Test
  public void testInsertPostmail() throws TechniqueException {
    String trackId = "2017-02-GHI-JKM-00";
    String recipientAddress = "1 rue Vaugirard";
    String recipientCity = "Paris";
    String recipientPostalCode = "75015";
    String recipientName = "Mairie de Paris 15";
    String attachmentPath = "attachment.zip";
    MailMessage mailMessage = new MailMessage();
    mailMessage.setTrackerId(trackId);
    mailMessage.setRecipientAddress(recipientAddress);
    mailMessage.setRecipientCity(recipientCity);
    mailMessage.setRecipientPostalCode(recipientPostalCode);
    mailMessage.setRecipientName(recipientName);
    mailMessage.setAttachmentPath(attachmentPath);

    mailMessageDAO.create(mailMessage);

    MailMessage actual = mailMessageDAO.read(trackId);
    assertThat(actual.getTrackerId()).isEqualTo(trackId);
    assertThat(actual.getRecipientAddress()).isEqualTo(recipientAddress);
    assertThat(actual.getRecipientCity()).isEqualTo(recipientCity);
    assertThat(actual.getRecipientPostalCode()).isEqualTo(recipientPostalCode);
    assertThat(actual.getRecipientName()).isEqualTo(recipientName);
    assertThat(actual.getAttachmentPath()).isEqualTo(attachmentPath);
  }

  /**
   * Test update
   * 
   * @throws TechniqueException
   */
  @Test
  public void testUpdatePostmail() throws TechniqueException {
    String trackId = "2016-11-VHB-LKD-55";
    String recipientAddress = "20 rue de Paris";
    String recipientCity = "Vincennes";
    String recipientPostalCode = "94300";
    String recipientName = "Maire de Vincennes";
    String attachmentPath = "2017-02-08_2016-11-VHB-LKD-55.pdf";
    String status = "testUpdate";
    MailMessage mailMessage = new MailMessage();
    mailMessage.setTrackerId(trackId);
    mailMessage.setStatus(status);

    mailMessageDAO.update(mailMessage);

    MailMessage actual = mailMessageDAO.read(trackId);
    assertThat(actual.getTrackerId()).isEqualTo(trackId);
    assertThat(actual.getStatus()).isEqualTo(status);
    assertThat(actual.getRecipientAddress()).isEqualTo(recipientAddress);
    assertThat(actual.getRecipientCity()).isEqualTo(recipientCity);
    assertThat(actual.getRecipientPostalCode()).isEqualTo(recipientPostalCode);
    assertThat(actual.getRecipientName()).isEqualTo(recipientName);
    assertThat(actual.getAttachmentPath()).isEqualTo(attachmentPath);
  }

  /**
   * Test create/update
   * 
   * @throws TechniqueException
   */
  @Test
  public void testCreateUpdatePostmail() throws TechniqueException {
    String trackId = "2017-02-GHI-JKM-00";
    String recipientAddress = "1 rue Vaugirard";
    String recipientCity = "Paris";
    String recipientPostalCode = "75015";
    String recipientName = "Mairie de Paris 15";
    String attachmentPath = "attachment.zip";
    MailMessage mailMessage = new MailMessage();
    mailMessage.setTrackerId(trackId);
    mailMessage.setDeliveryId(trackId + "_RVR");
    mailMessage.setRecipientAddress(recipientAddress);
    mailMessage.setRecipientCity(recipientCity);
    mailMessage.setRecipientPostalCode(recipientPostalCode);
    mailMessage.setRecipientName(recipientName);
    mailMessage.setAttachmentPath(attachmentPath);

    // create
    mailMessageDAO.create(mailMessage);

    // re-read
    MailMessage actual = mailMessageDAO.read(trackId);
    assertThat(actual.getTrackerId()).isEqualTo(trackId);
    assertThat(actual.getRecipientAddress()).isEqualTo(recipientAddress);
    assertThat(actual.getRecipientCity()).isEqualTo(recipientCity);
    assertThat(actual.getRecipientPostalCode()).isEqualTo(recipientPostalCode);
    assertThat(actual.getRecipientName()).isEqualTo(recipientName);
    assertThat(actual.getAttachmentPath()).isEqualTo(attachmentPath);

    String deliveryCode = "RVR";
    String deliveryId = "1_3889stanchristophe.lebeau@finances.gouv.fr3889.pdf";
    int numberPages = 2;
    String channel = "POSTE";
    String urgency = "R1 avec AR";
    String numreco = "12345678901234567890";
    String status = "Transféré";
    mailMessage.setDeliveryCode(deliveryCode);
    mailMessage.setDeliveryId(deliveryId);
    mailMessage.setDatePostmail(null);
    mailMessage.setNumberPages(numberPages);
    mailMessage.setChannel(channel);
    mailMessage.setUrgency(urgency);
    mailMessage.setNumreco(numreco);
    mailMessage.setStatus(status);

    // modify
    mailMessageDAO.update(mailMessage);

    // re-read
    actual = mailMessageDAO.read(trackId);
    assertThat(actual.getTrackerId()).isEqualTo(trackId);
    assertThat(actual.getDeliveryCode()).isEqualTo(deliveryCode);
    assertThat(actual.getDeliveryId()).isEqualTo(deliveryId);
    assertThat(actual.getDatePostmail()).isNotNull();
    assertThat(actual.getNumberPages()).isEqualTo(numberPages);
    assertThat(actual.getChannel()).isEqualTo(channel);
    assertThat(actual.getUrgency()).isEqualTo(urgency);
    assertThat(actual.getStatus()).isEqualTo(status);
    assertThat(actual.getNumreco()).isEqualTo(numreco);

  }
}
