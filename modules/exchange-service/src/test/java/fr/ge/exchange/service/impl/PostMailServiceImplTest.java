package fr.ge.exchange.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.core.exception.TechniqueException;
import fr.ge.ct.ses.clients.SFTPClient;
import fr.ge.exchange.bean.MailMessage;
import fr.ge.exchange.dao.PostmailDAO;
import fr.ge.exchange.domaine.IMailMessageDomaine;
import fr.ge.exchange.domaine.ImprimerieNationaleDeliveryIdGenerator;
import fr.ge.exchange.service.IPostMailService;
import fr.ge.exchange.service.ITrackerLinkService;

/**
 * Class PostMailServiceImplTest.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class PostMailServiceImplTest {

    /** The postmail service. */
    @InjectMocks
    private IPostMailService postMailService = new PostMailServiceImpl();

    /** The mail message domaine. */
    @Mock
    private IMailMessageDomaine mailMessageDomaine;

    /** The postmail mapper. */
    @Mock
    private PostmailDAO postmailDAO;

    /** sftp client. */
    @Mock
    private SFTPClient sftpClient;

    /** service to call tracker. */
    @Mock
    private ITrackerLinkService trackerLink;

    /** storage directory pdf in local. */
    private String localDirectoryPdf = this.getClass().getClassLoader().getResource(".").getFile() + File.separator + "pdf";

    /** storage directory pdf in local. */
    private String sftpDirectoryPdf = "in";

    /** file where there is the template. */
    private File file = new File(this.getClass().getClassLoader().getResource(".").getFile() + File.separator + "pdf/templateTestOK.pdf");

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test postMail.
     * 
     * @throws TechniqueException
     * @throws FileNotFoundException
     * 
     * @throws Exception
     *             Raise Exception
     */
    @Test
    public void testPostmail() throws TechniqueException, FileNotFoundException {
        String trackId = "2017-02-GHI-JKM-01";
        MailMessage mailMessage = new MailMessage();
        mailMessage.setRecipientAddress("1 rue Vaugirard");
        mailMessage.setRecipientCity("Paris");
        mailMessage.setRecipientPostalCode("75014");
        mailMessage.setRecipientName("Mairie de Paris 14");

        InputStream templateStream = new FileInputStream(file);

        Mockito.when(trackerLink.createTrackerId(null)).thenReturn(trackId);

        String postMailTrackerId = postMailService.postMail(templateStream, mailMessage, null, localDirectoryPdf, sftpDirectoryPdf);
        assertThat(postMailTrackerId).isEqualTo(trackId);

        // check deliveryId
        ImprimerieNationaleDeliveryIdGenerator deliveryIdGenerator = ImprimerieNationaleDeliveryIdGenerator.create(trackId).rectoVerso().color().recommandeAccuseReception();
        assertThat(mailMessage.getDeliveryId()).isEqualTo(deliveryIdGenerator.buildId());
        assertThat(mailMessage.getDeliveryModeId()).isEqualTo(deliveryIdGenerator.getDeliveryCode());
    }

    /**
     * Test postMail with wrong attachment type.
     * 
     * @throws TechniqueException
     * @throws FileNotFoundException
     * 
     * @throws Exception
     *             Raise Exception
     */
    @Test(expected = TechniqueException.class)
    public void testPostmailJpeg() throws TechniqueException, FileNotFoundException {
        String trackId = "2017-02-GHI-JKM-02";

        MailMessage mailMessage = new MailMessage();
        mailMessage.setRecipientAddress("1 rue Vaugirard");
        mailMessage.setRecipientCity("Paris");
        mailMessage.setRecipientPostalCode("75014");
        mailMessage.setRecipientName("Mairie de Paris 14");

        InputStream templateStream = new FileInputStream(file);
        String pdfLocalStorageDirectory = "storageTest/pdf";

        Mockito.when(trackerLink.createTrackerId(null)).thenReturn(trackId);

        postMailService.postMail(templateStream, mailMessage, null, pdfLocalStorageDirectory, sftpDirectoryPdf);
    }

    /**
     * Test postMail with null mailMessage.
     * 
     * @throws TechniqueException
     * @throws FileNotFoundException
     * 
     * @throws Exception
     *             Raise Exception
     */
    @Test(expected = TechniqueException.class)
    public void testPostmailNullMailMessage() throws TechniqueException, FileNotFoundException {
        MailMessage mailMessage = null;

        InputStream templateStream = new FileInputStream(file);

        postMailService.postMail(templateStream, mailMessage, null, localDirectoryPdf, sftpDirectoryPdf);
    }

    /**
     * Test download attachment.
     * 
     * @throws Exception
     *             Raise Exception
     */
    @Test
    public void testDownload() throws Exception {
        String trackId = "2016-11-VHB-LKD-55";

        MailMessage mailMessage = new MailMessage();
        mailMessage.setRecipientAddress("1 rue Vaugirard");
        mailMessage.setRecipientCity("Paris");
        mailMessage.setRecipientPostalCode("75014");
        mailMessage.setRecipientName("Mairie de Paris 14");
        mailMessage.setAttachmentPath(PostMailServiceImplTest.class.getResource("/").getFile() + "/zip/2017-02-16_2016-11-VHB-LKD-55.zip");

        Mockito.when(mailMessageDomaine.read(trackId)).thenReturn(mailMessage);

        MailMessage mailMessageRead = postMailService.getMailMessage(trackId);
        assertThat(mailMessage).isEqualTo(mailMessageRead); // mocked ==> same
                                                            // object, no need
                                                            // to do
                                                            // more than that.
    }

}
