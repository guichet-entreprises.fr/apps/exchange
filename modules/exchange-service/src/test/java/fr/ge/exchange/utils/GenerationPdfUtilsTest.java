package fr.ge.exchange.utils;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.Test;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.PostmailBean;

/**
 * Test generation of PDFs.
 */
public class GenerationPdfUtilsTest {

  /**
   * Test generation pdf with adresse filter.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testOverlapAndStorePdfWithAddressFilter() throws Exception {

    // Recto/Verso : [RV]
    // NB/Couleur : [NC]
    // Recommandé/AR/Non : [RAN]
    File file = new File(this.getClass().getResource("/").getFile() + "pdf/courrier_IN_INPI.pdf");
    InputStream templateStream = new FileInputStream(file);
    PostmailBean postMailInfo = new PostmailBean();
    postMailInfo.setTrackId("2017-02-GHI-JKO-00_VCA");
    postMailInfo.setRecipientName("Hôtel de Ville de Paris");
    postMailInfo.setRecipientAddress("Place de l'Hôtel de Ville");
    postMailInfo.setRecipientPostalCode("75572");
    postMailInfo.setRecipientCity("Paris Cedex 12");
    String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";
    String zipFilePath = GenerationPdfUtils.overlapAndStorePdfWithAddressFilter(templateStream, storageDirectoryPdf,
      postMailInfo.getTrackerId(), postMailInfo.getRecipientName(), postMailInfo.getRecipientAddress(),
      postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());

    // Check if the zipFilePath is correct
    String expectedZipStorage = this.getClass().getResource("/").getFile() + "pdf" + File.separator
      + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "_2017-02-GHI-JKO-00_VCA.zip";
    assertThat(zipFilePath).isEqualTo(expectedZipStorage);

    // Récupération du pdf dans le ZIP généré, et on vérifie que l'adresse du filtre a bien été
    // ajouté
    ZipFile zipFile = new ZipFile(expectedZipStorage);
    Enumeration < ? extends ZipEntry > entries = zipFile.entries();
    ZipEntry entry = entries.nextElement();
    InputStream pdfStream = zipFile.getInputStream(entry);
    PDDocument expectedPdf = PDDocument.load(pdfStream);
    assertThat(expectedPdf).isNotNull();
    String pdfText = extractPdfText(expectedPdf);
    assertThat(pdfText).contains("2017-02-GHI-JKO-00_VCA");
    assertThat(pdfText).contains("Hôtel de Ville de Paris");

    // Close inputs
    zipFile.close();
    expectedPdf.close();
  }

  /**
   * Test encoding and address length.
   * 
   * @throws Exception
   *           exception
   */
  @Test
  public void testAddressEncoding() throws Exception {

    // SCN Guichet Entreprises
    // Ministère de l'économie et des finances
    // Bâtiment NECKER - Télédoc 766
    // 120, rue de Bercy
    // 75572 PARIS Cedex 12

    File file = new File(this.getClass().getResource("/").getFile() + "pdf/courrier_IN_INPI.pdf");
    InputStream templateStream = new FileInputStream(file);
    PostmailBean postMailInfo = new PostmailBean();
    String trackId = "2017-02-LAB-LAB-00_VCA";
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientName("SCN Guichet Entreprises" + "\n" + "Ministère de l'économie et des finances");
    postMailInfo.setRecipientAddress("Bâtiment NECKER -  Télédoc 766" + "\n" + "120, rue de Bercy" + "\n"
      + "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ");
    // "€�‚ƒ„…†‡ˆ‰Š‹Œ�Ž��‘’“”•–—˜™š›œ�žŸ ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ");
    postMailInfo.setRecipientPostalCode("75572");
    postMailInfo.setRecipientCity("Paris Cedex 12");
    String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";
    String zipFilePath = GenerationPdfUtils.overlapAndStorePdfWithAddressFilter(templateStream, storageDirectoryPdf,
      postMailInfo.getTrackerId(), postMailInfo.getRecipientName(), postMailInfo.getRecipientAddress(),
      postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());

    // Check if the zipFilePath is correct
    String expectedZipStorage = this.getClass().getResource("/").getFile() + "pdf" + File.separator
      + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "_2017-02-LAB-LAB-00_VCA.zip";
    assertThat(zipFilePath).isEqualTo(expectedZipStorage);

    // Récupération du pdf dans le ZIP généré, et on vérifie que l'adresse du filtre a bien été
    // ajouté
    ZipFile zipFile = new ZipFile(expectedZipStorage);
    Enumeration < ? extends ZipEntry > entries = zipFile.entries();
    ZipEntry entry = entries.nextElement();
    InputStream pdfStream = zipFile.getInputStream(entry);
    PDDocument expectedPdf = PDDocument.load(pdfStream);
    assertThat(expectedPdf).isNotNull();
    String pdfText = extractPdfText(expectedPdf);
    assertThat(pdfText).contains("2017-02-LAB-LAB-00_VCA");
    assertThat(pdfText).contains("SCN Guichet Entreprises");
    assertThat(pdfText).contains("ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ");

    // Close inputs
    zipFile.close();
    expectedPdf.close();
  }

  /**
   * Test barrette : Le contenu de la barrette technique est de la forme :
   * AAAA-MM-xxx-xxx-NN_[RV][NC][RALE].
   * 
   * @throws Exception
   *           exception
   */
  @Test
  public void testBarrette() throws Exception {

    // SCN Guichet Entreprises
    // Ministère de l'économie et des finances
    // Bâtiment NECKER - Télédoc 766
    // 120, rue de Bercy
    // 75572 PARIS Cedex 12

    String[] trackIds = new String[] {"2017-01-LAB-AAA-01_RNR", "2017-02-LAB-BBB-02_VCA", "2017-03-LAB-CCC-03_RCL",
      "2017-04-LAB-DDD-04_VNE" };
    int i = 0;
    for (String trackId : trackIds) {
      File file = new File(this.getClass().getResource("/").getFile() + "pdf/courrier_IN_INPI.pdf");
      InputStream templateStream = new FileInputStream(file);
      PostmailBean postMailInfo = new PostmailBean();
      postMailInfo.setTrackId(trackId);
      postMailInfo.setRecipientName("SCN Guichet Entreprises" + ++i + "\n" + "Ministère de l'économie et des finances");
      postMailInfo.setRecipientAddress("Bâtiment NECKER -  Télédoc 766" + "\n" + "120, rue de Bercy");
      postMailInfo.setRecipientPostalCode("75572");
      postMailInfo.setRecipientCity("Paris Cedex 12");
      String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";
      String zipFilePath = GenerationPdfUtils.overlapAndStorePdfWithAddressFilter(templateStream, storageDirectoryPdf,
        postMailInfo.getTrackerId(), postMailInfo.getRecipientName(), postMailInfo.getRecipientAddress(),
        postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());

      // Check if the zipFilePath is correct
      String expectedZipStorage = this.getClass().getResource("/").getFile() + "pdf" + File.separator
        + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "_" + trackId + ".zip";
      assertThat(zipFilePath).isEqualTo(expectedZipStorage);

      // Récupération du pdf dans le ZIP généré, et on vérifie que l'adresse du filtre a bien été
      // ajouté
      ZipFile zipFile = new ZipFile(expectedZipStorage);
      Enumeration < ? extends ZipEntry > entries = zipFile.entries();
      ZipEntry entry = entries.nextElement();
      InputStream pdfStream = zipFile.getInputStream(entry);
      PDDocument expectedPdf = PDDocument.load(pdfStream);
      assertThat(expectedPdf).isNotNull();
      String pdfText = extractPdfText(expectedPdf);
      assertThat(pdfText).contains(trackId);
      assertThat(pdfText).contains("SCN Guichet Entreprises");

      // Close inputs
      zipFile.close();
      expectedPdf.close();
      templateStream.close();
    }
  }

  /**
   * Test les exceptions de la méthode de génération de PDF.
   *
   * @throws Exception
   *           exception
   */
  @Test
  public void testOverlapAndStorePdfWithAddressFilterException() throws Exception {

    // template is a jpg and not a pdf => error with generation of the PDF File
    // TODO LAB : retirer le sangoku de l'image de test
    File fakePdf = new File(this.getClass().getResource("/").getFile() + "jpg/templateTest.jpg");
    InputStream fakeTemplateStream = new FileInputStream(fakePdf);
    PostmailBean postMailInfo = new PostmailBean();
    postMailInfo.setTrackId("VHB-LKD-55");
    postMailInfo.setRecipientName("Hôtel de Ville de Paris");
    postMailInfo.setRecipientAddress("Place de l'Hôtel de Ville");
    postMailInfo.setRecipientPostalCode("75572");
    postMailInfo.setRecipientCity("Paris Cedex 12");
    String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";
    try {
      GenerationPdfUtils.overlapAndStorePdfWithAddressFilter(fakeTemplateStream, storageDirectoryPdf, postMailInfo.getTrackerId(),
        postMailInfo.getRecipientName(), postMailInfo.getRecipientAddress(), postMailInfo.getRecipientPostalCode(),
        postMailInfo.getRecipientCity());
      fail("Technical Exception expected");
    } catch (TechniqueException e) {
      assertThat(e.getCode()).isEqualTo("ERR_EXCH_INVALID_PDF");
    }

    // Address of the storage directory does no exit => error with storage of the PDF File
    File fileOK = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestOK.pdf");
    InputStream templateStreamOK = new FileInputStream(fileOK);
    String storageDirectoryPdfError = this.getClass().getResource("/").getFile() + "fakeStorage/pdf";
    try {
      GenerationPdfUtils.overlapAndStorePdfWithAddressFilter(templateStreamOK, storageDirectoryPdfError,
        postMailInfo.getTrackerId(), postMailInfo.getRecipientName(), postMailInfo.getRecipientAddress(),
        postMailInfo.getRecipientPostalCode(), postMailInfo.getRecipientCity());
      fail("Technical Exception expected");
    } catch (TechniqueException e) {
      assertThat(e.getCode()).isEqualTo("ERR_EXCH_DISK_ERROR");
    }

    // Content of the PDF is invalid because reserved zones are not empty => error invalid PDF
    File fileKO = new File(this.getClass().getResource("/").getFile() + "pdf/templateTestKO.pdf");
    InputStream templateStreamKO = new FileInputStream(fileKO);
    try {
      GenerationPdfUtils.overlapAndStorePdfWithAddressFilter(templateStreamKO, storageDirectoryPdf, postMailInfo.getTrackerId(),
        postMailInfo.getRecipientName(), postMailInfo.getRecipientAddress(), postMailInfo.getRecipientPostalCode(),
        postMailInfo.getRecipientCity());
      fail("Technical Exception expected");
    } catch (TechniqueException e) {
      assertThat(e.getCode()).isEqualTo("ERR_EXCH_INVALID_PDF");
    }

  }

  /**
   * Extract pdf text.
   *
   * @param pdfDoc
   *          pdf doc
   * @return string
   * @throws IOException
   *           Signale qu'une exception I/O est apparue
   */
  private static String extractPdfText(final PDDocument pdfDoc) throws IOException {
    try {
      return new PDFTextStripper().getText(pdfDoc);
    } finally {
      pdfDoc.close();
    }
  }

}
