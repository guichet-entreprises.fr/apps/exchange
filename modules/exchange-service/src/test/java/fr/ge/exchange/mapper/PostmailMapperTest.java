package fr.ge.exchange.mapper;

import static org.fest.assertions.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.exchange.bean.PostmailBean;
import fr.ge.exchange.dao.PostmailDAO;
import fr.ge.exchange.test.AbstractDbTest;

/**
 * Tests Postmail.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-ge-exchange-service-config-test.xml" })
public class PostmailMapperTest extends AbstractDbTest {

  /** The postmail mapper. */
  @Autowired
  private PostmailDAO postmailDAO;

  /**
   * Setup.
   *
   * @throws Exception
   *           exception
   */
  @Before
  public void setUp() throws Exception {
    this.initDb(this.resourceName("dataset.xml"));
  }

  /**
   * Tests {@link PostmailDAO#getPostmail(String)}.
   */
  @Test
  public void testGetPostmail() {
    assertThat(this.postmailDAO.getPostmail("2016-11-VHB-LKD-55"), allOf(Arrays.asList(notNullValue(),
      hasProperty("trackerId", equalTo("2016-11-VHB-LKD-55")), hasProperty("recipientCity", equalTo("Vincennes")))));
  }

  /**
   * Test {@link PostmailDAO#insertPostmail(PostmailBean)}.
   */
  @Test
  public void testInsertPostmail() {
    String trackId = "2017-02-GHI-JKM-00";
    String recipientAddress = "1 rue Vaugirard";
    String recipientCity = "Paris";
    String recipientPostalCode = "75015";
    String recipientName = "Mairie de Paris 15";
    String attachmentPath = "attachment.zip";
    PostmailBean postMailInfo = new PostmailBean();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setRecipientAddress(recipientAddress);
    postMailInfo.setRecipientCity(recipientCity);
    postMailInfo.setRecipientPostalCode(recipientPostalCode);
    postMailInfo.setRecipientName(recipientName);
    postMailInfo.setAttachmentPath(attachmentPath);

    postmailDAO.insertPostmail(postMailInfo);

    PostmailBean actual = postmailDAO.getPostmail(trackId);
    assertThat(actual.getTrackerId()).isEqualTo(trackId);
    assertThat(actual.getRecipientAddress()).isEqualTo(recipientAddress);
    assertThat(actual.getRecipientCity()).isEqualTo(recipientCity);
    assertThat(actual.getRecipientPostalCode()).isEqualTo(recipientPostalCode);
    assertThat(actual.getRecipientName()).isEqualTo(recipientName);
    assertThat(actual.getAttachmentPath()).isEqualTo(attachmentPath);
  }

  /**
   * {@link PostmailDAO#udpatePostmail(PostmailBean)}.
   */
  @Test
  public void testUpdatePostmail() {
    String trackId = "2016-11-VHB-LKD-55";
    String recipientAddress = "20 rue de Paris";
    String recipientCity = "Vincennes";
    String recipientPostalCode = "94300";
    String recipientName = "Maire de Vincennes";
    String attachmentPath = "2017-02-08_2016-11-VHB-LKD-55.pdf";
    String status = "testUpdate";
    PostmailBean postMailInfo = new PostmailBean();
    postMailInfo.setTrackId(trackId);
    postMailInfo.setStatus(status);

    postmailDAO.updatePostmail(postMailInfo);

    PostmailBean actual = postmailDAO.getPostmail(trackId);
    assertThat(actual.getTrackerId()).isEqualTo(trackId);
    assertThat(actual.getStatus()).isEqualTo(status);
    assertThat(actual.getRecipientAddress()).isEqualTo(recipientAddress);
    assertThat(actual.getRecipientCity()).isEqualTo(recipientCity);
    assertThat(actual.getRecipientPostalCode()).isEqualTo(recipientPostalCode);
    assertThat(actual.getRecipientName()).isEqualTo(recipientName);
    assertThat(actual.getAttachmentPath()).isEqualTo(attachmentPath);
  }
}
