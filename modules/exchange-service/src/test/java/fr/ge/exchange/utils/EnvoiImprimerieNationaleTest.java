package fr.ge.exchange.utils;

import static org.fest.assertions.Assertions.assertThat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.Ignore;
import org.junit.Test;

import fr.ge.exchange.bean.PostmailBean;

/**
 * Test pour l'IN.
 */
@Ignore("Uniquement pour l'IN")
public class EnvoiImprimerieNationaleTest {

  /**
   * Création d'un fichier pour l'Imprimerie Nationale : Le contenu de la barrette technique est de
   * la forme : AAAA-MM-xxx-xxx-NN_[RV][NC][RALE].
   * 
   * @throws Exception
   *           exception
   */
  @Test
  public void testFichierExempleIN() throws Exception {

    // Service Guichet Entreprises
    // Direction Générale des Entreprises
    // Ministère de l'économie et des finances
    // Bâtiment NECKER - Télédoc 766
    // 120, rue de Bercy
    // 75572 PARIS Cedex 12

    String[] trackIds = new String[] {"2017-09-LAB-AAA-01_VCA", "2017-09-LAB-AAA-02_RNR", "2017-09-LAB-AAA-03_VNE",
      "2017-09-LAB-AAA-04_RCL" };
    int i = 0;
    for (String trackId : trackIds) {
      File file = new File(this.getClass().getResource("/").getFile() + "pdf/exemple_dossier_IN.pdf");
      InputStream templateStream = new FileInputStream(file);
      PostmailBean postMail = new PostmailBean();
      postMail.setReferenceId(trackId);
      postMail.setRecipientName("Service Guichet Entreprises\n" + "Direction Générale des Entreprises" + "\n"
        + "Ministère de l'économie et des finances");
      postMail.setRecipientAddress("Bâtiment NECKER -  Télédoc 766" + "\n" + "120, rue de Bercy");
      postMail.setRecipientPostalCode("75572");
      postMail.setRecipientCity("Paris Cedex 12");
      String storageDirectoryPdf = this.getClass().getResource("/").getFile() + "pdf";
      String zipFilePath = GenerationPdfUtils.overlapAndStorePdfWithAddressFilter(templateStream, storageDirectoryPdf,
        postMail.getTrackerId(), postMail.getRecipientName(), postMail.getRecipientAddress(), postMail.getRecipientPostalCode(),
        postMail.getRecipientCity());

      // Check if the zipFilePath is correct
      String expectedZipStorage = this.getClass().getResource("/").getFile() + "pdf" + File.separator
        + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + "_" + trackId + ".zip";
      assertThat(zipFilePath).isEqualTo(expectedZipStorage);

      // Récupération du pdf dans le ZIP généré, et on vérifie que l'adresse du filtre a bien été
      // ajouté
      ZipFile zipFile = new ZipFile(expectedZipStorage);
      Enumeration < ? extends ZipEntry > entries = zipFile.entries();
      ZipEntry entry = entries.nextElement();
      InputStream pdfStream = zipFile.getInputStream(entry);
      PDDocument expectedPdf = PDDocument.load(pdfStream);
      assertThat(expectedPdf).isNotNull();
      String pdfText = extractPdfText(expectedPdf);
      assertThat(pdfText).contains(trackId);
      assertThat(pdfText).contains("Service Guichet Entreprises");

      // Close inputs
      zipFile.close();
      expectedPdf.close();
      templateStream.close();
    }
  }

  /**
   * Extract pdf text.
   *
   * @param pdfDoc
   *          pdf doc
   * @return string
   * @throws IOException
   *           Signale qu'une exception I/O est apparue
   */
  private static String extractPdfText(final PDDocument pdfDoc) throws IOException {
    try {
      return new PDFTextStripper().getText(pdfDoc);
    } finally {
      pdfDoc.close();
    }
  }

}
