package fr.ge.exchange.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.MailMessage;

/**
 * Testing the {@link MailMessageResultsParser}
 */
public class MailMessageResultParserTest {

    /**
     * Test de la méthode readAndParseCSV.
     * 
     * @throws IOException
     */
    @Test
    public void testReadAndParseCSV() throws TechniqueException {
        String updateSatusCsvDirectoryStorage = this.getClass().getClassLoader().getResource(".").getFile() + File.separator + "csv";
        String updateStatusCsvFile = "updateStatus.csv";

        File fileSrc = new File(updateSatusCsvDirectoryStorage + "/" + updateStatusCsvFile);
        List<MailMessage> results = MailMessageResultsParser.parse(fileSrc.getAbsolutePath());
        assertThat(results, allOf(notNullValue(), hasSize(5)));
    }

    /**
     * Test de la méthode readAndParseCSV en erreur.
     * 
     * @throws IOException
     */
    @Test(expected = TechniqueException.class)
    public void testReadAndParseCSVException() throws TechniqueException {
        String updateSatusCsvDirectoryStorage = this.getClass().getClassLoader().getResource(".").getFile() + File.separator + "csv";
        String updateStatusCsvFile = "this_file_does_not_exist.csv";

        File fileSrc = new File(updateSatusCsvDirectoryStorage + "/" + updateStatusCsvFile);
        MailMessageResultsParser.parse(fileSrc.getAbsolutePath());
    }

    /**
     * Test de la méthode readAndParseCSV en erreur avec fichier vide.
     * 
     * @throws IOException
     */
    @Test(expected = TechniqueException.class)
    public void testReadAndParseCSVEmpty() throws TechniqueException {
        String updateSatusCsvDirectoryStorage = this.getClass().getClassLoader().getResource(".").getFile() + File.separator + "csv";
        String updateStatusCsvFile = "updateStatusEmpty.csv";

        File fileSrc = new File(updateSatusCsvDirectoryStorage + "/" + updateStatusCsvFile);
        MailMessageResultsParser.parse(fileSrc.getAbsolutePath());
    }

    /**
     * The result file is completely empty. TODO finir le test
     */
    @Test
    public void testEmptyFile() {

    }

    /**
     * The result file is completely empty but with headers. TODO finir le test
     */
    @Test
    public void testEmptyFileWithHeaders() {

    }

}
