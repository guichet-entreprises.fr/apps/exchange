/**
 * 
 */
package fr.ge.exchange.utils;

import static org.fest.assertions.Assertions.assertThat;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.junit.Test;

import fr.ge.core.exception.TechniqueException;

/**
 * Test for {@link PdfArchiveGenerator}. Don't really know what to test, just checking the page
 * totals are the same.
 */
public class PdfArchiveGeneratorTest {

  @Test
  public void fromDynamiquePdfToArchivePdf() throws TechniqueException, IOException {

    PDDocument document = PDDocument.load(this.getClass().getResourceAsStream("/pdf/exemple_dossier_IN.pdf"));

    PDDocument documentA = PdfArchiveGenerator.generateArchive(document);

    // assertThat(new PDFTextStripper().getText(document)).contains("2017-09-EVC-CVJ-46");
    int nbPagesOrigin = document.getNumberOfPages();
    document.close();

    assertThat(documentA.getNumberOfPages()).isEqualTo(nbPagesOrigin);
    // assertThat(new PDFTextStripper().getText(documentA)).contains("2017-09-EVC-CVJ-46");
    documentA.close();
  }
}
