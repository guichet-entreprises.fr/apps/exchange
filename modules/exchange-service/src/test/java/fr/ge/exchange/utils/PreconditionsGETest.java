/**
 * 
 */
package fr.ge.exchange.utils;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.constante.ICodeExceptionConstante;

/**
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
public class PreconditionsGETest {

  /**
   * Constructeur de la classe.
   *
   */
  public PreconditionsGETest() {

  }

  @Test
  public void testCheckNull() {
    Object object = null;
    String codeErreur = "codeErreur";
    PreconditionsGE preconditionsGE = new PreconditionsGE(codeErreur);

    try {
      preconditionsGE.checkNotNull(object, "object");
      fail("Le contrôle doit jeter une exception");
    } catch (Exception e) {
      assertThat(e).isInstanceOf(TechniqueException.class);
      assertThat(((TechniqueException) e).getCode()).isEqualTo(codeErreur);
      assertThat(preconditionsGE.getCodeErreur()).isEqualTo(codeErreur);
    }
  }

  @Test
  public void testCheckNotNull() {
    String object = "objet";
    String codeErreur = "codeErreur";
    PreconditionsGE preconditionsGE = new PreconditionsGE();
    preconditionsGE.setCodeErreur(codeErreur);

    try {
      preconditionsGE.checkNotNull(object, "object");
    } catch (Exception e) {
      assertThat(e).isInstanceOf(TechniqueException.class);
      assertThat(((TechniqueException) e).getCode()).isEqualTo(codeErreur);
    }
  }

  @Test
  public void testCheckStringBlank() {
    String str = " ";
    PreconditionsGE preconditionsGE = new PreconditionsGE();

    try {
      preconditionsGE.checkStringNotBlank(str, "str");
      fail("Le contrôle doit jeter une exception");
    } catch (Exception e) {
      assertThat(e).isInstanceOf(TechniqueException.class);
      assertThat(((TechniqueException) e).getCode()).isEqualTo(ICodeExceptionConstante.ERR_EXCH_INVALID_PARAMETER);
    }
  }

  @Test
  public void testCheckStringNotBlank() {
    String str = "notBlank";
    PreconditionsGE preconditionsGE = new PreconditionsGE();

    try {
      preconditionsGE.checkStringNotBlank(str, "str");
    } catch (Exception e) {
      assertThat(e).isInstanceOf(TechniqueException.class);
      assertThat(((TechniqueException) e).getCode()).isEqualTo(ICodeExceptionConstante.ERR_EXCH_INVALID_PARAMETER);
    }
  }
}
