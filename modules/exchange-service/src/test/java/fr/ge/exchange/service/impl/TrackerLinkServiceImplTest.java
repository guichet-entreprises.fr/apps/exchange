package fr.ge.exchange.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.bean.MessageTrackerProvider;
import fr.ge.tracker.facade.ITrackerFacade;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:spring/ge-services-commons-properties-files-config.xml" })
public class TrackerLinkServiceImplTest {

    /** The postmail service. */
    @InjectMocks
    private TrackerLinkServiceImpl trackerLinkService;

    /** tracker facade. */
    @Mock
    private ITrackerFacade trackerFacade;

    /**
     * Message Provider.
     */
    @Mock
    private MessageTrackerProvider messageTrackerProvider;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test add post mail event.
     * 
     */
    @Test
    public void testAddPostMailEvent() {
        String trackId = "2017-02-ABC-DEF-00";
        String eventCode = "tracker.postmail.updated";
        Mockito.when(messageTrackerProvider.getMessage(Matchers.anyString(), Matchers.anyString())).thenReturn("");
        trackerLinkService.addPostMailEvent(trackId, eventCode);
    }

    /**
     * Test add post mail event with failure.
     * 
     */
    @Test
    public void testFailedAddPostMailEvent() {
        String trackId = "2017-02-ABC-DEF-00";
        String eventCode = "";
        Mockito.when(messageTrackerProvider.getMessage(Matchers.anyString(), Matchers.anyString())).thenThrow(new NullPointerException());
        trackerLinkService.addPostMailEvent(trackId, eventCode);
    }

    /**
     * Test update post mail status.
     * 
     */
    @Test
    public void testAddUpdatePostmailStatuts() {
        String trackId = "2017-02-ABC-DEF-00";
        String eventCode = "tracker.postmail.updated";
        Mockito.when(messageTrackerProvider.getMessage(Matchers.anyString(), Matchers.anyString())).thenReturn("");
        trackerLinkService.addUpdatePostmailStatuts(trackId, eventCode);
    }

    /**
     * Test update post mail status with failure.
     * 
     */
    @Test
    public void testFailedAddUpdatePostmailStatuts() {
        String trackId = "2017-02-ABC-DEF-00";
        String eventCode = "";
        Mockito.when(messageTrackerProvider.getMessage(Matchers.anyString(), Matchers.anyString())).thenThrow(new NullPointerException());
        trackerLinkService.addUpdatePostmailStatuts(trackId, eventCode);
    }

    /**
     * Test getting track id.
     * 
     * @throws TechniqueException
     *             Exceptiion raised
     */
    @Test
    public void testCreateTrackerId() throws TechniqueException {
        String trackId = "2017-02-ABC-DEF-00";
        Mockito.when(trackerFacade.createUid()).thenReturn(trackId);
        Mockito.when(trackerFacade.link(Matchers.anyString(), Matchers.anyString())).thenReturn("Response");
        String actual = trackerLinkService.createTrackerId("Ref");
        assertEquals(actual, trackId);
    }
}
