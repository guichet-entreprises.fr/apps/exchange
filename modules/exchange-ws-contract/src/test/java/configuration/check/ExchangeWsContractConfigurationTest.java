/**
 * 
 */
package configuration.check;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Classe permettant de tester le démarrage du contexte Spring pour le ws
 * contract.
 *
 * @author $Author: kelmoura $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:/spring/applicationContext-ge-exchange-ws-contract.xml" })
public class ExchangeWsContractConfigurationTest {

    /**
     * Test.
     */
    @Test
    public void testSpringConfiguration() {
        // On teste juste la configuration Spring au chargement
    }

}
