/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.exchange.ws.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

/**
 * Exchange REST service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Path("/v1")
public interface IEmailRestService {

  // TODO Vérifier que l'on peut supprimer la seconde méthode (sans pj)
  /**
   * Send a new email with(out) a single attachment.
   * 
   * @param senderEmailAddress
   *          Email of the sender (required)
   * @param recipientEmailAddress
   *          Email of the recipient (required)
   * @param object
   *          Email object (required)
   * @param content
   *          Email content (required)
   * @param attachmentFile
   *          Attachment file (optional)
   * @return True if the email is sent with success
   */
  @Path("email/send")
  @POST
  @Consumes({MediaType.APPLICATION_JSON, MediaType.MULTIPART_FORM_DATA })
  @Produces(MediaType.APPLICATION_JSON)
  Boolean send(@QueryParam("senderEmailAddress") final String senderEmailAddress,
    @QueryParam("recipientEmailAddress") final String recipientEmailAddress, @QueryParam("object") final String object,
    @QueryParam("content") final String content, final Attachment attachmentFile);

}
