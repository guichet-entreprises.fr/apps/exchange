/**
 * 
 */
package fr.ge.exchange.email.ws.v1.service;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;

/**
 * Interface ISendEmailRestService.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface ISendEmailRestService {

  /**
   * REST method for sending an email.
   *
   * @param emailSend
   *          Email information
   * @param attachmentPdf
   *          attachment pdf
   * @return The response
   * @throws TechniqueException
   *           is any problem preventing the execution to complete occurs
   */

  @Path("/email/send")
  @POST
  @Consumes({MediaType.APPLICATION_JSON, MediaType.MULTIPART_FORM_DATA })
  @Produces(MediaType.APPLICATION_JSON)
  Response sendEmail(@QueryParam("") final EmailSendBean emailSend,
    @Multipart(value = "file", type = "application/pdf") final InputStream attachmentPdf) throws TechniqueException;

  /**
   * REST method for sending an email without any attachment.
   *
   * @param emailSend
   *          Email information
   * @return The response
   * @throws TechniqueException
   *           is any problem preventing the execution to complete occurs
   */
  @Path("/email/sendWithoutAttachement")
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  Response sendEmail(@QueryParam("") final EmailSendBean emailSend) throws TechniqueException;
}
