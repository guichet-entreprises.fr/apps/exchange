/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.exchange.postmail.ws.v1.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * The bean that describes a postmail status.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "postmailStatus", namespace = "http://v1.ws.exchange.ge.fr")
public class PostMailStatusBean implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The postmail id. */
  private String trackId;

  /** The recipient name. */
  private String recipientName;

  /** The status. */
  private String status;

  /** The channel. */
  private String channel;

  /** The urgency. */
  private String urgency;

  /** The Id Imprimerie Nationale. */
  private String idImpNat;

  /** The number of pages. */
  private int numberPages;

  /** The full address of the recipient. */
  private String fullAddress;

  /** The date. */
  private String datePostmail;

  /**
   * Default constructor.
   */
  public PostMailStatusBean() {
    // Nothing to do
  }

  /**
   * Accesseur sur l'attribut {@link #trackId}.
   *
   * @return String id
   */
  public String getTrackId() {
    return trackId;
  }

  /**
   * Mutateur sur l'attribut {@link #trackId}.
   *
   * @param trackId
   *          la nouvelle valeur de l'attribut trackId
   */
  public void setTrackId(final String trackId) {
    this.trackId = trackId;
  }

  /**
   * Accesseur sur l'attribut {@link #recipientName}.
   *
   * @return String recipientName
   */
  public String getRecipientName() {
    return recipientName;
  }

  /**
   * Mutateur sur l'attribut {@link #recipientName}.
   *
   * @param recipientName
   *          la nouvelle valeur de l'attribut recipient
   */
  public void setRecipientName(final String recipientName) {
    this.recipientName = recipientName;
  }

  /**
   * Accesseur sur l'attribut {@link #channel}.
   *
   * @return String channel
   */
  public String getChannel() {
    return channel;
  }

  /**
   * Mutateur sur l'attribut {@link #channel}.
   *
   * @param channel
   *          la nouvelle valeur de l'attribut channel
   */
  public void setChannel(final String channel) {
    this.channel = channel;
  }

  /**
   * Accesseur sur l'attribut {@link #urgency}.
   *
   * @return String urgency
   */
  public String getUrgency() {
    return urgency;
  }

  /**
   * Mutateur sur l'attribut {@link #urgency}.
   *
   * @param urgency
   *          la nouvelle valeur de l'attribut urgency
   */
  public void setUrgency(final String urgency) {
    this.urgency = urgency;
  }

  /**
   * Accesseur sur l'attribut {@link #idImpNat}.
   *
   * @return String idImpNat
   */
  public String getIdImpNat() {
    return idImpNat;
  }

  /**
   * Mutateur sur l'attribut {@link #idImpNat}.
   *
   * @param idImpNat
   *          la nouvelle valeur de l'attribut idImpNat
   */
  public void setIdImpNat(final String idImpNat) {
    this.idImpNat = idImpNat;
  }

  /**
   * Accesseur sur l'attribut {@link #numberPages}.
   *
   * @return int numberPages
   */
  public int getNumberPages() {
    return numberPages;
  }

  /**
   * Mutateur sur l'attribut {@link #numberPages}.
   *
   * @param numberPages
   *          la nouvelle valeur de l'attribut numberPages
   */
  public void setNumberPages(final int numberPages) {
    this.numberPages = numberPages;
  }

  /**
   * Accesseur sur l'attribut {@link #fullAddress}.
   *
   * @return String fullAddress
   */
  public String getFullAddress() {
    return fullAddress;
  }

  /**
   * Mutateur sur l'attribut {@link #fullAddress}.
   *
   * @param fullAddress
   *          la nouvelle valeur de l'attribut fullAddress
   */
  public void setFullAddress(final String fullAddress) {
    this.fullAddress = fullAddress;
  }

  /**
   * Accesseur sur l'attribut {@link #status}.
   *
   * @return String status
   */
  public String getStatus() {
    return status;
  }

  /**
   * Mutateur sur l'attribut {@link #status}.
   *
   * @param status
   *          la nouvelle valeur de l'attribut status
   */
  public void setStatus(final String status) {
    this.status = status;
  }

  /**
   * Accesseur sur l'attribut {@link #datePostmail}.
   *
   * @return String datePostmail
   */
  public String getDatePostmail() {
    return datePostmail;
  }

  /**
   * Mutateur sur l'attribut {@link #datePostmail}.
   *
   * @param datePostmail
   *          la nouvelle valeur de l'attribut datePostmail
   */
  public void setDatePostmail(final String datePostmail) {
    this.datePostmail = datePostmail;
  }

}
