/**
 * 
 */
package fr.ge.exchange.postmail.ws.v1.bean.in;

/**
 * @author $Author: mtakerra $
 * @version $Revision: 0 $
 */
public class PostmailCsvRecord {

  /** Tracker Id. */
  private String trackId;

  /** Document id. */
  private String docId;

  /** Status postmail returned by IN. */
  private String status;

  /** Number of pages in the postmail. */
  private int numberPages;

  /** The channel used. */
  private String channel;

  /** The urgency of the postmail. */
  private String urgency;

  /** numreco postmail */
  private String numreco;

  /**
   * Accesseur sur l'attribut {@link #trackId}.
   *
   * @return String trackId
   */
  public String getTrackId() {
    return trackId;
  }

  /**
   * Mutateur sur l'attribut {@link #trackId}.
   *
   * @param trackId
   *          la nouvelle valeur de l'attribut trackId
   */
  public void setTrackId(String trackId) {
    this.trackId = trackId;
  }

  /**
   * Accesseur sur l'attribut {@link #docId}.
   *
   * @return String docId
   */
  public String getDocId() {
    return docId;
  }

  /**
   * Mutateur sur l'attribut {@link #docId}.
   *
   * @param docId
   *          la nouvelle valeur de l'attribut docId
   */
  public void setDocId(String docId) {
    this.docId = docId;
  }

  /**
   * Accesseur sur l'attribut {@link #status}.
   *
   * @return String status
   */
  public String getStatus() {
    return status;
  }

  /**
   * Mutateur sur l'attribut {@link #status}.
   *
   * @param status
   *          la nouvelle valeur de l'attribut status
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * Accesseur sur l'attribut {@link #numberPages}.
   *
   * @return int numberPages
   */
  public int getNumberPages() {
    return numberPages;
  }

  /**
   * Mutateur sur l'attribut {@link #numberPages}.
   *
   * @param numberPages
   *          la nouvelle valeur de l'attribut numberPages
   */
  public void setNumberPages(int numberPages) {
    this.numberPages = numberPages;
  }

  /**
   * Accesseur sur l'attribut {@link #channel}.
   *
   * @return String channel
   */
  public String getChannel() {
    return channel;
  }

  /**
   * Mutateur sur l'attribut {@link #channel}.
   *
   * @param channel
   *          la nouvelle valeur de l'attribut channel
   */
  public void setChannel(String channel) {
    this.channel = channel;
  }

  /**
   * Accesseur sur l'attribut {@link #urgency}.
   *
   * @return String urgency
   */
  public String getUrgency() {
    return urgency;
  }

  /**
   * Mutateur sur l'attribut {@link #urgency}.
   *
   * @param urgency
   *          la nouvelle valeur de l'attribut urgency
   */
  public void setUrgency(String urgency) {
    this.urgency = urgency;
  }

  /**
   * Accesseur sur l'attribut {@link #numreco}.
   *
   * @return String numreco
   */
  public String getNumreco() {
    return numreco;
  }

  /**
   * Mutateur sur l'attribut {@link #numreco}.
   *
   * @param numreco
   *          la nouvelle valeur de l'attribut numreco
   */
  public void setNumreco(String numreco) {
    this.numreco = numreco;
  }
}
