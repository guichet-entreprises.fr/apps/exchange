/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.exchange.ws.rest;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.ws.model.MailMessageStatus;

/**
 * Exchange postmail REST service. This service acts as a proxy for a remote mail service that does
 * the actual post mail service.<br/>
 * The mail content is transform to fit the remote mail service, the result of the transfomation is
 * downloadable.
 */
public interface IMailRestService {

  /**
   * Create a new postmail request.
   * 
   * @param referenceId
   *          the business id to link this mail to.
   * @param recipientName
   *          Name of the recipient (required)
   * @param recipientNameCompl
   *          Complement name of the recipient (optional)
   * @param recipientAddressName
   *          Address of the recipient (required)
   * @param recipientAddressNameCompl
   *          Complement address of the recipient (optional)
   * @param recipientPostalCode
   *          Postal code of the recipient (required)
   * @param recipientCity
   *          City of the recipient (required)
   * @param attachmentFile
   *          PDF attachment (required)
   * @return The postmail Id request
   * @throws TechniqueException
   *           a technical error prevents from sending the mail
   */
  @PUT
  @Consumes({MediaType.APPLICATION_JSON, MediaType.MULTIPART_FORM_DATA })
  @Path("/private/postmail")
  @Produces(MediaType.APPLICATION_JSON)
  String send(@QueryParam("referenceId") final String referenceId, @QueryParam("recipientName") final String recipientName,
    @QueryParam("recipientNameCompl") final String recipientNameCompl,
    @QueryParam("recipientAddressName") final String recipientAddressName,
    @QueryParam("recipientAddressNameCompl") final String recipientAddressNameCompl,
    @QueryParam("recipientPostalCode") final String recipientPostalCode, @QueryParam("recipientCity") final String recipientCity,
    @Multipart(value = "attachmentFile", type = "application/pdf") final InputStream attachmentFile) throws TechniqueException;

  /**
   * Download the postmail attachment.
   * 
   * @param postmailId
   *          MailRequest id (required)
   * @return the file to download, as {@link InputStream}
   */
  @GET
  @Path("/private/postmail/{postmailId}/download")
  @Produces(MediaType.APPLICATION_OCTET_STREAM)
  Response download(@PathParam("postmailId") final String postmailId);

  /**
   * Get the mail status from DB.
   * 
   * @param trackerId
   *          Track id
   * @return The response
   */
  @GET
  @Path("/private/postmail/{postmailId}/status")
  @Produces(MediaType.APPLICATION_JSON)
  MailMessageStatus getStatus(@PathParam("postmailId") final String trackerId);

}
