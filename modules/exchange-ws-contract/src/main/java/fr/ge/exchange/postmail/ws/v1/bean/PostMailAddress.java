/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.exchange.postmail.ws.v1.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The bean that describes a postmail send.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
// TODO LAB : renommer le bean
@XmlRootElement(name = "postmailSend", namespace = "http://v1.ws.exchange.ge.fr")
public class PostMailAddress implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The reference id. */
    private String trackId; // TODO LAB : renommer en referenceId

    // TODO LAB : revoir la javadoc : incompréhensible
    /** The recipient name. */
    private String recipientName;

    /** The recipient name complement. */
    private String recipientNameCompl;

    /** The recipient addressName. */
    private String addressName;

    /** The recipient addressName. */
    private String addressNameCompl;

    /** The recipient postal code. */
    private String postalCode;

    /** The recipient city. */
    private String city;

    // TODO LAB : vérifier l'utilité
    /** The attachment absolute local path. */
    @JsonIgnore
    private String attachmentPath;

    /**
     * Default constructor.
     */
    public PostMailAddress() {
        // Nothing to do
    }

    /**
     * Accesseur sur l'attribut {@link #trackId}.
     *
     * @return String id
     */
    public String getTrackId() {
        return trackId;
    }

    /**
     * Mutateur sur l'attribut {@link #trackId}.
     *
     * @param trackId
     *            la nouvelle valeur de l'attribut trackId
     */
    public void setTrackId(final String trackId) {
        this.trackId = trackId;
    }

    /**
     * Accesseur sur l'attribut {@link #recipientName}.
     *
     * @return String recipientName
     */
    public String getRecipientName() {
        return recipientName;
    }

    /**
     * Mutateur sur l'attribut {@link #recipientName}.
     *
     * @param recipientName
     *            la nouvelle valeur de l'attribut recipient
     */
    public void setRecipientName(final String recipientName) {
        this.recipientName = recipientName;
    }

    /**
     * Accesseur sur l'attribut {@link #addressName}.
     *
     * @return String address
     */
    public String getAddressName() {
        return addressName;
    }

    /**
     * Mutateur sur l'attribut {@link #addressName}.
     *
     * @param addressName
     *            la nouvelle valeur de l'attribut addressName
     */
    public void setAddressName(final String addressName) {
        this.addressName = addressName;
    }

    /**
     * Accesseur sur l'attribut {@link #postalCode}.
     *
     * @return String postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Mutateur sur l'attribut {@link #postalCode}.
     *
     * @param postalCode
     *            la nouvelle valeur de l'attribut postalCode
     */
    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Accesseur sur l'attribut {@link #city}.
     *
     * @return String city
     */
    public String getCity() {
        return city;
    }

    /**
     * Mutateur sur l'attribut {@link #city}.
     *
     * @param city
     *            la nouvelle valeur de l'attribut city
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * Accesseur sur l'attribut {@link #addressNameCompl}.
     *
     * @return String addressNameCompl
     */
    public String getAddressNameCompl() {
        return addressNameCompl;
    }

    /**
     * Mutateur sur l'attribut {@link #addressNameCompl}.
     *
     * @param addressNameCompl
     *            la nouvelle valeur de l'attribut addressNameCompl
     */
    public void setAddressNameCompl(final String addressNameCompl) {
        this.addressNameCompl = addressNameCompl;
    }

    /**
     * Accesseur sur l'attribut {@link #recipientNameCompl}.
     *
     * @return String recipientNameCompl
     */
    public String getRecipientNameCompl() {
        return recipientNameCompl;
    }

    /**
     * Mutateur sur l'attribut {@link #recipientNameCompl}.
     *
     * @param recipientNameCompl
     *            la nouvelle valeur de l'attribut recipientNameCompl
     */
    public void setRecipientNameCompl(final String recipientNameCompl) {
        this.recipientNameCompl = recipientNameCompl;
    }

    /**
     * Return the complete recipient name.
     * 
     * @return the complete recipient name
     */
    @JsonIgnore
    public String getRecipientNameComplete() {
        StringBuffer sb = new StringBuffer(this.getRecipientName());
        if (null != this.getRecipientNameCompl() && !this.getRecipientNameCompl().isEmpty()) {
            sb.append("\n").append(this.getRecipientNameCompl());
        }
        return sb.toString();
    }

    /**
     * Return the complete address recipient.
     * 
     * @return the complete address
     */
    @JsonIgnore
    public String getAddressNameComplete() {
        StringBuffer sb = new StringBuffer(this.getAddressName());
        if (null != this.getAddressNameCompl() && !this.getAddressNameCompl().isEmpty()) {
            sb.append("\n").append(this.getAddressNameCompl());
        }
        return sb.toString();
    }

    /**
     * Accesseur sur l'attribut {@link #attachmentPath}.
     *
     * @return String attachmentPath
     */
    public String getAttachmentPath() {
        return attachmentPath;
    }

    /**
     * Mutateur sur l'attribut {@link #attachmentPath}.
     *
     * @param attachmentPath
     *            la nouvelle valeur de l'attribut attachmentPath
     */
    public void setAttachmentPath(final String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }
}
