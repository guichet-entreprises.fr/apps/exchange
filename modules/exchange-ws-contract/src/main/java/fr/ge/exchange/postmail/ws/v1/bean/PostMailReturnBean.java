/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.exchange.postmail.ws.v1.bean;

//TODO LAB : normalement inutile
/**
 * The bean that describes a postmail send.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class PostMailReturnBean extends PostMailIdBean {

  /** The recipient name. */
  private String recipient;

  /** The recipient address. */
  private String address;

  /** The recipient postal code. */
  private String postalCode;

  /** The recipient city. */
  private String city;

  /**
   * Default constructor.
   */
  public PostMailReturnBean() {
    // Nothing to do
  }

  /**
   * Accesseur sur l'attribut {@link #recipient}.
   *
   * @return String recipient
   */
  public String getRecipient() {
    return recipient;
  }

  /**
   * Mutateur sur l'attribut {@link #recipient}.
   *
   * @param recipient
   *          la nouvelle valeur de l'attribut recipient
   */
  public void setRecipient(String recipient) {
    this.recipient = recipient;
  }

  /**
   * Accesseur sur l'attribut {@link #address}.
   *
   * @return String address
   */
  public String getAddress() {
    return address;
  }

  /**
   * Mutateur sur l'attribut {@link #address}.
   *
   * @param address
   *          la nouvelle valeur de l'attribut address
   */
  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * Accesseur sur l'attribut {@link #postalCode}.
   *
   * @return String postalCode
   */
  public String getPostalCode() {
    return postalCode;
  }

  /**
   * Mutateur sur l'attribut {@link #postalCode}.
   *
   * @param postalCode
   *          la nouvelle valeur de l'attribut postalCode
   */
  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  /**
   * Accesseur sur l'attribut {@link #city}.
   *
   * @return String city
   */
  public String getCity() {
    return city;
  }

  /**
   * Mutateur sur l'attribut {@link #city}.
   *
   * @param city
   *          la nouvelle valeur de l'attribut city
   */
  public void setCity(String city) {
    this.city = city;
  }
}
