/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.exchange.email.ws.v1.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * The bean that describes a email send.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "emailSend", namespace = "http://v1.ws.exchange.ge.fr")
public class EmailSendBean implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** Sender email addresse. **/
  private String sender;

  /** List of recipients separated by ';'. */
  private String recipient;

  /** The object email. */
  private String object;

  /** The status. */
  private String status;

  /** The body. */
  private String content;

  /** Key corresponding to the email template id. **/
  private String templateKey;

  /** PDF Attachement file name. **/
  private String attachmentPDFName;

  /**
   * 
   * Constructeur de la classe.
   *
   */
  public EmailSendBean() {

  }

  /**
   * Accesseur sur l'attribut {@link #sender}.
   *
   * @return String sender
   */
  public String getSender() {
    return sender;
  }

  /**
   * Mutateur sur l'attribut {@link #sender}.
   *
   * @param sender
   *          la nouvelle valeur de l'attribut sender
   */
  public void setSender(final String sender) {
    this.sender = sender;
  }

  /**
   * Accesseur sur l'attribut {@link #recipient}.
   *
   * @return String recipient
   */
  public String getRecipient() {
    return recipient;
  }

  /**
   * Mutateur sur l'attribut {@link #recipient}.
   *
   * @param recipient
   *          la nouvelle valeur de l'attribut recipient
   */
  public void setRecipient(final String recipient) {
    this.recipient = recipient;
  }

  /**
   * Accesseur sur l'attribut {@link #object}.
   *
   * @return String object
   */
  public String getObject() {
    return object;
  }

  /**
   * Mutateur sur l'attribut {@link #object}.
   *
   * @param object
   *          la nouvelle valeur de l'attribut object
   */
  public void setObject(final String object) {
    this.object = object;
  }

  /**
   * Accesseur sur l'attribut {@link #status}.
   *
   * @return String status
   */
  public String getStatus() {
    return status;
  }

  /**
   * Mutateur sur l'attribut {@link #status}.
   *
   * @param status
   *          la nouvelle valeur de l'attribut status
   */
  public void setStatus(final String status) {
    this.status = status;
  }

  /**
   * Accesseur sur l'attribut {@link #content}.
   *
   * @return String content
   */
  public String getContent() {
    return content;
  }

  /**
   * Mutateur sur l'attribut {@link #content}.
   *
   * @param content
   *          la nouvelle valeur de l'attribut content
   */
  public void setContent(final String content) {
    this.content = content;
  }

  /**
   * Accesseur sur l'attribut {@link #templateKey}.
   *
   * @return String templateKey
   */
  public String getTemplateKey() {
    return templateKey;
  }

  /**
   * Mutateur sur l'attribut {@link #templateKey}.
   *
   * @param templateKey
   *          la nouvelle valeur de l'attribut templateKey
   */
  public void setTemplateKey(final String templateKey) {
    this.templateKey = templateKey;
  }

  /**
   * Accesseur sur l'attribut {@link #attachmentPDFName}.
   *
   * @return String attachmentPDFName
   */
  public String getAttachmentPDFName() {
    return attachmentPDFName;
  }

  /**
   * Mutateur sur l'attribut {@link #attachmentPDFName}.
   *
   * @param attachmentPDFName
   *          la nouvelle valeur de l'attribut attachmentPDFName
   */
  public void setAttachmentPDFName(final String attachmentPDFName) {
    this.attachmentPDFName = attachmentPDFName;
  }

}
